/*     */ package net.minecraft.client.settings;
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum EnumOptions
/*     */ {
/*   5 */   MUSIC("options.music", true, false), 
/*   6 */   SOUND("options.sound", true, false), 
/*   7 */   INVERT_MOUSE("options.invertMouse", false, true), 
/*   8 */   SENSITIVITY("options.sensitivity", true, false), 
/*   9 */   FOV("options.fov", true, false), 
/*  10 */   GAMMA("options.gamma", true, false), 
/*  11 */   RENDER_DISTANCE("options.renderDistance", false, false), 
/*  12 */   VIEW_BOBBING("options.viewBobbing", false, true), 
/*  13 */   ANAGLYPH("options.anaglyph", false, true), 
/*  14 */   ADVANCED_OPENGL("options.advancedOpengl", false, true), 
/*  15 */   FRAMERATE_LIMIT("options.framerateLimit", false, false), 
/*  16 */   DIFFICULTY("options.difficulty", false, false), 
/*  17 */   GRAPHICS("options.graphics", false, false), 
/*  18 */   AMBIENT_OCCLUSION("options.ao", false, false), 
/*  19 */   GUI_SCALE("options.guiScale", false, false), 
/*  20 */   RENDER_CLOUDS("options.renderClouds", false, true), 
/*  21 */   PARTICLES("options.particles", false, false), 
/*  22 */   CHAT_VISIBILITY("options.chat.visibility", false, false), 
/*  23 */   CHAT_COLOR("options.chat.color", false, true), 
/*  24 */   CHAT_LINKS("options.chat.links", false, true), 
/*  25 */   CHAT_OPACITY("options.chat.opacity", true, false), 
/*  26 */   CHAT_LINKS_PROMPT("options.chat.links.prompt", false, true), 
/*  27 */   USE_SERVER_TEXTURES("options.serverTextures", false, true), 
/*  28 */   SNOOPER_ENABLED("options.snooper", false, true), 
/*  29 */   USE_FULLSCREEN("options.fullscreen", false, true), 
/*  30 */   ENABLE_VSYNC("options.vsync", false, true), 
/*  31 */   SHOW_CAPE("options.showCape", false, true), 
/*  32 */   TOUCHSCREEN("options.touchscreen", false, true), 
/*  33 */   CHAT_SCALE("options.chat.scale", true, false), 
/*  34 */   CHAT_WIDTH("options.chat.width", true, false), 
/*  35 */   CHAT_HEIGHT_FOCUSED("options.chat.height.focused", true, false), 
/*  36 */   CHAT_HEIGHT_UNFOCUSED("options.chat.height.unfocused", true, false), 
/*     */ 
/*  38 */   FOG_FANCY("Туман", false, false), 
/*  39 */   FOG_START("Начало Тумана", false, false), 
/*  40 */   MIPMAP_LEVEL("Уровень Детализации", false, false), 
/*  41 */   MIPMAP_TYPE("Тип Детализации", false, false), 
/*  42 */   LOAD_FAR("Дистанция Загрузки", false, false), 
/*  43 */   PRELOADED_CHUNKS("Предзагр. Чанков", false, false), 
/*  44 */   SMOOTH_FPS("Сглаживание FPS", false, false), 
/*     */ 
/*  46 */   CLOUDS("Облака", false, false), 
/*  47 */   CLOUD_HEIGHT("Высота Облаков", true, false), 
/*  48 */   TREES("Листья", false, false), 
/*  49 */   GRASS("Трава", false, false), 
/*  50 */   RAIN("Дождь & Снег", false, false), 
/*  51 */   WATER("Вода", false, false), 
/*  52 */   ANIMATED_WATER("Анимация Воды", false, false), 
/*  53 */   ANIMATED_LAVA("Анимация Лавы", false, false), 
/*  54 */   ANIMATED_FIRE("Анимация Огня", false, false), 
/*  55 */   ANIMATED_PORTAL("Анимация Портала", false, false), 
/*  56 */   AO_LEVEL("Мягкое Освещение", true, false), 
/*  57 */   LAGOMETER("Отладоч. Информация", false, false), 
/*  58 */   AUTOSAVE_TICKS("Автосохранение", false, false), 
/*  59 */   BETTER_GRASS("Красивая Трава", false, false), 
/*  60 */   ANIMATED_REDSTONE("Анимация Красной пыли", false, false), 
/*  61 */   ANIMATED_EXPLOSION("Анимация Взрыва", false, false), 
/*  62 */   ANIMATED_FLAME("Анимация Пламени", false, false), 
/*  63 */   ANIMATED_SMOKE("Анимация Дыма", false, false), 
/*  64 */   WEATHER("Погода", false, false), 
/*  65 */   SKY("Небо", false, false), 
/*  66 */   STARS("Звезды", false, false), 
/*  67 */   SUN_MOON("Солнце & Луна", false, false), 
/*  68 */   CHUNK_UPDATES("Обновл. Чанков за Кадр", false, false), 
/*  69 */   CHUNK_UPDATES_DYNAMIC("Динамич. Обновление", false, false), 
/*  70 */   TIME("Время", false, false), 
/*  71 */   CLEAR_WATER("Прозрачная Вода", false, false), 
/*  72 */   SMOOTH_WORLD("Сглаживание Мира", false, false), 
/*  73 */   DEPTH_FOG("Глубина Тумана", false, false), 
/*  74 */   VOID_PARTICLES("Частицы Пустоты", false, false), 
/*  75 */   WATER_PARTICLES("Частицы Воды", false, false), 
/*  76 */   RAIN_SPLASH("Частицы Дождя", false, false), 
/*  77 */   PORTAL_PARTICLES("Частицы Портала", false, false), 
/*  78 */   POTION_PARTICLES("Частицы Зелий", false, false), 
/*  79 */   PROFILER("Отладочный Профиль", false, false), 
/*  80 */   DRIPPING_WATER_LAVA("Капающая Вода / Лава", false, false), 
/*  81 */   BETTER_SNOW("Красивый Снег", false, false), 
/*  82 */   FULLSCREEN_MODE("Экран", false, false), 
/*  83 */   ANIMATED_TERRAIN("Анимация Местности", false, false), 
/*  84 */   ANIMATED_ITEMS("Анимация Вещей", false, false), 
/*  85 */   SWAMP_COLORS("Цвета Болота", false, false), 
/*  86 */   RANDOM_MOBS("Случайные Мобы", false, false), 
/*  87 */   SMOOTH_BIOMES("Сглаживание Биомов", false, false), 
/*  88 */   CUSTOM_FONTS("Нестандартный Шрифт", false, false), 
/*  89 */   CUSTOM_COLORS("Нестандартные Цвета", false, false), 
/*  90 */   SHOW_CAPES("Отображать Плащ", false, false), 
/*  91 */   CONNECTED_TEXTURES("Связь Текстур", false, false), 
/*  92 */   AA_LEVEL("Сглаживание", false, false), 
/*  93 */   AF_LEVEL("Анизотроп. Фильтрация", false, false), 
/*  94 */   RENDER_DISTANCE_FINE("Дистанция", true, false), 
/*  95 */   ANIMATED_TEXTURES("Анимация Текстур", false, false), 
/*  96 */   NATURAL_TEXTURES("Природные Текстуры", false, false), 
/*  97 */   CHUNK_LOADING("Загр.Чанков", false, false), 
/*  98 */   FRAMERATE_LIMIT_FINE("FPS", true, false), 
/*  99 */   HELD_ITEM_TOOLTIPS("Подсказки у Вещей", false, false), 
/* 100 */   DROPPED_ITEMS("Объемные Вещи", false, false), 
/* 101 */   LAZY_CHUNK_LOADING("Отложенная Загрузка", false, false), 
/* 102 */   CUSTOM_SKY("Нестандартное Небо", false, false);
/*     */ 
/*     */   private final boolean enumFloat;
/*     */   private final boolean enumBoolean;
/*     */   private final String enumString;
/*     */ 
/*     */   public static EnumOptions getEnumOptions(int par0) {
/* 111 */     EnumOptions[] var1 = values();
/* 112 */     int var2 = var1.length;
/*     */ 
/* 114 */     for (int var3 = 0; var3 < var2; var3++)
/*     */     {
/* 116 */       EnumOptions var4 = var1[var3];
/*     */ 
/* 118 */       if (var4.returnEnumOrdinal() == par0)
/*     */       {
/* 120 */         return var4;
/*     */       }
/*     */     }
/*     */ 
/* 124 */     return null;
/*     */   }
/*     */ 
/*     */   private EnumOptions(String par3Str, boolean par4, boolean par5)
/*     */   {
/* 129 */     this.enumString = par3Str;
/* 130 */     this.enumFloat = par4;
/* 131 */     this.enumBoolean = par5;
/*     */   }
/*     */ 
/*     */   public boolean getEnumFloat()
/*     */   {
/* 136 */     return this.enumFloat;
/*     */   }
/*     */ 
/*     */   public boolean getEnumBoolean()
/*     */   {
/* 141 */     return this.enumBoolean;
/*     */   }
/*     */ 
/*     */   public int returnEnumOrdinal()
/*     */   {
/* 146 */     return ordinal();
/*     */   }
/*     */ 
/*     */   public String getEnumString()
/*     */   {
/* 151 */     return this.enumString;
/*     */   }
/*     */ }

/* Location:           D:\Minecraft_Job\Minecraft-adventures.ru\OptiFine.zip
 * Qualified Name:     net.minecraft.client.settings.EnumOptions
 * JD-Core Version:    0.6.2
 */