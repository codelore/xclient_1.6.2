/*      */ package net.minecraft.client.settings;
/*      */ 
/*      */ import java.io.BufferedReader;
/*      */ import java.io.File;
/*      */ import java.io.FileReader;
/*      */ import java.io.FileWriter;
/*      */ import java.io.PrintWriter;
/*      */ import java.util.Arrays;
/*      */ import java.util.List;
/*      */ import net.minecraft.block.Block;
/*      */ import net.minecraft.block.BlockFluid;
/*      */ import net.minecraft.client.Minecraft;
/*      */ import net.minecraft.client.audio.SoundManager;
/*      */ import net.minecraft.client.entity.EntityClientPlayerMP;
/*      */ import net.minecraft.client.gui.FontRenderer;
/*      */ import net.minecraft.client.gui.GuiIngame;
/*      */ import net.minecraft.client.gui.GuiNewChat;
/*      */ import net.minecraft.client.multiplayer.NetClientHandler;
/*      */ import net.minecraft.client.renderer.RenderGlobal;
/*      */ import net.minecraft.client.resources.I18n;
/*      */ import net.minecraft.logging.ILogAgent;
/*      */ import net.minecraft.network.packet.Packet204ClientInfo;
/*      */ import net.minecraft.src.Config;
/*      */ import net.minecraft.src.CustomColorizer;
/*      */ import net.minecraft.src.CustomSky;
/*      */ import net.minecraft.src.NaturalTextures;
/*      */ import net.minecraft.src.RandomMobs;
/*      */ import net.minecraft.src.Reflector;
/*      */ import net.minecraft.src.ReflectorClass;
/*      */ import net.minecraft.src.TextureUtils;
/*      */ import net.minecraft.src.WrUpdaterSmooth;
/*      */ import net.minecraft.src.WrUpdaterThreaded;
/*      */ import net.minecraft.src.WrUpdates;
/*      */ import net.minecraft.world.World;
/*      */ import net.minecraft.world.chunk.Chunk;
/*      */ import net.minecraft.world.chunk.EmptyChunk;
/*      */ import net.minecraft.world.chunk.IChunkProvider;
/*      */ import net.minecraft.world.chunk.NibbleArray;
/*      */ import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
/*      */ import org.lwjgl.input.Keyboard;
/*      */ import org.lwjgl.input.Mouse;
/*      */ import org.lwjgl.opengl.Display;
/*      */ 
/*      */ public class GameSettings
/*      */ {
/*   17 */   private static final String[] RENDER_DISTANCES = { "options.renderDistance.far", "options.renderDistance.normal", "options.renderDistance.short", "options.renderDistance.tiny" };
/*   18 */   private static final String[] DIFFICULTIES = { "options.difficulty.peaceful", "options.difficulty.easy", "options.difficulty.normal", "options.difficulty.hard" };
/*      */ 
/*   21 */   private static final String[] GUISCALES = { "options.guiScale.auto", "options.guiScale.small", "options.guiScale.normal", "options.guiScale.large" };
/*   22 */   private static final String[] CHAT_VISIBILITIES = { "options.chat.visibility.full", "options.chat.visibility.system", "options.chat.visibility.hidden" };
/*   23 */   private static final String[] PARTICLES = { "options.particles.all", "options.particles.decreased", "options.particles.minimal" };
/*      */ 
/*   26 */   private static final String[] LIMIT_FRAMERATES = { "performance.max", "performance.balanced", "performance.powersaver" };
/*   27 */   private static final String[] AMBIENT_OCCLUSIONS = { "options.ao.off", "options.ao.min", "options.ao.max" };
/*   28 */   public float musicVolume = 1.0F;
/*   29 */   public float soundVolume = 1.0F;
/*   30 */   public float mouseSensitivity = 0.5F;
/*      */   public boolean invertMouse;
/*      */   public int renderDistance;
/*   33 */   public boolean viewBobbing = true;
/*      */   public boolean anaglyph;
/*      */   public boolean advancedOpengl;
/*   38 */   public int limitFramerate = 1;
/*   39 */   public boolean fancyGraphics = true;
/*      */ 
/*   42 */   public int ambientOcclusion = 2;
/*      */ 
/*   45 */   public boolean clouds = true;
/*      */ 
/*   48 */   public int ofRenderDistanceFine = 128;
/*      */ 
/*   50 */   public int ofLimitFramerateFine = 0;
/*   51 */   public int ofFogType = 1;
/*   52 */   public float ofFogStart = 0.8F;
/*   53 */   public int ofMipmapLevel = 0;
/*   54 */   public int ofMipmapType = 0;
/*   55 */   public boolean ofLoadFar = false;
/*   56 */   public int ofPreloadedChunks = 0;
/*   57 */   public boolean ofOcclusionFancy = false;
/*   58 */   public boolean ofSmoothFps = false;
/*   59 */   public boolean ofSmoothWorld = Config.isSingleProcessor();
/*   60 */   public boolean ofLazyChunkLoading = Config.isSingleProcessor();
/*   61 */   public float ofAoLevel = 1.0F;
/*   62 */   public int ofAaLevel = 0;
/*   63 */   public int ofAfLevel = 1;
/*      */ 
/*   65 */   public int ofClouds = 0;
/*   66 */   public float ofCloudsHeight = 0.0F;
/*   67 */   public int ofTrees = 0;
/*   68 */   public int ofGrass = 0;
/*   69 */   public int ofRain = 0;
/*   70 */   public int ofWater = 0;
/*   71 */   public int ofDroppedItems = 0;
/*   72 */   public int ofBetterGrass = 3;
/*   73 */   public int ofAutoSaveTicks = 4000;
/*   74 */   public boolean ofLagometer = false;
/*   75 */   public boolean ofProfiler = false;
/*   76 */   public boolean ofWeather = true;
/*   77 */   public boolean ofSky = true;
/*   78 */   public boolean ofStars = true;
/*   79 */   public boolean ofSunMoon = true;
/*   80 */   public int ofChunkUpdates = 1;
/*   81 */   public int ofChunkLoading = 0;
/*   82 */   public boolean ofChunkUpdatesDynamic = false;
/*   83 */   public int ofTime = 0;
/*   84 */   public boolean ofClearWater = false;
/*   85 */   public boolean ofDepthFog = true;
/*   86 */   public boolean ofBetterSnow = false;
/*   87 */   public String ofFullscreenMode = "Стандарт";
/*   88 */   public boolean ofSwampColors = true;
/*   89 */   public boolean ofRandomMobs = true;
/*   90 */   public boolean ofSmoothBiomes = true;
/*   91 */   public boolean ofCustomFonts = true;
/*   92 */   public boolean ofCustomColors = true;
/*   93 */   public boolean ofCustomSky = true;
/*   94 */   public boolean ofShowCapes = true;
/*   95 */   public int ofConnectedTextures = 2;
/*   96 */   public boolean ofNaturalTextures = false;
/*      */ 
/*   98 */   public int ofAnimatedWater = 0;
/*   99 */   public int ofAnimatedLava = 0;
/*  100 */   public boolean ofAnimatedFire = true;
/*  101 */   public boolean ofAnimatedPortal = true;
/*  102 */   public boolean ofAnimatedRedstone = true;
/*  103 */   public boolean ofAnimatedExplosion = true;
/*  104 */   public boolean ofAnimatedFlame = true;
/*  105 */   public boolean ofAnimatedSmoke = true;
/*  106 */   public boolean ofVoidParticles = true;
/*  107 */   public boolean ofWaterParticles = true;
/*  108 */   public boolean ofRainSplash = true;
/*  109 */   public boolean ofPortalParticles = true;
/*  110 */   public boolean ofPotionParticles = true;
/*  111 */   public boolean ofDrippingWaterLava = true;
/*  112 */   public boolean ofAnimatedTerrain = true;
/*  113 */   public boolean ofAnimatedItems = true;
/*  114 */   public boolean ofAnimatedTextures = true;
/*      */   public static final int DEFAULT = 0;
/*      */   public static final int FAST = 1;
/*      */   public static final int FANCY = 2;
/*      */   public static final int Выкл = 3;
/*      */   public static final int ANIM_ON = 0;
/*      */   public static final int ANIM_GENERATED = 1;
/*      */   public static final int ANIM_OFF = 2;
/*      */   public static final int CL_DEFAULT = 0;
/*      */   public static final int CL_SMOOTH = 1;
/*      */   public static final int CL_THREADED = 2;
/*      */   public static final String DEFAULT_STR = "Стандарт";
/*      */   public KeyBinding ofKeyBindZoom;
/*  132 */   public String skin = "Стандарт";
/*      */   public int chatVisibility;
/*  134 */   public boolean chatColours = true;
/*  135 */   public boolean chatLinks = true;
/*  136 */   public boolean chatLinksPrompt = true;
/*  137 */   public float chatOpacity = 1.0F;
/*  138 */   public boolean serverTextures = true;
/*  139 */   public boolean snooperEnabled = true;
/*      */   public boolean fullScreen;
/*  141 */   public boolean enableVsync = true;
/*      */   public boolean hideServerAddress;
/*      */   public boolean advancedItemTooltips;
/*  150 */   public boolean pauseOnLostFocus = true;
/*      */ 
/*  153 */   public boolean showCape = true;
/*      */   public boolean touchscreen;
/*      */   public int overrideWidth;
/*      */   public int overrideHeight;
/*  157 */   public boolean heldItemTooltips = true;
/*  158 */   public float chatScale = 1.0F;
/*  159 */   public float chatWidth = 1.0F;
/*  160 */   public float chatHeightUnfocused = 0.443662F;
/*  161 */   public float chatHeightFocused = 1.0F;
/*  162 */   public KeyBinding keyBindForward = new KeyBinding("key.forward", 17);
/*  163 */   public KeyBinding keyBindLeft = new KeyBinding("key.left", 30);
/*  164 */   public KeyBinding keyBindBack = new KeyBinding("key.back", 31);
/*  165 */   public KeyBinding keyBindRight = new KeyBinding("key.right", 32);
/*  166 */   public KeyBinding keyBindJump = new KeyBinding("key.jump", 57);
/*  167 */   public KeyBinding keyBindInventory = new KeyBinding("key.inventory", 18);
/*  168 */   public KeyBinding keyBindDrop = new KeyBinding("key.drop", 16);
/*  169 */   public KeyBinding keyBindChat = new KeyBinding("key.chat", 20);
/*  170 */   public KeyBinding keyBindSneak = new KeyBinding("key.sneak", 42);
/*  171 */   public KeyBinding keyBindAttack = new KeyBinding("key.attack", -100);
/*  172 */   public KeyBinding keyBindUseItem = new KeyBinding("key.use", -99);
/*  173 */   public KeyBinding keyBindPlayerList = new KeyBinding("key.playerlist", 15);
/*  174 */   public KeyBinding keyBindPickBlock = new KeyBinding("key.pickItem", -98);
/*  175 */   public KeyBinding keyBindCommand = new KeyBinding("key.command", 53);
/*      */   public KeyBinding[] keyBindings;
/*      */   protected Minecraft mc;
/*      */   private File optionsFile;
/*      */   public int difficulty;
/*      */   public boolean hideGUI;
/*      */   public int thirdPersonView;
/*      */   public boolean showDebugInfo;
/*      */   public boolean showDebugProfilerChart;
/*      */   public String lastServer;
/*      */   public boolean noclip;
/*      */   public boolean smoothCamera;
/*      */   public boolean debugCamEnable;
/*      */   public float noclipRate;
/*      */   public float debugCamRate;
/*      */   public float fovSetting;
/*      */   public float gammaSetting;
/*      */   public int guiScale;
/*      */   public int particleSetting;
/*      */   public String language;
/*      */   private File optionsFileOF;
/*      */ 
/*      */   public GameSettings(Minecraft par1Minecraft, File par2File)
/*      */   {
/*  221 */     this.renderDistance = 1;
/*  222 */     this.limitFramerate = 0;
/*      */ 
/*  224 */     this.ofKeyBindZoom = new KeyBinding("Приближение", 29);
/*  225 */     this.keyBindings = new KeyBinding[] { this.keyBindAttack, this.keyBindUseItem, this.keyBindForward, this.keyBindLeft, this.keyBindBack, this.keyBindRight, this.keyBindJump, this.keyBindSneak, this.keyBindDrop, this.keyBindInventory, this.keyBindChat, this.keyBindPlayerList, this.keyBindPickBlock, this.ofKeyBindZoom, this.keyBindCommand };
/*      */ 
/*  230 */     this.difficulty = 2;
/*  231 */     this.lastServer = "";
/*  232 */     this.noclipRate = 1.0F;
/*  233 */     this.debugCamRate = 1.0F;
/*  234 */     this.language = "en_US";
/*  235 */     this.mc = par1Minecraft;
/*  236 */     this.optionsFile = new File(par2File, "options.txt");
/*      */ 
/*  238 */     this.optionsFileOF = new File(par2File, "optionsof.txt");
/*  239 */     loadOptions();
/*      */ 
/*  241 */     Config.initGameSettings(this);
/*      */   }
/*      */ 
/*      */   public GameSettings()
/*      */   {
/*  247 */     this.renderDistance = 1;
/*  248 */     this.limitFramerate = 0;
/*      */ 
/*  250 */     this.ofKeyBindZoom = new KeyBinding("Приближение", 29);
/*  251 */     this.keyBindings = new KeyBinding[] { this.keyBindAttack, this.keyBindUseItem, this.keyBindForward, this.keyBindLeft, this.keyBindBack, this.keyBindRight, this.keyBindJump, this.keyBindSneak, this.keyBindDrop, this.keyBindInventory, this.keyBindChat, this.keyBindPlayerList, this.keyBindPickBlock, this.ofKeyBindZoom, this.keyBindCommand };
/*      */ 
/*  256 */     this.difficulty = 2;
/*  257 */     this.lastServer = "";
/*  258 */     this.noclipRate = 1.0F;
/*  259 */     this.debugCamRate = 1.0F;
/*  260 */     this.language = "en_US";
/*      */   }
/*      */ 
/*      */   public String getKeyBindingDescription(int par1)
/*      */   {
/*  265 */     return I18n.getString(this.keyBindings[par1].keyDescription);
/*      */   }
/*      */ 
/*      */   public String getOptionDisplayString(int par1)
/*      */   {
/*  273 */     int var2 = this.keyBindings[par1].keyCode;
/*  274 */     return getKeyDisplayString(var2);
/*      */   }
/*      */ 
/*      */   public static String getKeyDisplayString(int par0)
/*      */   {
/*  282 */     return par0 < 0 ? I18n.getStringParams("key.mouseButton", new Object[] { Integer.valueOf(par0 + 101) }) : Keyboard.getKeyName(par0);
/*      */   }
/*      */ 
/*      */   public static boolean isKeyDown(KeyBinding par0KeyBinding)
/*      */   {
/*  290 */     return par0KeyBinding.keyCode < 0 ? Mouse.isButtonDown(par0KeyBinding.keyCode + 100) : Keyboard.isKeyDown(par0KeyBinding.keyCode);
/*      */   }
/*      */ 
/*      */   public void setKeyBinding(int par1, int par2)
/*      */   {
/*  298 */     this.keyBindings[par1].keyCode = par2;
/*  299 */     saveOptions();
/*      */   }
/*      */ 
/*      */   public void setOptionFloatValue(EnumOptions par1EnumOptions, float par2)
/*      */   {
/*  307 */     if (par1EnumOptions == EnumOptions.MUSIC)
/*      */     {
/*  309 */       this.musicVolume = par2;
/*  310 */       this.mc.sndManager.onSoundOptionsChanged();
/*      */     }
/*      */ 
/*  313 */     if (par1EnumOptions == EnumOptions.SOUND)
/*      */     {
/*  315 */       this.soundVolume = par2;
/*  316 */       this.mc.sndManager.onSoundOptionsChanged();
/*      */     }
/*      */ 
/*  319 */     if (par1EnumOptions == EnumOptions.SENSITIVITY)
/*      */     {
/*  321 */       this.mouseSensitivity = par2;
/*      */     }
/*      */ 
/*  324 */     if (par1EnumOptions == EnumOptions.FOV)
/*      */     {
/*  326 */       this.fovSetting = par2;
/*      */     }
/*      */ 
/*  329 */     if (par1EnumOptions == EnumOptions.GAMMA)
/*      */     {
/*  331 */       this.gammaSetting = par2;
/*      */     }
/*      */ 
/*  335 */     if (par1EnumOptions == EnumOptions.CLOUD_HEIGHT)
/*      */     {
/*  337 */       this.ofCloudsHeight = par2;
/*      */     }
/*  339 */     if (par1EnumOptions == EnumOptions.AO_LEVEL)
/*      */     {
/*  341 */       this.ofAoLevel = par2;
/*  342 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  344 */     if (par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE)
/*      */     {
/*  346 */       int ofRenderDistanceFineOld = this.ofRenderDistanceFine;
/*  347 */       this.ofRenderDistanceFine = (32 + (int)(par2 * 480.0F));
/*  348 */       this.ofRenderDistanceFine = (this.ofRenderDistanceFine >> 4 << 4);
/*  349 */       this.ofRenderDistanceFine = Config.limit(this.ofRenderDistanceFine, 32, 512);
/*      */ 
/*  351 */       this.renderDistance = fineToRenderDistance(this.ofRenderDistanceFine);
/*      */ 
/*  353 */       if (this.ofRenderDistanceFine != ofRenderDistanceFineOld)
/*  354 */         this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  356 */     if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE)
/*      */     {
/*  358 */       this.ofLimitFramerateFine = ((int)(par2 * 200.0F));
/*  359 */       this.enableVsync = false;
/*  360 */       if (this.ofLimitFramerateFine < 5)
/*      */       {
/*  362 */         this.enableVsync = true;
/*  363 */         this.ofLimitFramerateFine = 0;
/*      */       }
/*  365 */       if (this.ofLimitFramerateFine > 199)
/*      */       {
/*  367 */         this.enableVsync = false;
/*  368 */         this.ofLimitFramerateFine = 0;
/*      */       }
/*      */ 
/*  371 */       if (this.ofLimitFramerateFine > 30)
/*  372 */         this.ofLimitFramerateFine = (this.ofLimitFramerateFine / 5 * 5);
/*  373 */       if (this.ofLimitFramerateFine > 100) {
/*  374 */         this.ofLimitFramerateFine = (this.ofLimitFramerateFine / 10 * 10);
/*      */       }
/*  376 */       this.limitFramerate = fineToLimitFramerate(this.ofLimitFramerateFine);
/*      */ 
/*  378 */       updateVSync();
/*      */     }
/*      */ 
/*  382 */     if (par1EnumOptions == EnumOptions.CHAT_OPACITY)
/*      */     {
/*  384 */       this.chatOpacity = par2;
/*  385 */       this.mc.ingameGUI.getChatGUI().func_96132_b();
/*      */     }
/*      */ 
/*  388 */     if (par1EnumOptions == EnumOptions.CHAT_HEIGHT_FOCUSED)
/*      */     {
/*  390 */       this.chatHeightFocused = par2;
/*  391 */       this.mc.ingameGUI.getChatGUI().func_96132_b();
/*      */     }
/*      */ 
/*  394 */     if (par1EnumOptions == EnumOptions.CHAT_HEIGHT_UNFOCUSED)
/*      */     {
/*  396 */       this.chatHeightUnfocused = par2;
/*  397 */       this.mc.ingameGUI.getChatGUI().func_96132_b();
/*      */     }
/*      */ 
/*  400 */     if (par1EnumOptions == EnumOptions.CHAT_WIDTH)
/*      */     {
/*  402 */       this.chatWidth = par2;
/*  403 */       this.mc.ingameGUI.getChatGUI().func_96132_b();
/*      */     }
/*      */ 
/*  406 */     if (par1EnumOptions == EnumOptions.CHAT_SCALE)
/*      */     {
/*  408 */       this.chatScale = par2;
/*  409 */       this.mc.ingameGUI.getChatGUI().func_96132_b();
/*      */     }
/*      */   }
/*      */ 
/*      */   private void updateWaterOpacity()
/*      */   {
/*  421 */     if (this.mc.getIntegratedServer() != null) {
/*  422 */       Config.waterOpacityChanged = true;
/*      */     }
/*  424 */     int opacity = 3;
/*  425 */     if (this.ofClearWater) {
/*  426 */       opacity = 1;
/*      */     }
/*  428 */     Block.waterStill.setLightOpacity(opacity);
/*  429 */     Block.waterMoving.setLightOpacity(opacity);
/*      */ 
/*  431 */     if (this.mc.theWorld == null)
/*  432 */       return;
/*  433 */     IChunkProvider cp = this.mc.theWorld.chunkProvider;
/*  434 */     if (cp == null) {
/*  435 */       return;
/*      */     }
/*  437 */     for (int x = -512; x < 512; x++)
/*      */     {
/*  439 */       for (int z = -512; z < 512; z++)
/*      */       {
/*  441 */         if (cp.chunkExists(x, z))
/*      */         {
/*  444 */           Chunk c = cp.provideChunk(x, z);
/*  445 */           if ((c != null) && (!(c instanceof EmptyChunk)))
/*      */           {
/*  449 */             ExtendedBlockStorage[] ebss = c.getBlockStorageArray();
/*  450 */             for (int i = 0; i < ebss.length; i++)
/*      */             {
/*  452 */               ExtendedBlockStorage ebs = ebss[i];
/*  453 */               if (ebs != null)
/*      */               {
/*  455 */                 NibbleArray na = ebs.getSkylightArray();
/*  456 */                 if (na != null)
/*      */                 {
/*  459 */                   byte[] data = na.data;
/*  460 */                   for (int d = 0; d < data.length; d++)
/*  461 */                     data[d] = 0; 
/*      */                 }
/*      */               }
/*      */             }
/*  464 */             c.generateSkylightMap();
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*  468 */     this.mc.renderGlobal.loadRenderers();
/*      */   }
/*      */ 
/*      */   public void updateChunkLoading()
/*      */   {
/*  476 */     switch (this.ofChunkLoading)
/*      */     {
/*      */     case 1:
/*  479 */       WrUpdates.setWrUpdater(new WrUpdaterSmooth());
/*  480 */       break;
/*      */     case 2:
/*  482 */       WrUpdates.setWrUpdater(new WrUpdaterThreaded());
/*  483 */       break;
/*      */     default:
/*  485 */       WrUpdates.setWrUpdater(null);
/*      */     }
/*      */ 
/*  489 */     if (this.mc.renderGlobal != null)
/*  490 */       this.mc.renderGlobal.loadRenderers();
/*      */   }
/*      */ 
/*      */   public void setAllAnimations(boolean flag)
/*      */   {
/*  495 */     int animVal = flag ? 0 : 2;
/*      */ 
/*  497 */     this.ofAnimatedWater = animVal;
/*  498 */     this.ofAnimatedLava = animVal;
/*  499 */     this.ofAnimatedFire = flag;
/*  500 */     this.ofAnimatedPortal = flag;
/*  501 */     this.ofAnimatedRedstone = flag;
/*  502 */     this.ofAnimatedExplosion = flag;
/*  503 */     this.ofAnimatedFlame = flag;
/*  504 */     this.ofAnimatedSmoke = flag;
/*  505 */     this.ofVoidParticles = flag;
/*  506 */     this.ofWaterParticles = flag;
/*  507 */     this.ofRainSplash = flag;
/*  508 */     this.ofPortalParticles = flag;
/*  509 */     this.ofPotionParticles = flag;
/*  510 */     this.particleSetting = (flag ? 0 : 2);
/*  511 */     this.ofDrippingWaterLava = flag;
/*  512 */     this.ofAnimatedTerrain = flag;
/*  513 */     this.ofAnimatedItems = flag;
/*  514 */     this.ofAnimatedTextures = flag;
/*      */   }
/*      */ 
/*      */   public void setOptionValue(EnumOptions par1EnumOptions, int par2)
/*      */   {
/*  523 */     if (par1EnumOptions == EnumOptions.INVERT_MOUSE)
/*      */     {
/*  525 */       this.invertMouse = (!this.invertMouse);
/*      */     }
/*      */ 
/*  528 */     if (par1EnumOptions == EnumOptions.RENDER_DISTANCE)
/*      */     {
/*  530 */       this.renderDistance = (this.renderDistance + par2 & 0x3);
/*      */ 
/*  532 */       this.ofRenderDistanceFine = renderDistanceToFine(this.renderDistance);
/*      */     }
/*      */ 
/*  535 */     if (par1EnumOptions == EnumOptions.GUI_SCALE)
/*      */     {
/*  537 */       this.guiScale = (this.guiScale + par2 & 0x3);
/*      */     }
/*      */ 
/*  540 */     if (par1EnumOptions == EnumOptions.PARTICLES)
/*      */     {
/*  542 */       this.particleSetting = ((this.particleSetting + par2) % 3);
/*      */     }
/*      */ 
/*  545 */     if (par1EnumOptions == EnumOptions.VIEW_BOBBING)
/*      */     {
/*  547 */       this.viewBobbing = (!this.viewBobbing);
/*      */     }
/*      */ 
/*  550 */     if (par1EnumOptions == EnumOptions.RENDER_CLOUDS)
/*      */     {
/*  552 */       this.clouds = (!this.clouds);
/*      */     }
/*      */ 
/*  555 */     if (par1EnumOptions == EnumOptions.ADVANCED_OPENGL)
/*      */     {
/*  558 */       if (!Config.isOcclusionAvailable())
/*      */       {
/*  560 */         this.ofOcclusionFancy = false;
/*  561 */         this.advancedOpengl = false;
/*      */       }
/*  566 */       else if (!this.advancedOpengl)
/*      */       {
/*  568 */         this.advancedOpengl = true;
/*  569 */         this.ofOcclusionFancy = false;
/*      */       }
/*  573 */       else if (!this.ofOcclusionFancy) {
/*  574 */         this.ofOcclusionFancy = true;
/*      */       }
/*      */       else {
/*  577 */         this.ofOcclusionFancy = false;
/*  578 */         this.advancedOpengl = false;
/*      */       }
/*      */ 
/*  583 */       this.mc.renderGlobal.setAllRenderersVisible();
/*      */     }
/*      */ 
/*  586 */     if (par1EnumOptions == EnumOptions.ANAGLYPH)
/*      */     {
/*  588 */       this.anaglyph = (!this.anaglyph);
/*  589 */       this.mc.refreshResources();
/*      */     }
/*      */ 
/*  592 */     if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT)
/*      */     {
/*  594 */       this.limitFramerate = ((this.limitFramerate + par2 + 3) % 3);
/*      */ 
/*  596 */       this.ofLimitFramerateFine = limitFramerateToFine(this.limitFramerate);
/*      */     }
/*      */ 
/*  599 */     if (par1EnumOptions == EnumOptions.DIFFICULTY)
/*      */     {
/*  601 */       this.difficulty = (this.difficulty + par2 & 0x3);
/*      */     }
/*      */ 
/*  604 */     if (par1EnumOptions == EnumOptions.GRAPHICS)
/*      */     {
/*  606 */       this.fancyGraphics = (!this.fancyGraphics);
/*  607 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*      */ 
/*  610 */     if (par1EnumOptions == EnumOptions.AMBIENT_OCCLUSION)
/*      */     {
/*  612 */       this.ambientOcclusion = ((this.ambientOcclusion + par2) % 3);
/*  613 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*      */ 
/*  617 */     if (par1EnumOptions == EnumOptions.FOG_FANCY)
/*      */     {
/*  619 */       switch (this.ofFogType)
/*      */       {
/*      */       case 1:
/*  622 */         this.ofFogType = 2;
/*      */ 
/*  624 */         if (!Config.isFancyFogAvailable())
/*  625 */           this.ofFogType = 3; break;
/*      */       case 2:
/*  628 */         this.ofFogType = 3;
/*  629 */         break;
/*      */       case 3:
/*  631 */         this.ofFogType = 1;
/*  632 */         break;
/*      */       default:
/*  634 */         this.ofFogType = 1;
/*      */       }
/*      */     }
/*  637 */     if (par1EnumOptions == EnumOptions.FOG_START)
/*      */     {
/*  639 */       this.ofFogStart += 0.2F;
/*  640 */       if (this.ofFogStart > 0.81F)
/*  641 */         this.ofFogStart = 0.2F;
/*      */     }
/*  643 */     if (par1EnumOptions == EnumOptions.MIPMAP_LEVEL)
/*      */     {
/*  645 */       this.ofMipmapLevel += 1;
/*  646 */       if (this.ofMipmapLevel > 4) {
/*  647 */         this.ofMipmapLevel = 0;
/*      */       }
/*  649 */       TextureUtils.refreshBlockTextures();
/*      */     }
/*      */ 
/*  653 */     if (par1EnumOptions == EnumOptions.MIPMAP_TYPE)
/*      */     {
/*  655 */       this.ofMipmapType += 1;
/*  656 */       if (this.ofMipmapType > 3) {
/*  657 */         this.ofMipmapType = 0;
/*      */       }
/*  659 */       TextureUtils.refreshBlockTextures();
/*      */     }
/*      */ 
/*  663 */     if (par1EnumOptions == EnumOptions.LOAD_FAR)
/*      */     {
/*  665 */       this.ofLoadFar = (!this.ofLoadFar);
/*      */ 
/*  667 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  669 */     if (par1EnumOptions == EnumOptions.PRELOADED_CHUNKS)
/*      */     {
/*  671 */       this.ofPreloadedChunks += 2;
/*  672 */       if (this.ofPreloadedChunks > 8) {
/*  673 */         this.ofPreloadedChunks = 0;
/*      */       }
/*  675 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  677 */     if (par1EnumOptions == EnumOptions.SMOOTH_FPS)
/*      */     {
/*  679 */       this.ofSmoothFps = (!this.ofSmoothFps);
/*      */     }
/*  681 */     if (par1EnumOptions == EnumOptions.SMOOTH_WORLD)
/*      */     {
/*  683 */       this.ofSmoothWorld = (!this.ofSmoothWorld);
/*  684 */       Config.updateThreadPriorities();
/*      */     }
/*  686 */     if (par1EnumOptions == EnumOptions.CLOUDS)
/*      */     {
/*  688 */       this.ofClouds += 1;
/*  689 */       if (this.ofClouds > 3)
/*  690 */         this.ofClouds = 0;
/*      */     }
/*  692 */     if (par1EnumOptions == EnumOptions.TREES)
/*      */     {
/*  694 */       this.ofTrees += 1;
/*  695 */       if (this.ofTrees > 2)
/*  696 */         this.ofTrees = 0;
/*  697 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  699 */     if (par1EnumOptions == EnumOptions.GRASS)
/*      */     {
/*  701 */       this.ofGrass += 1;
/*  702 */       if (this.ofGrass > 2)
/*  703 */         this.ofGrass = 0;
/*  704 */       net.minecraft.client.renderer.RenderBlocks.fancyGrass = Config.isGrassFancy();
/*  705 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  707 */     if (par1EnumOptions == EnumOptions.DROPPED_ITEMS)
/*      */     {
/*  709 */       this.ofDroppedItems += 1;
/*  710 */       if (this.ofDroppedItems > 2)
/*  711 */         this.ofDroppedItems = 0;
/*      */     }
/*  713 */     if (par1EnumOptions == EnumOptions.RAIN)
/*      */     {
/*  715 */       this.ofRain += 1;
/*  716 */       if (this.ofRain > 3)
/*  717 */         this.ofRain = 0;
/*      */     }
/*  719 */     if (par1EnumOptions == EnumOptions.WATER)
/*      */     {
/*  721 */       this.ofWater += 1;
/*  722 */       if (this.ofWater > 2)
/*  723 */         this.ofWater = 0;
/*      */     }
/*  725 */     if (par1EnumOptions == EnumOptions.ANIMATED_WATER)
/*      */     {
/*  727 */       this.ofAnimatedWater += 1;
/*  728 */       if (this.ofAnimatedWater > 2) {
/*  729 */         this.ofAnimatedWater = 0;
/*      */       }
/*      */     }
/*  732 */     if (par1EnumOptions == EnumOptions.ANIMATED_LAVA)
/*      */     {
/*  734 */       this.ofAnimatedLava += 1;
/*  735 */       if (this.ofAnimatedLava > 2) {
/*  736 */         this.ofAnimatedLava = 0;
/*      */       }
/*      */     }
/*  739 */     if (par1EnumOptions == EnumOptions.ANIMATED_FIRE)
/*      */     {
/*  741 */       this.ofAnimatedFire = (!this.ofAnimatedFire);
/*      */     }
/*      */ 
/*  744 */     if (par1EnumOptions == EnumOptions.ANIMATED_PORTAL)
/*      */     {
/*  746 */       this.ofAnimatedPortal = (!this.ofAnimatedPortal);
/*      */     }
/*      */ 
/*  749 */     if (par1EnumOptions == EnumOptions.ANIMATED_REDSTONE)
/*      */     {
/*  751 */       this.ofAnimatedRedstone = (!this.ofAnimatedRedstone);
/*      */     }
/*  753 */     if (par1EnumOptions == EnumOptions.ANIMATED_EXPLOSION)
/*      */     {
/*  755 */       this.ofAnimatedExplosion = (!this.ofAnimatedExplosion);
/*      */     }
/*  757 */     if (par1EnumOptions == EnumOptions.ANIMATED_FLAME)
/*      */     {
/*  759 */       this.ofAnimatedFlame = (!this.ofAnimatedFlame);
/*      */     }
/*  761 */     if (par1EnumOptions == EnumOptions.ANIMATED_SMOKE)
/*      */     {
/*  763 */       this.ofAnimatedSmoke = (!this.ofAnimatedSmoke);
/*      */     }
/*  765 */     if (par1EnumOptions == EnumOptions.VOID_PARTICLES)
/*      */     {
/*  767 */       this.ofVoidParticles = (!this.ofVoidParticles);
/*      */     }
/*  769 */     if (par1EnumOptions == EnumOptions.WATER_PARTICLES)
/*      */     {
/*  771 */       this.ofWaterParticles = (!this.ofWaterParticles);
/*      */     }
/*  773 */     if (par1EnumOptions == EnumOptions.PORTAL_PARTICLES)
/*      */     {
/*  775 */       this.ofPortalParticles = (!this.ofPortalParticles);
/*      */     }
/*  777 */     if (par1EnumOptions == EnumOptions.POTION_PARTICLES)
/*      */     {
/*  779 */       this.ofPotionParticles = (!this.ofPotionParticles);
/*      */     }
/*  781 */     if (par1EnumOptions == EnumOptions.DRIPPING_WATER_LAVA)
/*      */     {
/*  783 */       this.ofDrippingWaterLava = (!this.ofDrippingWaterLava);
/*      */     }
/*  785 */     if (par1EnumOptions == EnumOptions.ANIMATED_TERRAIN)
/*      */     {
/*  787 */       this.ofAnimatedTerrain = (!this.ofAnimatedTerrain);
/*      */     }
/*      */ 
/*  790 */     if (par1EnumOptions == EnumOptions.ANIMATED_TEXTURES)
/*      */     {
/*  792 */       this.ofAnimatedTextures = (!this.ofAnimatedTextures);
/*      */     }
/*      */ 
/*  795 */     if (par1EnumOptions == EnumOptions.ANIMATED_ITEMS)
/*      */     {
/*  797 */       this.ofAnimatedItems = (!this.ofAnimatedItems);
/*      */     }
/*      */ 
/*  800 */     if (par1EnumOptions == EnumOptions.RAIN_SPLASH)
/*      */     {
/*  802 */       this.ofRainSplash = (!this.ofRainSplash);
/*      */     }
/*  804 */     if (par1EnumOptions == EnumOptions.LAGOMETER)
/*      */     {
/*  806 */       this.ofLagometer = (!this.ofLagometer);
/*      */     }
/*  808 */     if (par1EnumOptions == EnumOptions.AUTOSAVE_TICKS)
/*      */     {
/*  810 */       this.ofAutoSaveTicks *= 10;
/*  811 */       if (this.ofAutoSaveTicks > 40000)
/*  812 */         this.ofAutoSaveTicks = 40;
/*      */     }
/*  814 */     if (par1EnumOptions == EnumOptions.BETTER_GRASS)
/*      */     {
/*  816 */       this.ofBetterGrass += 1;
/*  817 */       if (this.ofBetterGrass > 3)
/*  818 */         this.ofBetterGrass = 1;
/*  819 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  821 */     if (par1EnumOptions == EnumOptions.CONNECTED_TEXTURES)
/*      */     {
/*  823 */       this.ofConnectedTextures += 1;
/*  824 */       if (this.ofConnectedTextures > 3)
/*  825 */         this.ofConnectedTextures = 1;
/*  826 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  828 */     if (par1EnumOptions == EnumOptions.WEATHER)
/*      */     {
/*  830 */       this.ofWeather = (!this.ofWeather);
/*      */     }
/*  832 */     if (par1EnumOptions == EnumOptions.SKY)
/*      */     {
/*  834 */       this.ofSky = (!this.ofSky);
/*      */     }
/*  836 */     if (par1EnumOptions == EnumOptions.STARS)
/*      */     {
/*  838 */       this.ofStars = (!this.ofStars);
/*      */     }
/*  840 */     if (par1EnumOptions == EnumOptions.SUN_MOON)
/*      */     {
/*  842 */       this.ofSunMoon = (!this.ofSunMoon);
/*      */     }
/*  844 */     if (par1EnumOptions == EnumOptions.CHUNK_UPDATES)
/*      */     {
/*  846 */       this.ofChunkUpdates += 1;
/*  847 */       if (this.ofChunkUpdates > 5)
/*  848 */         this.ofChunkUpdates = 1;
/*      */     }
/*  850 */     if (par1EnumOptions == EnumOptions.CHUNK_LOADING)
/*      */     {
/*  852 */       this.ofChunkLoading += 1;
/*  853 */       if (this.ofChunkLoading > 2) {
/*  854 */         this.ofChunkLoading = 0;
/*      */       }
/*  856 */       updateChunkLoading();
/*      */     }
/*  858 */     if (par1EnumOptions == EnumOptions.CHUNK_UPDATES_DYNAMIC)
/*      */     {
/*  860 */       this.ofChunkUpdatesDynamic = (!this.ofChunkUpdatesDynamic);
/*      */     }
/*  862 */     if (par1EnumOptions == EnumOptions.TIME)
/*      */     {
/*  864 */       this.ofTime += 1;
/*  865 */       if (this.ofTime > 3)
/*  866 */         this.ofTime = 0;
/*      */     }
/*  868 */     if (par1EnumOptions == EnumOptions.CLEAR_WATER)
/*      */     {
/*  870 */       this.ofClearWater = (!this.ofClearWater);
/*  871 */       updateWaterOpacity();
/*      */     }
/*  873 */     if (par1EnumOptions == EnumOptions.DEPTH_FOG)
/*      */     {
/*  875 */       this.ofDepthFog = (!this.ofDepthFog);
/*      */     }
/*  877 */     if (par1EnumOptions == EnumOptions.AA_LEVEL)
/*      */     {
/*  879 */       int[] aaLevels = { 0, 2, 4, 6, 8, 12, 16 };
/*  880 */       boolean found = false;
/*  881 */       for (int l = 0; l < aaLevels.length - 1; l++)
/*      */       {
/*  883 */         if (this.ofAaLevel == aaLevels[l])
/*      */         {
/*  885 */           this.ofAaLevel = aaLevels[(l + 1)];
/*  886 */           found = true;
/*  887 */           break;
/*      */         }
/*      */       }
/*  890 */       if (!found)
/*  891 */         this.ofAaLevel = 0;
/*      */     }
/*  893 */     if (par1EnumOptions == EnumOptions.AF_LEVEL)
/*      */     {
/*  895 */       this.ofAfLevel *= 2;
/*  896 */       if (this.ofAfLevel > 16)
/*  897 */         this.ofAfLevel = 1;
/*  898 */       this.ofAfLevel = Config.limit(this.ofAfLevel, 1, 16);
/*      */ 
/*  900 */       TextureUtils.refreshBlockTextures();
/*      */ 
/*  902 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  904 */     if (par1EnumOptions == EnumOptions.PROFILER)
/*      */     {
/*  906 */       this.ofProfiler = (!this.ofProfiler);
/*      */     }
/*  908 */     if (par1EnumOptions == EnumOptions.BETTER_SNOW)
/*      */     {
/*  910 */       this.ofBetterSnow = (!this.ofBetterSnow);
/*  911 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  913 */     if (par1EnumOptions == EnumOptions.SWAMP_COLORS)
/*      */     {
/*  915 */       this.ofSwampColors = (!this.ofSwampColors);
/*  916 */       CustomColorizer.updateUseDefaultColorMultiplier();
/*  917 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  919 */     if (par1EnumOptions == EnumOptions.RANDOM_MOBS)
/*      */     {
/*  921 */       this.ofRandomMobs = (!this.ofRandomMobs);
/*  922 */       RandomMobs.resetTextures();
/*      */     }
/*  924 */     if (par1EnumOptions == EnumOptions.SMOOTH_BIOMES)
/*      */     {
/*  926 */       this.ofSmoothBiomes = (!this.ofSmoothBiomes);
/*  927 */       CustomColorizer.updateUseDefaultColorMultiplier();
/*  928 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  930 */     if (par1EnumOptions == EnumOptions.CUSTOM_FONTS)
/*      */     {
/*  932 */       this.ofCustomFonts = (!this.ofCustomFonts);
/*      */ 
/*  934 */       this.mc.fontRenderer.onResourceManagerReload(Config.getResourceManager());
/*  935 */       this.mc.standardGalacticFontRenderer.onResourceManagerReload(Config.getResourceManager());
/*      */     }
/*  937 */     if (par1EnumOptions == EnumOptions.CUSTOM_COLORS)
/*      */     {
/*  939 */       this.ofCustomColors = (!this.ofCustomColors);
/*      */ 
/*  941 */       CustomColorizer.update();
/*      */ 
/*  943 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  945 */     if (par1EnumOptions == EnumOptions.CUSTOM_SKY)
/*      */     {
/*  947 */       this.ofCustomSky = (!this.ofCustomSky);
/*      */ 
/*  949 */       CustomSky.update();
/*      */     }
/*  951 */     if (par1EnumOptions == EnumOptions.SHOW_CAPES)
/*      */     {
/*  953 */       this.ofShowCapes = (!this.ofShowCapes);
/*  954 */       this.mc.renderGlobal.updateCapes();
/*      */     }
/*  956 */     if (par1EnumOptions == EnumOptions.NATURAL_TEXTURES)
/*      */     {
/*  958 */       this.ofNaturalTextures = (!this.ofNaturalTextures);
/*      */ 
/*  960 */       NaturalTextures.update();
/*      */ 
/*  962 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  964 */     if (par1EnumOptions == EnumOptions.LAZY_CHUNK_LOADING)
/*      */     {
/*  966 */       this.ofLazyChunkLoading = (!this.ofLazyChunkLoading);
/*  967 */       this.mc.renderGlobal.loadRenderers();
/*      */     }
/*  969 */     if (par1EnumOptions == EnumOptions.FULLSCREEN_MODE)
/*      */     {
/*  971 */       List modeList = Arrays.asList(Config.getFullscreenModes());
/*  972 */       if (this.ofFullscreenMode.equals("Стандарт"))
/*      */       {
/*  974 */         this.ofFullscreenMode = ((String)modeList.get(0));
/*      */       }
/*      */       else
/*      */       {
/*  978 */         int index = modeList.indexOf(this.ofFullscreenMode);
/*  979 */         if (index < 0)
/*      */         {
/*  981 */           this.ofFullscreenMode = "Стандарт";
/*      */         }
/*      */         else
/*      */         {
/*  985 */           index++;
/*  986 */           if (index >= modeList.size())
/*  987 */             this.ofFullscreenMode = "Стандарт";
/*      */           else
/*  989 */             this.ofFullscreenMode = ((String)modeList.get(index));
/*      */         }
/*      */       }
/*      */     }
/*  993 */     if (par1EnumOptions == EnumOptions.HELD_ITEM_TOOLTIPS)
/*      */     {
/*  995 */       this.heldItemTooltips = (!this.heldItemTooltips);
/*      */     }
/*      */ 
/*  999 */     if (par1EnumOptions == EnumOptions.CHAT_VISIBILITY)
/*      */     {
/* 1001 */       this.chatVisibility = ((this.chatVisibility + par2) % 3);
/*      */     }
/*      */ 
/* 1004 */     if (par1EnumOptions == EnumOptions.CHAT_COLOR)
/*      */     {
/* 1006 */       this.chatColours = (!this.chatColours);
/*      */     }
/*      */ 
/* 1009 */     if (par1EnumOptions == EnumOptions.CHAT_LINKS)
/*      */     {
/* 1011 */       this.chatLinks = (!this.chatLinks);
/*      */     }
/*      */ 
/* 1014 */     if (par1EnumOptions == EnumOptions.CHAT_LINKS_PROMPT)
/*      */     {
/* 1016 */       this.chatLinksPrompt = (!this.chatLinksPrompt);
/*      */     }
/*      */ 
/* 1019 */     if (par1EnumOptions == EnumOptions.USE_SERVER_TEXTURES)
/*      */     {
/* 1021 */       this.serverTextures = (!this.serverTextures);
/*      */     }
/*      */ 
/* 1024 */     if (par1EnumOptions == EnumOptions.SNOOPER_ENABLED)
/*      */     {
/* 1026 */       this.snooperEnabled = (!this.snooperEnabled);
/*      */     }
/*      */ 
/* 1029 */     if (par1EnumOptions == EnumOptions.SHOW_CAPE)
/*      */     {
/* 1031 */       this.showCape = (!this.showCape);
/*      */     }
/*      */ 
/* 1034 */     if (par1EnumOptions == EnumOptions.TOUCHSCREEN)
/*      */     {
/* 1036 */       this.touchscreen = (!this.touchscreen);
/*      */     }
/*      */ 
/* 1039 */     if (par1EnumOptions == EnumOptions.USE_FULLSCREEN)
/*      */     {
/* 1041 */       this.fullScreen = (!this.fullScreen);
/*      */ 
/* 1043 */       if (this.mc.isFullScreen() != this.fullScreen)
/*      */       {
/* 1045 */         this.mc.toggleFullscreen();
/*      */       }
/*      */     }
/*      */ 
/* 1049 */     if (par1EnumOptions == EnumOptions.ENABLE_VSYNC)
/*      */     {
/* 1051 */       this.enableVsync = (!this.enableVsync);
/* 1052 */       Display.setVSyncEnabled(this.enableVsync);
/*      */     }
/*      */ 
/* 1055 */     saveOptions();
/*      */   }
/*      */ 
/*      */   public float getOptionFloatValue(EnumOptions par1EnumOptions)
/*      */   {
/* 1061 */     if (par1EnumOptions == EnumOptions.CLOUD_HEIGHT)
/*      */     {
/* 1063 */       return this.ofCloudsHeight;
/*      */     }
/* 1065 */     if (par1EnumOptions == EnumOptions.AO_LEVEL)
/*      */     {
/* 1067 */       return this.ofAoLevel;
/*      */     }
/* 1069 */     if (par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE)
/*      */     {
/* 1071 */       return (this.ofRenderDistanceFine - 32) / 480.0F;
/*      */     }
/* 1073 */     if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE)
/*      */     {
/* 1076 */       if ((this.ofLimitFramerateFine <= 0) || (this.ofLimitFramerateFine >= 200))
/*      */       {
/* 1079 */         if (this.enableVsync) {
/* 1080 */           return 0.0F;
/*      */         }
/* 1082 */         return 1.0F;
/*      */       }
/*      */ 
/* 1085 */       return this.ofLimitFramerateFine / 200.0F;
/*      */     }
/*      */ 
/* 1089 */     return par1EnumOptions == EnumOptions.CHAT_WIDTH ? this.chatWidth : par1EnumOptions == EnumOptions.CHAT_SCALE ? this.chatScale : par1EnumOptions == EnumOptions.CHAT_HEIGHT_UNFOCUSED ? this.chatHeightUnfocused : par1EnumOptions == EnumOptions.CHAT_HEIGHT_FOCUSED ? this.chatHeightFocused : par1EnumOptions == EnumOptions.CHAT_OPACITY ? this.chatOpacity : par1EnumOptions == EnumOptions.SENSITIVITY ? this.mouseSensitivity : par1EnumOptions == EnumOptions.SOUND ? this.soundVolume : par1EnumOptions == EnumOptions.MUSIC ? this.musicVolume : par1EnumOptions == EnumOptions.GAMMA ? this.gammaSetting : par1EnumOptions == EnumOptions.FOV ? this.fovSetting : 0.0F;
/*      */   }
/*      */ 
/*      */   public boolean getOptionOrdinalValue(EnumOptions par1EnumOptions)
/*      */   {
/* 1094 */     switch (EnumOptionsHelper.enumOptionsMappingHelperArray[par1EnumOptions.ordinal()])
/*      */     {
/*      */     case 1:
/* 1097 */       return this.invertMouse;
/*      */     case 2:
/* 1100 */       return this.viewBobbing;
/*      */     case 3:
/* 1103 */       return this.anaglyph;
/*      */     case 4:
/* 1106 */       return this.advancedOpengl;
/*      */     case 5:
/* 1109 */       return this.clouds;
/*      */     case 6:
/* 1112 */       return this.chatColours;
/*      */     case 7:
/* 1115 */       return this.chatLinks;
/*      */     case 8:
/* 1118 */       return this.chatLinksPrompt;
/*      */     case 9:
/* 1121 */       return this.serverTextures;
/*      */     case 10:
/* 1124 */       return this.snooperEnabled;
/*      */     case 11:
/* 1127 */       return this.fullScreen;
/*      */     case 12:
/* 1130 */       return this.enableVsync;
/*      */     case 13:
/* 1133 */       return this.showCape;
/*      */     case 14:
/* 1136 */       return this.touchscreen;
/*      */     }
/*      */ 
/* 1139 */     return false;
/*      */   }
/*      */ 
/*      */   private static String getTranslation(String[] par0ArrayOfStr, int par1)
/*      */   {
/* 1149 */     if ((par1 < 0) || (par1 >= par0ArrayOfStr.length))
/*      */     {
/* 1151 */       par1 = 0;
/*      */     }
/*      */ 
/* 1154 */     return I18n.getString(par0ArrayOfStr[par1]);
/*      */   }
/*      */ 
/*      */   public String getKeyBinding(EnumOptions par1EnumOptions)
/*      */   {
/* 1163 */     String prefix = I18n.getString(par1EnumOptions.getEnumString());
/* 1164 */     if (prefix == null)
/* 1165 */       prefix = par1EnumOptions.getEnumString();
/* 1166 */     String label = prefix + ": ";
/*      */ 
/* 1169 */     String s = label;
/* 1170 */     if (par1EnumOptions == EnumOptions.RENDER_DISTANCE_FINE)
/*      */     {
/* 1172 */       String str = "Близко";
/* 1173 */       int baseDist = 32;
/* 1174 */       if (this.ofRenderDistanceFine >= 64)
/*      */       {
/* 1176 */         str = "Средне";
/* 1177 */         baseDist = 64;
/*      */       }
/* 1179 */       if (this.ofRenderDistanceFine >= 128)
/*      */       {
/* 1181 */         str = "Нормально";
/* 1182 */         baseDist = 128;
/*      */       }
/* 1184 */       if (this.ofRenderDistanceFine >= 256)
/*      */       {
/* 1186 */         str = "Далеко";
/* 1187 */         baseDist = 256;
/*      */       }
/* 1189 */       if (this.ofRenderDistanceFine >= 512)
/*      */       {
/* 1191 */         str = "Экстра";
/* 1192 */         baseDist = 512;
/*      */       }
/* 1194 */       int diff = this.ofRenderDistanceFine - baseDist;
/* 1195 */       if (diff == 0) {
/* 1196 */         return s + str;
/*      */       }
/* 1198 */       return s + str + " +" + diff;
/*      */     }
/* 1200 */     if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT_FINE)
/*      */     {
/* 1203 */       if ((this.ofLimitFramerateFine <= 0) || (this.ofLimitFramerateFine >= 200))
/*      */       {
/* 1206 */         if (this.enableVsync) {
/* 1207 */           return s + " VSync";
/*      */         }
/* 1209 */         return s + "Максимум";
/*      */       }
/*      */ 
/* 1212 */       return s + " " + this.ofLimitFramerateFine + " FPS";
/*      */     }
/*      */ 
/* 1217 */     if (par1EnumOptions == EnumOptions.ADVANCED_OPENGL)
/*      */     {
/* 1219 */       if (!this.advancedOpengl)
/*      */       {
/* 1221 */         return s + "Выкл";
/*      */       }
/* 1223 */       if (this.ofOcclusionFancy) {
/* 1224 */         return s + "Вкл";
/*      */       }
/* 1226 */       return s + "Быстро";
/*      */     }
/*      */ 
/* 1231 */     if (par1EnumOptions == EnumOptions.FOG_FANCY)
/*      */     {
/* 1233 */       switch (this.ofFogType)
/*      */       {
/*      */       case 1:
/* 1236 */         return s + "Быстро";
/*      */       case 2:
/* 1238 */         return s + "Вкл";
/*      */       case 3:
/* 1240 */         return s + "Выкл";
/*      */       }
/* 1242 */       return s + "Выкл";
/*      */     }
/*      */ 
/* 1245 */     if (par1EnumOptions == EnumOptions.FOG_START)
/*      */     {
/* 1247 */       return s + this.ofFogStart;
/*      */     }
/* 1249 */     if (par1EnumOptions == EnumOptions.MIPMAP_LEVEL)
/*      */     {
/* 1251 */       if (this.ofMipmapLevel == 0)
/* 1252 */         return s + "Выкл";
/* 1253 */       if (this.ofMipmapLevel == 4) {
/* 1254 */         return s + "Макс.";
/*      */       }
/* 1256 */       return s + this.ofMipmapLevel;
/*      */     }
/* 1258 */     if (par1EnumOptions == EnumOptions.MIPMAP_TYPE)
/*      */     {
/* 1260 */       switch (this.ofMipmapType)
/*      */       {
/*      */       case 0:
/* 1263 */         return s + "Грубо";
/*      */       case 1:
/* 1265 */         return s + "x1";
/*      */       case 2:
/* 1267 */         return s + "x2";
/*      */       case 3:
/* 1269 */         return s + "x3";
/*      */       }
/* 1271 */       return s + "Грубо";
/*      */     }
/* 1273 */     if (par1EnumOptions == EnumOptions.LOAD_FAR)
/*      */     {
/* 1275 */       if (this.ofLoadFar) {
/* 1276 */         return s + "Вкл";
/*      */       }
/* 1278 */       return s + "Выкл";
/*      */     }
/* 1280 */     if (par1EnumOptions == EnumOptions.PRELOADED_CHUNKS)
/*      */     {
/* 1282 */       if (this.ofPreloadedChunks == 0) {
/* 1283 */         return s + "Выкл";
/*      */       }
/* 1285 */       return s + this.ofPreloadedChunks;
/*      */     }
/* 1287 */     if (par1EnumOptions == EnumOptions.SMOOTH_FPS)
/*      */     {
/* 1289 */       if (this.ofSmoothFps) {
/* 1290 */         return s + "Вкл";
/*      */       }
/* 1292 */       return s + "Выкл";
/*      */     }
/* 1294 */     if (par1EnumOptions == EnumOptions.SMOOTH_WORLD)
/*      */     {
/* 1296 */       if (this.ofSmoothWorld) {
/* 1297 */         return s + "Вкл";
/*      */       }
/* 1299 */       return s + "Выкл";
/*      */     }
/* 1301 */     if (par1EnumOptions == EnumOptions.CLOUDS)
/*      */     {
/* 1303 */       switch (this.ofClouds) {
/*      */       case 1:
/* 1305 */         return s + "Быстро";
/*      */       case 2:
/* 1306 */         return s + "Вкл";
/*      */       case 3:
/* 1307 */         return s + "Выкл";
/* 1308 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1311 */     if (par1EnumOptions == EnumOptions.TREES)
/*      */     {
/* 1313 */       switch (this.ofTrees) {
/*      */       case 1:
/* 1315 */         return s + "Быстро";
/*      */       case 2:
/* 1316 */         return s + "Вкл";
/* 1317 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1320 */     if (par1EnumOptions == EnumOptions.GRASS)
/*      */     {
/* 1322 */       switch (this.ofGrass) {
/*      */       case 1:
/* 1324 */         return s + "Быстро";
/*      */       case 2:
/* 1325 */         return s + "Вкл";
/* 1326 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1329 */     if (par1EnumOptions == EnumOptions.DROPPED_ITEMS)
/*      */     {
/* 1331 */       switch (this.ofDroppedItems) {
/*      */       case 1:
/* 1333 */         return s + "Быстро";
/*      */       case 2:
/* 1334 */         return s + "Вкл";
/* 1335 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1338 */     if (par1EnumOptions == EnumOptions.RAIN)
/*      */     {
/* 1340 */       switch (this.ofRain) {
/*      */       case 1:
/* 1342 */         return s + "Быстро";
/*      */       case 2:
/* 1343 */         return s + "Вкл";
/*      */       case 3:
/* 1344 */         return s + "Выкл";
/* 1345 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1348 */     if (par1EnumOptions == EnumOptions.WATER)
/*      */     {
/* 1350 */       switch (this.ofWater) {
/*      */       case 1:
/* 1352 */         return s + "Быстро";
/*      */       case 2:
/* 1353 */         return s + "Вкл";
/*      */       case 3:
/* 1354 */         return s + "Выкл";
/* 1355 */       }return s + "Стандарт";
/*      */     }
/*      */ 
/* 1358 */     if (par1EnumOptions == EnumOptions.ANIMATED_WATER)
/*      */     {
/* 1360 */       switch (this.ofAnimatedWater) {
/*      */       case 1:
/* 1362 */         return s + "Динамично";
/*      */       case 2:
/* 1363 */         return s + "Выкл";
/*      */       }
/* 1365 */       return s + "Вкл";
/*      */     }
/* 1367 */     if (par1EnumOptions == EnumOptions.ANIMATED_LAVA)
/*      */     {
/* 1369 */       switch (this.ofAnimatedLava) {
/*      */       case 1:
/* 1371 */         return s + "Динамично";
/*      */       case 2:
/* 1372 */         return s + "Выкл";
/*      */       }
/* 1374 */       return s + "Вкл";
/*      */     }
/* 1376 */     if (par1EnumOptions == EnumOptions.ANIMATED_FIRE)
/*      */     {
/* 1378 */       if (this.ofAnimatedFire) {
/* 1379 */         return s + "Вкл";
/*      */       }
/* 1381 */       return s + "Выкл";
/*      */     }
/* 1383 */     if (par1EnumOptions == EnumOptions.ANIMATED_PORTAL)
/*      */     {
/* 1385 */       if (this.ofAnimatedPortal) {
/* 1386 */         return s + "Вкл";
/*      */       }
/* 1388 */       return s + "Выкл";
/*      */     }
/* 1390 */     if (par1EnumOptions == EnumOptions.ANIMATED_REDSTONE)
/*      */     {
/* 1392 */       if (this.ofAnimatedRedstone) {
/* 1393 */         return s + "Вкл";
/*      */       }
/* 1395 */       return s + "Выкл";
/*      */     }
/* 1397 */     if (par1EnumOptions == EnumOptions.ANIMATED_EXPLOSION)
/*      */     {
/* 1399 */       if (this.ofAnimatedExplosion) {
/* 1400 */         return s + "Вкл";
/*      */       }
/* 1402 */       return s + "Выкл";
/*      */     }
/* 1404 */     if (par1EnumOptions == EnumOptions.ANIMATED_FLAME)
/*      */     {
/* 1406 */       if (this.ofAnimatedFlame) {
/* 1407 */         return s + "Вкл";
/*      */       }
/* 1409 */       return s + "Выкл";
/*      */     }
/* 1411 */     if (par1EnumOptions == EnumOptions.ANIMATED_SMOKE)
/*      */     {
/* 1413 */       if (this.ofAnimatedSmoke) {
/* 1414 */         return s + "Вкл";
/*      */       }
/* 1416 */       return s + "Выкл";
/*      */     }
/* 1418 */     if (par1EnumOptions == EnumOptions.VOID_PARTICLES)
/*      */     {
/* 1420 */       if (this.ofVoidParticles) {
/* 1421 */         return s + "Вкл";
/*      */       }
/* 1423 */       return s + "Выкл";
/*      */     }
/* 1425 */     if (par1EnumOptions == EnumOptions.WATER_PARTICLES)
/*      */     {
/* 1427 */       if (this.ofWaterParticles) {
/* 1428 */         return s + "Вкл";
/*      */       }
/* 1430 */       return s + "Выкл";
/*      */     }
/* 1432 */     if (par1EnumOptions == EnumOptions.PORTAL_PARTICLES)
/*      */     {
/* 1434 */       if (this.ofPortalParticles) {
/* 1435 */         return s + "Вкл";
/*      */       }
/* 1437 */       return s + "Выкл";
/*      */     }
/* 1439 */     if (par1EnumOptions == EnumOptions.POTION_PARTICLES)
/*      */     {
/* 1441 */       if (this.ofPotionParticles) {
/* 1442 */         return s + "Вкл";
/*      */       }
/* 1444 */       return s + "Выкл";
/*      */     }
/* 1446 */     if (par1EnumOptions == EnumOptions.DRIPPING_WATER_LAVA)
/*      */     {
/* 1448 */       if (this.ofDrippingWaterLava) {
/* 1449 */         return s + "Вкл";
/*      */       }
/* 1451 */       return s + "Выкл";
/*      */     }
/* 1453 */     if (par1EnumOptions == EnumOptions.ANIMATED_TERRAIN)
/*      */     {
/* 1455 */       if (this.ofAnimatedTerrain) {
/* 1456 */         return s + "Вкл";
/*      */       }
/* 1458 */       return s + "Выкл";
/*      */     }
/* 1460 */     if (par1EnumOptions == EnumOptions.ANIMATED_TEXTURES)
/*      */     {
/* 1462 */       if (this.ofAnimatedTextures) {
/* 1463 */         return s + "Вкл";
/*      */       }
/* 1465 */       return s + "Выкл";
/*      */     }
/* 1467 */     if (par1EnumOptions == EnumOptions.ANIMATED_ITEMS)
/*      */     {
/* 1469 */       if (this.ofAnimatedItems) {
/* 1470 */         return s + "Вкл";
/*      */       }
/* 1472 */       return s + "Выкл";
/*      */     }
/* 1474 */     if (par1EnumOptions == EnumOptions.RAIN_SPLASH)
/*      */     {
/* 1476 */       if (this.ofRainSplash) {
/* 1477 */         return s + "Вкл";
/*      */       }
/* 1479 */       return s + "Выкл";
/*      */     }
/* 1481 */     if (par1EnumOptions == EnumOptions.LAGOMETER)
/*      */     {
/* 1483 */       if (this.ofLagometer) {
/* 1484 */         return s + "Вкл";
/*      */       }
/* 1486 */       return s + "Выкл";
/*      */     }
/* 1488 */     if (par1EnumOptions == EnumOptions.AUTOSAVE_TICKS)
/*      */     {
/* 1490 */       if (this.ofAutoSaveTicks <= 40)
/* 1491 */         return s + "Станд. (2 с)";
/* 1492 */       if (this.ofAutoSaveTicks <= 400)
/* 1493 */         return s + "20 сек";
/* 1494 */       if (this.ofAutoSaveTicks <= 4000) {
/* 1495 */         return s + "3 мин";
/*      */       }
/* 1497 */       return s + "30 мин";
/*      */     }
/* 1499 */     if (par1EnumOptions == EnumOptions.BETTER_GRASS)
/*      */     {
/* 1501 */       switch (this.ofBetterGrass)
/*      */       {
/*      */       case 1:
/* 1504 */         return s + "Быстро";
/*      */       case 2:
/* 1506 */         return s + "Вкл";
/*      */       }
/* 1508 */       return s + "Выкл";
/*      */     }
/* 1510 */     if (par1EnumOptions == EnumOptions.CONNECTED_TEXTURES)
/*      */     {
/* 1512 */       switch (this.ofConnectedTextures)
/*      */       {
/*      */       case 1:
/* 1515 */         return s + "Быстро";
/*      */       case 2:
/* 1517 */         return s + "Вкл";
/*      */       }
/* 1519 */       return s + "Выкл";
/*      */     }
/* 1521 */     if (par1EnumOptions == EnumOptions.WEATHER)
/*      */     {
/* 1523 */       if (this.ofWeather) {
/* 1524 */         return s + "Вкл";
/*      */       }
/* 1526 */       return s + "Выкл";
/*      */     }
/* 1528 */     if (par1EnumOptions == EnumOptions.SKY)
/*      */     {
/* 1530 */       if (this.ofSky) {
/* 1531 */         return s + "Вкл";
/*      */       }
/* 1533 */       return s + "Выкл";
/*      */     }
/* 1535 */     if (par1EnumOptions == EnumOptions.STARS)
/*      */     {
/* 1537 */       if (this.ofStars) {
/* 1538 */         return s + "Вкл";
/*      */       }
/* 1540 */       return s + "Выкл";
/*      */     }
/* 1542 */     if (par1EnumOptions == EnumOptions.SUN_MOON)
/*      */     {
/* 1544 */       if (this.ofSunMoon) {
/* 1545 */         return s + "Вкл";
/*      */       }
/* 1547 */       return s + "Выкл";
/*      */     }
/* 1549 */     if (par1EnumOptions == EnumOptions.CHUNK_UPDATES)
/*      */     {
/* 1551 */       return s + this.ofChunkUpdates;
/*      */     }
/* 1553 */     if (par1EnumOptions == EnumOptions.CHUNK_LOADING)
/*      */     {
/* 1555 */       if (this.ofChunkLoading == 1)
/* 1556 */         return s + "Гладкая";
/* 1557 */       if (this.ofChunkLoading == 2) {
/* 1558 */         return s + "Multi-Core";
/*      */       }
/* 1560 */       return s + "Стандарт";
/*      */     }
/* 1562 */     if (par1EnumOptions == EnumOptions.CHUNK_UPDATES_DYNAMIC)
/*      */     {
/* 1564 */       if (this.ofChunkUpdatesDynamic) {
/* 1565 */         return s + "Вкл";
/*      */       }
/* 1567 */       return s + "Выкл";
/*      */     }
/* 1569 */     if (par1EnumOptions == EnumOptions.TIME)
/*      */     {
/* 1571 */       if (this.ofTime == 1)
/* 1572 */         return s + "Только День";
/* 1573 */       if (this.ofTime == 3)
/* 1574 */         return s + "Только Ночь";
/* 1575 */       return s + "Стандарт";
/*      */     }
/* 1577 */     if (par1EnumOptions == EnumOptions.CLEAR_WATER)
/*      */     {
/* 1579 */       if (this.ofClearWater) {
/* 1580 */         return s + "Вкл";
/*      */       }
/* 1582 */       return s + "Выкл";
/*      */     }
/* 1584 */     if (par1EnumOptions == EnumOptions.DEPTH_FOG)
/*      */     {
/* 1586 */       if (this.ofDepthFog) {
/* 1587 */         return s + "Вкл";
/*      */       }
/* 1589 */       return s + "Выкл";
/*      */     }
/* 1591 */     if (par1EnumOptions == EnumOptions.AA_LEVEL)
/*      */     {
/* 1593 */       if (this.ofAaLevel == 0) {
/* 1594 */         return s + "Выкл";
/*      */       }
/* 1596 */       return s + this.ofAaLevel;
/*      */     }
/* 1598 */     if (par1EnumOptions == EnumOptions.AF_LEVEL)
/*      */     {
/* 1600 */       if (this.ofAfLevel == 1) {
/* 1601 */         return s + "Выкл";
/*      */       }
/* 1603 */       return s + this.ofAfLevel;
/*      */     }
/* 1605 */     if (par1EnumOptions == EnumOptions.PROFILER)
/*      */     {
/* 1607 */       if (this.ofProfiler) {
/* 1608 */         return s + "Вкл";
/*      */       }
/* 1610 */       return s + "Выкл";
/*      */     }
/* 1612 */     if (par1EnumOptions == EnumOptions.BETTER_SNOW)
/*      */     {
/* 1614 */       if (this.ofBetterSnow) {
/* 1615 */         return s + "Вкл";
/*      */       }
/* 1617 */       return s + "Выкл";
/*      */     }
/* 1619 */     if (par1EnumOptions == EnumOptions.SWAMP_COLORS)
/*      */     {
/* 1621 */       if (this.ofSwampColors) {
/* 1622 */         return s + "Вкл";
/*      */       }
/* 1624 */       return s + "Выкл";
/*      */     }
/* 1626 */     if (par1EnumOptions == EnumOptions.RANDOM_MOBS)
/*      */     {
/* 1628 */       if (this.ofRandomMobs) {
/* 1629 */         return s + "Вкл";
/*      */       }
/* 1631 */       return s + "Выкл";
/*      */     }
/* 1633 */     if (par1EnumOptions == EnumOptions.SMOOTH_BIOMES)
/*      */     {
/* 1635 */       if (this.ofSmoothBiomes) {
/* 1636 */         return s + "Вкл";
/*      */       }
/* 1638 */       return s + "Выкл";
/*      */     }
/* 1640 */     if (par1EnumOptions == EnumOptions.CUSTOM_FONTS)
/*      */     {
/* 1642 */       if (this.ofCustomFonts) {
/* 1643 */         return s + "Вкл";
/*      */       }
/* 1645 */       return s + "Выкл";
/*      */     }
/* 1647 */     if (par1EnumOptions == EnumOptions.CUSTOM_COLORS)
/*      */     {
/* 1649 */       if (this.ofCustomColors) {
/* 1650 */         return s + "Вкл";
/*      */       }
/* 1652 */       return s + "Выкл";
/*      */     }
/* 1654 */     if (par1EnumOptions == EnumOptions.CUSTOM_SKY)
/*      */     {
/* 1656 */       if (this.ofCustomSky) {
/* 1657 */         return s + "Вкл";
/*      */       }
/* 1659 */       return s + "Выкл";
/*      */     }
/* 1661 */     if (par1EnumOptions == EnumOptions.SHOW_CAPES)
/*      */     {
/* 1663 */       if (this.ofShowCapes) {
/* 1664 */         return s + "Вкл";
/*      */       }
/* 1666 */       return s + "Выкл";
/*      */     }
/* 1668 */     if (par1EnumOptions == EnumOptions.NATURAL_TEXTURES)
/*      */     {
/* 1670 */       if (this.ofNaturalTextures) {
/* 1671 */         return s + "Вкл";
/*      */       }
/* 1673 */       return s + "Выкл";
/*      */     }
/* 1675 */     if (par1EnumOptions == EnumOptions.LAZY_CHUNK_LOADING)
/*      */     {
/* 1677 */       if (this.ofLazyChunkLoading) {
/* 1678 */         return s + "Вкл";
/*      */       }
/* 1680 */       return s + "Выкл";
/*      */     }
/* 1682 */     if (par1EnumOptions == EnumOptions.FULLSCREEN_MODE)
/*      */     {
/* 1684 */       return s + this.ofFullscreenMode;
/*      */     }
/* 1686 */     if (par1EnumOptions == EnumOptions.HELD_ITEM_TOOLTIPS)
/*      */     {
/* 1688 */       if (this.heldItemTooltips) {
/* 1689 */         return s + "Вкл";
/*      */       }
/* 1691 */       return s + "Выкл";
/*      */     }
/*      */ 
/* 1695 */     String var2 = I18n.getString(par1EnumOptions.getEnumString()) + ": ";
/*      */ 
/* 1697 */     if (par1EnumOptions.getEnumFloat())
/*      */     {
/* 1699 */       float var5 = getOptionFloatValue(par1EnumOptions);
/* 1700 */       return var2 + (int)(var5 * 100.0F) + "%";
/*      */     }
/* 1702 */     if (par1EnumOptions.getEnumBoolean())
/*      */     {
/* 1704 */       boolean var4 = getOptionOrdinalValue(par1EnumOptions);
/* 1705 */       return var2 + I18n.getString("options.off");
/*      */     }
/* 1707 */     if (par1EnumOptions == EnumOptions.RENDER_DISTANCE)
/*      */     {
/* 1709 */       return var2 + getTranslation(RENDER_DISTANCES, this.renderDistance);
/*      */     }
/* 1711 */     if (par1EnumOptions == EnumOptions.DIFFICULTY)
/*      */     {
/* 1713 */       return var2 + getTranslation(DIFFICULTIES, this.difficulty);
/*      */     }
/* 1715 */     if (par1EnumOptions == EnumOptions.GUI_SCALE)
/*      */     {
/* 1717 */       return var2 + getTranslation(GUISCALES, this.guiScale);
/*      */     }
/* 1719 */     if (par1EnumOptions == EnumOptions.CHAT_VISIBILITY)
/*      */     {
/* 1721 */       return var2 + getTranslation(CHAT_VISIBILITIES, this.chatVisibility);
/*      */     }
/* 1723 */     if (par1EnumOptions == EnumOptions.PARTICLES)
/*      */     {
/* 1725 */       return var2 + getTranslation(PARTICLES, this.particleSetting);
/*      */     }
/* 1727 */     if (par1EnumOptions == EnumOptions.FRAMERATE_LIMIT)
/*      */     {
/* 1729 */       return var2 + getTranslation(LIMIT_FRAMERATES, this.limitFramerate);
/*      */     }
/* 1731 */     if (par1EnumOptions == EnumOptions.AMBIENT_OCCLUSION)
/*      */     {
/* 1733 */       return var2 + getTranslation(AMBIENT_OCCLUSIONS, this.ambientOcclusion);
/*      */     }
/* 1735 */     if (par1EnumOptions == EnumOptions.GRAPHICS)
/*      */     {
/* 1737 */       if (this.fancyGraphics)
/*      */       {
/* 1739 */         return var2 + I18n.getString("options.graphics.fancy");
/*      */       }
/*      */ 
/* 1743 */       String var3 = "options.graphics.fast";
/* 1744 */       return var2 + I18n.getString("options.graphics.fast");
/*      */     }
/*      */ 
/* 1749 */     return var2;
/*      */   }
/*      */ 
/*      */   public void loadOptions()
/*      */   {
/*      */     try
/*      */     {
/* 1760 */       if (!this.optionsFile.exists())
/*      */       {
/* 1762 */         return;
/*      */       }
/*      */ 
/* 1765 */       BufferedReader var1 = new BufferedReader(new FileReader(this.optionsFile));
/* 1766 */       String var2 = "";
/*      */ 
/* 1768 */       while ((var2 = var1.readLine()) != null)
/*      */       {
/*      */         try
/*      */         {
/* 1772 */           String[] var3 = var2.split(":");
/*      */ 
/* 1774 */           if (var3[0].equals("music"))
/*      */           {
/* 1776 */             this.musicVolume = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1779 */           if (var3[0].equals("sound"))
/*      */           {
/* 1781 */             this.soundVolume = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1784 */           if (var3[0].equals("mouseSensitivity"))
/*      */           {
/* 1786 */             this.mouseSensitivity = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1789 */           if (var3[0].equals("fov"))
/*      */           {
/* 1791 */             this.fovSetting = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1794 */           if (var3[0].equals("gamma"))
/*      */           {
/* 1796 */             this.gammaSetting = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1799 */           if (var3[0].equals("invertYMouse"))
/*      */           {
/* 1801 */             this.invertMouse = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1804 */           if (var3[0].equals("viewDistance"))
/*      */           {
/* 1806 */             this.renderDistance = Integer.parseInt(var3[1]);
/*      */ 
/* 1808 */             this.ofRenderDistanceFine = renderDistanceToFine(this.renderDistance);
/*      */           }
/*      */ 
/* 1811 */           if (var3[0].equals("guiScale"))
/*      */           {
/* 1813 */             this.guiScale = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1816 */           if (var3[0].equals("particles"))
/*      */           {
/* 1818 */             this.particleSetting = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1821 */           if (var3[0].equals("bobView"))
/*      */           {
/* 1823 */             this.viewBobbing = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1826 */           if (var3[0].equals("anaglyph3d"))
/*      */           {
/* 1828 */             this.anaglyph = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1831 */           if (var3[0].equals("advancedOpengl"))
/*      */           {
/* 1833 */             this.advancedOpengl = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1836 */           if (var3[0].equals("fpsLimit"))
/*      */           {
/* 1838 */             this.limitFramerate = Integer.parseInt(var3[1]);
/*      */ 
/* 1840 */             this.ofLimitFramerateFine = limitFramerateToFine(this.limitFramerate);
/*      */           }
/*      */ 
/* 1843 */           if (var3[0].equals("difficulty"))
/*      */           {
/* 1845 */             this.difficulty = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1848 */           if (var3[0].equals("fancyGraphics"))
/*      */           {
/* 1850 */             this.fancyGraphics = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1853 */           if (var3[0].equals("ao"))
/*      */           {
/* 1855 */             if (var3[1].equals("true"))
/*      */             {
/* 1857 */               this.ambientOcclusion = 2;
/*      */             }
/* 1859 */             else if (var3[1].equals("false"))
/*      */             {
/* 1861 */               this.ambientOcclusion = 0;
/*      */             }
/*      */             else
/*      */             {
/* 1865 */               this.ambientOcclusion = Integer.parseInt(var3[1]);
/*      */             }
/*      */           }
/*      */ 
/* 1869 */           if (var3[0].equals("clouds"))
/*      */           {
/* 1871 */             this.clouds = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1874 */           if (var3[0].equals("skin"))
/*      */           {
/* 1876 */             this.skin = var3[1];
/*      */           }
/*      */ 
/* 1879 */           if ((var3[0].equals("lastServer")) && (var3.length >= 2))
/*      */           {
/* 1881 */             this.lastServer = var2.substring(var2.indexOf(':') + 1);
/*      */           }
/*      */ 
/* 1884 */           if ((var3[0].equals("lang")) && (var3.length >= 2))
/*      */           {
/* 1886 */             this.language = var3[1];
/*      */           }
/*      */ 
/* 1889 */           if (var3[0].equals("chatVisibility"))
/*      */           {
/* 1891 */             this.chatVisibility = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1894 */           if (var3[0].equals("chatColors"))
/*      */           {
/* 1896 */             this.chatColours = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1899 */           if (var3[0].equals("chatLinks"))
/*      */           {
/* 1901 */             this.chatLinks = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1904 */           if (var3[0].equals("chatLinksPrompt"))
/*      */           {
/* 1906 */             this.chatLinksPrompt = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1909 */           if (var3[0].equals("chatOpacity"))
/*      */           {
/* 1911 */             this.chatOpacity = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1914 */           if (var3[0].equals("serverTextures"))
/*      */           {
/* 1916 */             this.serverTextures = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1919 */           if (var3[0].equals("snooperEnabled"))
/*      */           {
/* 1921 */             this.snooperEnabled = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1924 */           if (var3[0].equals("fullscreen"))
/*      */           {
/* 1926 */             this.fullScreen = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1929 */           if (var3[0].equals("enableVsync"))
/*      */           {
/* 1931 */             this.enableVsync = var3[1].equals("true");
/*      */ 
/* 1933 */             updateVSync();
/*      */           }
/*      */ 
/* 1936 */           if (var3[0].equals("hideServerAddress"))
/*      */           {
/* 1938 */             this.hideServerAddress = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1941 */           if (var3[0].equals("advancedItemTooltips"))
/*      */           {
/* 1943 */             this.advancedItemTooltips = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1946 */           if (var3[0].equals("pauseOnLostFocus"))
/*      */           {
/* 1948 */             this.pauseOnLostFocus = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1951 */           if (var3[0].equals("showCape"))
/*      */           {
/* 1953 */             this.showCape = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1956 */           if (var3[0].equals("touchscreen"))
/*      */           {
/* 1958 */             this.touchscreen = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1961 */           if (var3[0].equals("overrideHeight"))
/*      */           {
/* 1963 */             this.overrideHeight = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1966 */           if (var3[0].equals("overrideWidth"))
/*      */           {
/* 1968 */             this.overrideWidth = Integer.parseInt(var3[1]);
/*      */           }
/*      */ 
/* 1971 */           if (var3[0].equals("heldItemTooltips"))
/*      */           {
/* 1973 */             this.heldItemTooltips = var3[1].equals("true");
/*      */           }
/*      */ 
/* 1976 */           if (var3[0].equals("chatHeightFocused"))
/*      */           {
/* 1978 */             this.chatHeightFocused = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1981 */           if (var3[0].equals("chatHeightUnfocused"))
/*      */           {
/* 1983 */             this.chatHeightUnfocused = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1986 */           if (var3[0].equals("chatScale"))
/*      */           {
/* 1988 */             this.chatScale = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1991 */           if (var3[0].equals("chatWidth"))
/*      */           {
/* 1993 */             this.chatWidth = parseFloat(var3[1]);
/*      */           }
/*      */ 
/* 1996 */           for (int var4 = 0; var4 < this.keyBindings.length; var4++)
/*      */           {
/* 1998 */             if (var3[0].equals("key_" + this.keyBindings[var4].keyDescription))
/*      */             {
/* 2000 */               this.keyBindings[var4].keyCode = Integer.parseInt(var3[1]);
/*      */             }
/*      */           }
/*      */         }
/*      */         catch (Exception var5)
/*      */         {
/* 2006 */           this.mc.getLogAgent().logWarning("Skipping bad option: " + var2);
/* 2007 */           var5.printStackTrace();
/*      */         }
/*      */       }
/*      */ 
/* 2011 */       KeyBinding.resetKeyBindingArrayAndHash();
/* 2012 */       var1.close();
/*      */     }
/*      */     catch (Exception var6)
/*      */     {
/* 2016 */       this.mc.getLogAgent().logWarning("Failed to load options");
/* 2017 */       var6.printStackTrace();
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/* 2023 */       File ofReadFile = this.optionsFileOF;
/*      */ 
/* 2025 */       if (!ofReadFile.exists()) {
/* 2026 */         ofReadFile = this.optionsFile;
/*      */       }
/* 2028 */       if (!ofReadFile.exists())
/*      */       {
/* 2030 */         return;
/*      */       }
/* 2032 */       BufferedReader bufferedreader = new BufferedReader(new FileReader(ofReadFile));
/* 2033 */       for (String s = ""; (s = bufferedreader.readLine()) != null; )
/*      */       {
/*      */         try
/*      */         {
/* 2037 */           String[] as = s.split(":");
/*      */ 
/* 2039 */           if ((as[0].equals("ofRenderDistanceFine")) && (as.length >= 2))
/*      */           {
/* 2041 */             this.ofRenderDistanceFine = Integer.valueOf(as[1]).intValue();
/* 2042 */             this.ofRenderDistanceFine = Config.limit(this.ofRenderDistanceFine, 32, 512);
/*      */ 
/* 2044 */             this.renderDistance = fineToRenderDistance(this.ofRenderDistanceFine);
/*      */           }
/* 2046 */           if ((as[0].equals("ofLimitFramerateFine")) && (as.length >= 2))
/*      */           {
/* 2048 */             this.ofLimitFramerateFine = Integer.valueOf(as[1]).intValue();
/* 2049 */             this.ofLimitFramerateFine = Config.limit(this.ofLimitFramerateFine, 0, 199);
/*      */ 
/* 2051 */             this.limitFramerate = fineToLimitFramerate(this.ofLimitFramerateFine);
/*      */           }
/* 2053 */           if ((as[0].equals("ofFogType")) && (as.length >= 2))
/*      */           {
/* 2055 */             this.ofFogType = Integer.valueOf(as[1]).intValue();
/* 2056 */             this.ofFogType = Config.limit(this.ofFogType, 1, 3);
/*      */           }
/* 2058 */           if ((as[0].equals("ofFogStart")) && (as.length >= 2))
/*      */           {
/* 2060 */             this.ofFogStart = Float.valueOf(as[1]).floatValue();
/* 2061 */             if (this.ofFogStart < 0.2F)
/* 2062 */               this.ofFogStart = 0.2F;
/* 2063 */             if (this.ofFogStart > 0.81F)
/* 2064 */               this.ofFogStart = 0.8F;
/*      */           }
/* 2066 */           if ((as[0].equals("ofMipmapLevel")) && (as.length >= 2))
/*      */           {
/* 2068 */             this.ofMipmapLevel = Integer.valueOf(as[1]).intValue();
/* 2069 */             if (this.ofMipmapLevel < 0)
/* 2070 */               this.ofMipmapLevel = 0;
/* 2071 */             if (this.ofMipmapLevel > 4)
/* 2072 */               this.ofMipmapLevel = 4;
/*      */           }
/* 2074 */           if ((as[0].equals("ofMipmapType")) && (as.length >= 2))
/*      */           {
/* 2076 */             this.ofMipmapType = Integer.valueOf(as[1]).intValue();
/* 2077 */             this.ofMipmapType = Config.limit(this.ofMipmapType, 0, 3);
/*      */           }
/* 2079 */           if ((as[0].equals("ofLoadFar")) && (as.length >= 2))
/*      */           {
/* 2081 */             this.ofLoadFar = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2083 */           if ((as[0].equals("ofPreloadedChunks")) && (as.length >= 2))
/*      */           {
/* 2085 */             this.ofPreloadedChunks = Integer.valueOf(as[1]).intValue();
/* 2086 */             if (this.ofPreloadedChunks < 0)
/* 2087 */               this.ofPreloadedChunks = 0;
/* 2088 */             if (this.ofPreloadedChunks > 8)
/* 2089 */               this.ofPreloadedChunks = 8;
/*      */           }
/* 2091 */           if ((as[0].equals("ofOcclusionFancy")) && (as.length >= 2))
/*      */           {
/* 2093 */             this.ofOcclusionFancy = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2095 */           if ((as[0].equals("ofSmoothFps")) && (as.length >= 2))
/*      */           {
/* 2097 */             this.ofSmoothFps = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2099 */           if ((as[0].equals("ofSmoothWorld")) && (as.length >= 2))
/*      */           {
/* 2101 */             this.ofSmoothWorld = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2103 */           if ((as[0].equals("ofAoLevel")) && (as.length >= 2))
/*      */           {
/* 2105 */             this.ofAoLevel = Float.valueOf(as[1]).floatValue();
/* 2106 */             this.ofAoLevel = Config.limit(this.ofAoLevel, 0.0F, 1.0F);
/*      */           }
/* 2108 */           if ((as[0].equals("ofClouds")) && (as.length >= 2))
/*      */           {
/* 2110 */             this.ofClouds = Integer.valueOf(as[1]).intValue();
/* 2111 */             this.ofClouds = Config.limit(this.ofClouds, 0, 3);
/*      */           }
/* 2113 */           if ((as[0].equals("ofCloudsHeight")) && (as.length >= 2))
/*      */           {
/* 2115 */             this.ofCloudsHeight = Float.valueOf(as[1]).floatValue();
/* 2116 */             this.ofCloudsHeight = Config.limit(this.ofCloudsHeight, 0.0F, 1.0F);
/*      */           }
/* 2118 */           if ((as[0].equals("ofTrees")) && (as.length >= 2))
/*      */           {
/* 2120 */             this.ofTrees = Integer.valueOf(as[1]).intValue();
/* 2121 */             this.ofTrees = Config.limit(this.ofTrees, 0, 2);
/*      */           }
/* 2123 */           if ((as[0].equals("ofGrass")) && (as.length >= 2))
/*      */           {
/* 2125 */             this.ofGrass = Integer.valueOf(as[1]).intValue();
/* 2126 */             this.ofGrass = Config.limit(this.ofGrass, 0, 2);
/*      */           }
/* 2128 */           if ((as[0].equals("ofDroppedItems")) && (as.length >= 2))
/*      */           {
/* 2130 */             this.ofDroppedItems = Integer.valueOf(as[1]).intValue();
/* 2131 */             this.ofDroppedItems = Config.limit(this.ofDroppedItems, 0, 2);
/*      */           }
/* 2133 */           if ((as[0].equals("ofRain")) && (as.length >= 2))
/*      */           {
/* 2135 */             this.ofRain = Integer.valueOf(as[1]).intValue();
/* 2136 */             this.ofRain = Config.limit(this.ofRain, 0, 3);
/*      */           }
/* 2138 */           if ((as[0].equals("ofWater")) && (as.length >= 2))
/*      */           {
/* 2140 */             this.ofWater = Integer.valueOf(as[1]).intValue();
/* 2141 */             this.ofWater = Config.limit(this.ofWater, 0, 3);
/*      */           }
/* 2143 */           if ((as[0].equals("ofAnimatedWater")) && (as.length >= 2))
/*      */           {
/* 2145 */             this.ofAnimatedWater = Integer.valueOf(as[1]).intValue();
/* 2146 */             this.ofAnimatedWater = Config.limit(this.ofAnimatedWater, 0, 2);
/*      */           }
/* 2148 */           if ((as[0].equals("ofAnimatedLava")) && (as.length >= 2))
/*      */           {
/* 2150 */             this.ofAnimatedLava = Integer.valueOf(as[1]).intValue();
/* 2151 */             this.ofAnimatedLava = Config.limit(this.ofAnimatedLava, 0, 2);
/*      */           }
/* 2153 */           if ((as[0].equals("ofAnimatedFire")) && (as.length >= 2))
/*      */           {
/* 2155 */             this.ofAnimatedFire = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2157 */           if ((as[0].equals("ofAnimatedPortal")) && (as.length >= 2))
/*      */           {
/* 2159 */             this.ofAnimatedPortal = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2161 */           if ((as[0].equals("ofAnimatedRedstone")) && (as.length >= 2))
/*      */           {
/* 2163 */             this.ofAnimatedRedstone = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2165 */           if ((as[0].equals("ofAnimatedExplosion")) && (as.length >= 2))
/*      */           {
/* 2167 */             this.ofAnimatedExplosion = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2169 */           if ((as[0].equals("ofAnimatedFlame")) && (as.length >= 2))
/*      */           {
/* 2171 */             this.ofAnimatedFlame = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2173 */           if ((as[0].equals("ofAnimatedSmoke")) && (as.length >= 2))
/*      */           {
/* 2175 */             this.ofAnimatedSmoke = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2177 */           if ((as[0].equals("ofVoidParticles")) && (as.length >= 2))
/*      */           {
/* 2179 */             this.ofVoidParticles = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2181 */           if ((as[0].equals("ofWaterParticles")) && (as.length >= 2))
/*      */           {
/* 2183 */             this.ofWaterParticles = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2185 */           if ((as[0].equals("ofPortalParticles")) && (as.length >= 2))
/*      */           {
/* 2187 */             this.ofPortalParticles = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2189 */           if ((as[0].equals("ofPotionParticles")) && (as.length >= 2))
/*      */           {
/* 2191 */             this.ofPotionParticles = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2193 */           if ((as[0].equals("ofDrippingWaterLava")) && (as.length >= 2))
/*      */           {
/* 2195 */             this.ofDrippingWaterLava = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2197 */           if ((as[0].equals("ofAnimatedTerrain")) && (as.length >= 2))
/*      */           {
/* 2199 */             this.ofAnimatedTerrain = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2201 */           if ((as[0].equals("ofAnimatedTextures")) && (as.length >= 2))
/*      */           {
/* 2203 */             this.ofAnimatedTextures = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2205 */           if ((as[0].equals("ofAnimatedItems")) && (as.length >= 2))
/*      */           {
/* 2207 */             this.ofAnimatedItems = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2209 */           if ((as[0].equals("ofRainSplash")) && (as.length >= 2))
/*      */           {
/* 2211 */             this.ofRainSplash = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2213 */           if ((as[0].equals("ofLagometer")) && (as.length >= 2))
/*      */           {
/* 2215 */             this.ofLagometer = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2217 */           if ((as[0].equals("ofAutoSaveTicks")) && (as.length >= 2))
/*      */           {
/* 2219 */             this.ofAutoSaveTicks = Integer.valueOf(as[1]).intValue();
/* 2220 */             this.ofAutoSaveTicks = Config.limit(this.ofAutoSaveTicks, 40, 40000);
/*      */           }
/* 2222 */           if ((as[0].equals("ofBetterGrass")) && (as.length >= 2))
/*      */           {
/* 2224 */             this.ofBetterGrass = Integer.valueOf(as[1]).intValue();
/* 2225 */             this.ofBetterGrass = Config.limit(this.ofBetterGrass, 1, 3);
/*      */           }
/* 2227 */           if ((as[0].equals("ofConnectedTextures")) && (as.length >= 2))
/*      */           {
/* 2229 */             this.ofConnectedTextures = Integer.valueOf(as[1]).intValue();
/* 2230 */             this.ofConnectedTextures = Config.limit(this.ofConnectedTextures, 1, 3);
/*      */           }
/* 2232 */           if ((as[0].equals("ofWeather")) && (as.length >= 2))
/*      */           {
/* 2234 */             this.ofWeather = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2236 */           if ((as[0].equals("ofSky")) && (as.length >= 2))
/*      */           {
/* 2238 */             this.ofSky = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2240 */           if ((as[0].equals("ofStars")) && (as.length >= 2))
/*      */           {
/* 2242 */             this.ofStars = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2244 */           if ((as[0].equals("ofSunMoon")) && (as.length >= 2))
/*      */           {
/* 2246 */             this.ofSunMoon = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2248 */           if ((as[0].equals("ofChunkUpdates")) && (as.length >= 2))
/*      */           {
/* 2250 */             this.ofChunkUpdates = Integer.valueOf(as[1]).intValue();
/* 2251 */             this.ofChunkUpdates = Config.limit(this.ofChunkUpdates, 1, 5);
/*      */           }
/* 2253 */           if ((as[0].equals("ofChunkLoading")) && (as.length >= 2))
/*      */           {
/* 2255 */             this.ofChunkLoading = Integer.valueOf(as[1]).intValue();
/* 2256 */             this.ofChunkLoading = Config.limit(this.ofChunkLoading, 0, 2);
/*      */ 
/* 2258 */             updateChunkLoading();
/*      */           }
/* 2260 */           if ((as[0].equals("ofChunkUpdatesDynamic")) && (as.length >= 2))
/*      */           {
/* 2262 */             this.ofChunkUpdatesDynamic = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2264 */           if ((as[0].equals("ofTime")) && (as.length >= 2))
/*      */           {
/* 2266 */             this.ofTime = Integer.valueOf(as[1]).intValue();
/* 2267 */             this.ofTime = Config.limit(this.ofTime, 0, 3);
/*      */           }
/* 2269 */           if ((as[0].equals("ofClearWater")) && (as.length >= 2))
/*      */           {
/* 2271 */             this.ofClearWater = Boolean.valueOf(as[1]).booleanValue();
/* 2272 */             updateWaterOpacity();
/*      */           }
/* 2274 */           if ((as[0].equals("ofDepthFog")) && (as.length >= 2))
/*      */           {
/* 2276 */             this.ofDepthFog = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2278 */           if ((as[0].equals("ofAaLevel")) && (as.length >= 2))
/*      */           {
/* 2280 */             this.ofAaLevel = Integer.valueOf(as[1]).intValue();
/* 2281 */             this.ofAaLevel = Config.limit(this.ofAaLevel, 0, 16);
/*      */           }
/* 2283 */           if ((as[0].equals("ofAfLevel")) && (as.length >= 2))
/*      */           {
/* 2285 */             this.ofAfLevel = Integer.valueOf(as[1]).intValue();
/* 2286 */             this.ofAfLevel = Config.limit(this.ofAfLevel, 1, 16);
/*      */           }
/* 2288 */           if ((as[0].equals("ofProfiler")) && (as.length >= 2))
/*      */           {
/* 2290 */             this.ofProfiler = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2292 */           if ((as[0].equals("ofBetterSnow")) && (as.length >= 2))
/*      */           {
/* 2294 */             this.ofBetterSnow = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2296 */           if ((as[0].equals("ofSwampColors")) && (as.length >= 2))
/*      */           {
/* 2298 */             this.ofSwampColors = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2300 */           if ((as[0].equals("ofRandomMobs")) && (as.length >= 2))
/*      */           {
/* 2302 */             this.ofRandomMobs = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2304 */           if ((as[0].equals("ofSmoothBiomes")) && (as.length >= 2))
/*      */           {
/* 2306 */             this.ofSmoothBiomes = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2308 */           if ((as[0].equals("ofCustomFonts")) && (as.length >= 2))
/*      */           {
/* 2310 */             this.ofCustomFonts = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2312 */           if ((as[0].equals("ofCustomColors")) && (as.length >= 2))
/*      */           {
/* 2314 */             this.ofCustomColors = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2316 */           if ((as[0].equals("ofCustomSky")) && (as.length >= 2))
/*      */           {
/* 2318 */             this.ofCustomSky = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2320 */           if ((as[0].equals("ofShowCapes")) && (as.length >= 2))
/*      */           {
/* 2322 */             this.ofShowCapes = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2324 */           if ((as[0].equals("ofNaturalTextures")) && (as.length >= 2))
/*      */           {
/* 2326 */             this.ofNaturalTextures = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2328 */           if ((as[0].equals("ofLazyChunkLoading")) && (as.length >= 2))
/*      */           {
/* 2330 */             this.ofLazyChunkLoading = Boolean.valueOf(as[1]).booleanValue();
/*      */           }
/* 2332 */           if ((as[0].equals("ofFullscreenMode")) && (as.length >= 2))
/*      */           {
/* 2334 */             this.ofFullscreenMode = as[1];
/*      */           }
/*      */ 
/*      */         }
/*      */         catch (Exception exception1)
/*      */         {
/* 2340 */           Config.dbg("Skipping bad option: " + s);
/* 2341 */           exception1.printStackTrace();
/*      */         }
/*      */       }
/*      */ 
/* 2345 */       KeyBinding.resetKeyBindingArrayAndHash();
/* 2346 */       bufferedreader.close();
/*      */     }
/*      */     catch (Exception exception)
/*      */     {
/* 2350 */       Config.warn("Failed to load options");
/* 2351 */       exception.printStackTrace();
/*      */     }
/*      */   }
/*      */ 
/*      */   private float parseFloat(String par1Str)
/*      */   {
/* 2361 */     return par1Str.equals("false") ? 0.0F : par1Str.equals("true") ? 1.0F : Float.parseFloat(par1Str);
/*      */   }
/*      */ 
/*      */   public void saveOptions()
/*      */   {
/* 2370 */     if (Reflector.FMLClientHandler.exists())
/*      */     {
/* 2373 */       Object fml = Reflector.call(Reflector.FMLClientHandler_instance, new Object[0]);
/* 2374 */       if ((fml != null) && (Reflector.callBoolean(fml, Reflector.FMLClientHandler_isLoading, new Object[0]))) {
/* 2375 */         return;
/*      */       }
/*      */     }
/*      */     try
/*      */     {
/* 2380 */       PrintWriter var1 = new PrintWriter(new FileWriter(this.optionsFile));
/* 2381 */       var1.println("music:" + this.musicVolume);
/* 2382 */       var1.println("sound:" + this.soundVolume);
/* 2383 */       var1.println("invertYMouse:" + this.invertMouse);
/* 2384 */       var1.println("mouseSensitivity:" + this.mouseSensitivity);
/* 2385 */       var1.println("fov:" + this.fovSetting);
/* 2386 */       var1.println("gamma:" + this.gammaSetting);
/* 2387 */       var1.println("viewDistance:" + this.renderDistance);
/* 2388 */       var1.println("guiScale:" + this.guiScale);
/* 2389 */       var1.println("particles:" + this.particleSetting);
/* 2390 */       var1.println("bobView:" + this.viewBobbing);
/* 2391 */       var1.println("anaglyph3d:" + this.anaglyph);
/* 2392 */       var1.println("advancedOpengl:" + this.advancedOpengl);
/* 2393 */       var1.println("fpsLimit:" + this.limitFramerate);
/* 2394 */       var1.println("difficulty:" + this.difficulty);
/* 2395 */       var1.println("fancyGraphics:" + this.fancyGraphics);
/* 2396 */       var1.println("ao:" + this.ambientOcclusion);
/* 2397 */       var1.println("clouds:" + this.clouds);
/* 2398 */       var1.println("skin:" + this.skin);
/* 2399 */       var1.println("lastServer:" + this.lastServer);
/* 2400 */       var1.println("lang:" + this.language);
/* 2401 */       var1.println("chatVisibility:" + this.chatVisibility);
/* 2402 */       var1.println("chatColors:" + this.chatColours);
/* 2403 */       var1.println("chatLinks:" + this.chatLinks);
/* 2404 */       var1.println("chatLinksPrompt:" + this.chatLinksPrompt);
/* 2405 */       var1.println("chatOpacity:" + this.chatOpacity);
/* 2406 */       var1.println("serverTextures:" + this.serverTextures);
/* 2407 */       var1.println("snooperEnabled:" + this.snooperEnabled);
/* 2408 */       var1.println("fullscreen:" + this.fullScreen);
/* 2409 */       var1.println("enableVsync:" + this.enableVsync);
/* 2410 */       var1.println("hideServerAddress:" + this.hideServerAddress);
/* 2411 */       var1.println("advancedItemTooltips:" + this.advancedItemTooltips);
/* 2412 */       var1.println("pauseOnLostFocus:" + this.pauseOnLostFocus);
/* 2413 */       var1.println("showCape:" + this.showCape);
/* 2414 */       var1.println("touchscreen:" + this.touchscreen);
/* 2415 */       var1.println("overrideWidth:" + this.overrideWidth);
/* 2416 */       var1.println("overrideHeight:" + this.overrideHeight);
/* 2417 */       var1.println("heldItemTooltips:" + this.heldItemTooltips);
/* 2418 */       var1.println("chatHeightFocused:" + this.chatHeightFocused);
/* 2419 */       var1.println("chatHeightUnfocused:" + this.chatHeightUnfocused);
/* 2420 */       var1.println("chatScale:" + this.chatScale);
/* 2421 */       var1.println("chatWidth:" + this.chatWidth);
/*      */ 
/* 2423 */       for (int var2 = 0; var2 < this.keyBindings.length; var2++)
/*      */       {
/* 2425 */         var1.println("key_" + this.keyBindings[var2].keyDescription + ":" + this.keyBindings[var2].keyCode);
/*      */       }
/*      */ 
/* 2428 */       var1.close();
/*      */     }
/*      */     catch (Exception var3)
/*      */     {
/* 2432 */       this.mc.getLogAgent().logWarning("Failed to save options");
/* 2433 */       var3.printStackTrace();
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/* 2439 */       PrintWriter printwriter = new PrintWriter(new FileWriter(this.optionsFileOF));
/*      */ 
/* 2441 */       printwriter.println("ofRenderDistanceFine:" + this.ofRenderDistanceFine);
/* 2442 */       printwriter.println("ofLimitFramerateFine:" + this.ofLimitFramerateFine);
/* 2443 */       printwriter.println("ofFogType:" + this.ofFogType);
/* 2444 */       printwriter.println("ofFogStart:" + this.ofFogStart);
/* 2445 */       printwriter.println("ofMipmapLevel:" + this.ofMipmapLevel);
/* 2446 */       printwriter.println("ofMipmapType:" + this.ofMipmapType);
/* 2447 */       printwriter.println("ofLoadFar:" + this.ofLoadFar);
/* 2448 */       printwriter.println("ofPreloadedChunks:" + this.ofPreloadedChunks);
/* 2449 */       printwriter.println("ofOcclusionFancy:" + this.ofOcclusionFancy);
/* 2450 */       printwriter.println("ofSmoothFps:" + this.ofSmoothFps);
/* 2451 */       printwriter.println("ofSmoothWorld:" + this.ofSmoothWorld);
/* 2452 */       printwriter.println("ofAoLevel:" + this.ofAoLevel);
/* 2453 */       printwriter.println("ofClouds:" + this.ofClouds);
/* 2454 */       printwriter.println("ofCloudsHeight:" + this.ofCloudsHeight);
/* 2455 */       printwriter.println("ofTrees:" + this.ofTrees);
/* 2456 */       printwriter.println("ofGrass:" + this.ofGrass);
/* 2457 */       printwriter.println("ofDroppedItems:" + this.ofDroppedItems);
/* 2458 */       printwriter.println("ofRain:" + this.ofRain);
/* 2459 */       printwriter.println("ofWater:" + this.ofWater);
/* 2460 */       printwriter.println("ofAnimatedWater:" + this.ofAnimatedWater);
/* 2461 */       printwriter.println("ofAnimatedLava:" + this.ofAnimatedLava);
/* 2462 */       printwriter.println("ofAnimatedFire:" + this.ofAnimatedFire);
/* 2463 */       printwriter.println("ofAnimatedPortal:" + this.ofAnimatedPortal);
/* 2464 */       printwriter.println("ofAnimatedRedstone:" + this.ofAnimatedRedstone);
/* 2465 */       printwriter.println("ofAnimatedExplosion:" + this.ofAnimatedExplosion);
/* 2466 */       printwriter.println("ofAnimatedFlame:" + this.ofAnimatedFlame);
/* 2467 */       printwriter.println("ofAnimatedSmoke:" + this.ofAnimatedSmoke);
/* 2468 */       printwriter.println("ofVoidParticles:" + this.ofVoidParticles);
/* 2469 */       printwriter.println("ofWaterParticles:" + this.ofWaterParticles);
/* 2470 */       printwriter.println("ofPortalParticles:" + this.ofPortalParticles);
/* 2471 */       printwriter.println("ofPotionParticles:" + this.ofPotionParticles);
/* 2472 */       printwriter.println("ofDrippingWaterLava:" + this.ofDrippingWaterLava);
/* 2473 */       printwriter.println("ofAnimatedTerrain:" + this.ofAnimatedTerrain);
/* 2474 */       printwriter.println("ofAnimatedTextures:" + this.ofAnimatedTextures);
/* 2475 */       printwriter.println("ofAnimatedItems:" + this.ofAnimatedItems);
/* 2476 */       printwriter.println("ofRainSplash:" + this.ofRainSplash);
/* 2477 */       printwriter.println("ofLagometer:" + this.ofLagometer);
/* 2478 */       printwriter.println("ofAutoSaveTicks:" + this.ofAutoSaveTicks);
/* 2479 */       printwriter.println("ofBetterGrass:" + this.ofBetterGrass);
/* 2480 */       printwriter.println("ofConnectedTextures:" + this.ofConnectedTextures);
/* 2481 */       printwriter.println("ofWeather:" + this.ofWeather);
/* 2482 */       printwriter.println("ofSky:" + this.ofSky);
/* 2483 */       printwriter.println("ofStars:" + this.ofStars);
/* 2484 */       printwriter.println("ofSunMoon:" + this.ofSunMoon);
/* 2485 */       printwriter.println("ofChunkUpdates:" + this.ofChunkUpdates);
/* 2486 */       printwriter.println("ofChunkLoading:" + this.ofChunkLoading);
/* 2487 */       printwriter.println("ofChunkUpdatesDynamic:" + this.ofChunkUpdatesDynamic);
/* 2488 */       printwriter.println("ofTime:" + this.ofTime);
/* 2489 */       printwriter.println("ofClearWater:" + this.ofClearWater);
/* 2490 */       printwriter.println("ofDepthFog:" + this.ofDepthFog);
/* 2491 */       printwriter.println("ofAaLevel:" + this.ofAaLevel);
/* 2492 */       printwriter.println("ofAfLevel:" + this.ofAfLevel);
/* 2493 */       printwriter.println("ofProfiler:" + this.ofProfiler);
/* 2494 */       printwriter.println("ofBetterSnow:" + this.ofBetterSnow);
/* 2495 */       printwriter.println("ofSwampColors:" + this.ofSwampColors);
/* 2496 */       printwriter.println("ofRandomMobs:" + this.ofRandomMobs);
/* 2497 */       printwriter.println("ofSmoothBiomes:" + this.ofSmoothBiomes);
/* 2498 */       printwriter.println("ofCustomFonts:" + this.ofCustomFonts);
/* 2499 */       printwriter.println("ofCustomColors:" + this.ofCustomColors);
/* 2500 */       printwriter.println("ofCustomSky:" + this.ofCustomSky);
/* 2501 */       printwriter.println("ofShowCapes:" + this.ofShowCapes);
/* 2502 */       printwriter.println("ofNaturalTextures:" + this.ofNaturalTextures);
/* 2503 */       printwriter.println("ofLazyChunkLoading:" + this.ofLazyChunkLoading);
/* 2504 */       printwriter.println("ofFullscreenMode:" + this.ofFullscreenMode);
/*      */ 
/* 2506 */       printwriter.close();
/*      */     }
/*      */     catch (Exception exception)
/*      */     {
/* 2510 */       Config.warn("Failed to save options");
/* 2511 */       exception.printStackTrace();
/*      */     }
/*      */ 
/* 2514 */     sendSettingsToServer();
/*      */   }
/*      */ 
/*      */   public void sendSettingsToServer()
/*      */   {
/* 2522 */     if (this.mc.thePlayer != null)
/*      */     {
/* 2524 */       this.mc.thePlayer.sendQueue.addToSendQueue(new Packet204ClientInfo(this.language, this.renderDistance, this.chatVisibility, this.chatColours, this.difficulty, this.showCape));
/*      */     }
/*      */   }
/*      */ 
/*      */   public void resetSettings()
/*      */   {
/* 2534 */     this.renderDistance = 1;
/*      */ 
/* 2536 */     this.ofRenderDistanceFine = renderDistanceToFine(this.renderDistance);
/*      */ 
/* 2538 */     this.viewBobbing = true;
/* 2539 */     this.anaglyph = false;
/* 2540 */     this.advancedOpengl = false;
/*      */ 
/* 2542 */     this.limitFramerate = 0;
/* 2543 */     this.enableVsync = false;
/* 2544 */     updateVSync();
/* 2545 */     this.ofLimitFramerateFine = 0;
/*      */ 
/* 2547 */     this.fancyGraphics = true;
/* 2548 */     this.ambientOcclusion = 2;
/* 2549 */     this.clouds = true;
/* 2550 */     this.fovSetting = 0.0F;
/* 2551 */     this.gammaSetting = 0.0F;
/* 2552 */     this.guiScale = 0;
/* 2553 */     this.particleSetting = 0;
/* 2554 */     this.heldItemTooltips = true;
/*      */ 
/* 2556 */     this.ofFogType = 1;
/* 2557 */     this.ofFogStart = 0.8F;
/* 2558 */     this.ofMipmapLevel = 0;
/* 2559 */     this.ofMipmapType = 0;
/* 2560 */     this.ofLoadFar = false;
/* 2561 */     this.ofPreloadedChunks = 0;
/* 2562 */     this.ofOcclusionFancy = false;
/* 2563 */     this.ofSmoothFps = false;
/* 2564 */     this.ofSmoothWorld = Config.isSingleProcessor();
/* 2565 */     this.ofLazyChunkLoading = Config.isSingleProcessor();
/*      */ 
/* 2567 */     this.ofAoLevel = 1.0F;
/*      */ 
/* 2569 */     this.ofAaLevel = 0;
/* 2570 */     this.ofAfLevel = 1;
/*      */ 
/* 2572 */     this.ofClouds = 0;
/* 2573 */     this.ofCloudsHeight = 0.0F;
/* 2574 */     this.ofTrees = 0;
/* 2575 */     this.ofGrass = 0;
/* 2576 */     this.ofRain = 0;
/* 2577 */     this.ofWater = 0;
/* 2578 */     this.ofBetterGrass = 3;
/* 2579 */     this.ofAutoSaveTicks = 4000;
/* 2580 */     this.ofLagometer = false;
/* 2581 */     this.ofProfiler = false;
/* 2582 */     this.ofWeather = true;
/* 2583 */     this.ofSky = true;
/* 2584 */     this.ofStars = true;
/* 2585 */     this.ofSunMoon = true;
/* 2586 */     this.ofChunkUpdates = 1;
/* 2587 */     this.ofChunkLoading = 0;
/* 2588 */     this.ofChunkUpdatesDynamic = false;
/* 2589 */     this.ofTime = 0;
/* 2590 */     this.ofClearWater = false;
/* 2591 */     this.ofDepthFog = true;
/* 2592 */     this.ofBetterSnow = false;
/* 2593 */     this.ofFullscreenMode = "Стандарт";
/* 2594 */     this.ofSwampColors = true;
/* 2595 */     this.ofRandomMobs = true;
/* 2596 */     this.ofSmoothBiomes = true;
/* 2597 */     this.ofCustomFonts = true;
/* 2598 */     this.ofCustomColors = true;
/* 2599 */     this.ofCustomSky = true;
/* 2600 */     this.ofShowCapes = true;
/* 2601 */     this.ofConnectedTextures = 2;
/* 2602 */     this.ofNaturalTextures = false;
/*      */ 
/* 2604 */     this.ofAnimatedWater = 0;
/* 2605 */     this.ofAnimatedLava = 0;
/* 2606 */     this.ofAnimatedFire = true;
/* 2607 */     this.ofAnimatedPortal = true;
/* 2608 */     this.ofAnimatedRedstone = true;
/* 2609 */     this.ofAnimatedExplosion = true;
/* 2610 */     this.ofAnimatedFlame = true;
/* 2611 */     this.ofAnimatedSmoke = true;
/* 2612 */     this.ofVoidParticles = true;
/* 2613 */     this.ofWaterParticles = true;
/* 2614 */     this.ofRainSplash = true;
/* 2615 */     this.ofPortalParticles = true;
/* 2616 */     this.ofPotionParticles = true;
/* 2617 */     this.ofDrippingWaterLava = true;
/* 2618 */     this.ofAnimatedTerrain = true;
/* 2619 */     this.ofAnimatedItems = true;
/* 2620 */     this.ofAnimatedTextures = true;
/*      */ 
/* 2622 */     this.mc.renderGlobal.updateCapes();
/*      */ 
/* 2624 */     updateWaterOpacity();
/* 2625 */     this.mc.renderGlobal.setAllRenderersVisible();
/*      */ 
/* 2627 */     this.mc.refreshResources();
/*      */ 
/* 2629 */     saveOptions();
/*      */   }
/*      */ 
/*      */   public void updateVSync()
/*      */   {
/* 2635 */     Display.setVSyncEnabled(this.enableVsync);
/*      */   }
/*      */ 
/*      */   private static int fineToRenderDistance(int rdFine)
/*      */   {
/* 2643 */     int rd = 3;
/* 2644 */     if (rdFine > 32)
/* 2645 */       rd = 2;
/* 2646 */     if (rdFine > 64)
/* 2647 */       rd = 1;
/* 2648 */     if (rdFine > 128) {
/* 2649 */       rd = 0;
/*      */     }
/* 2651 */     return rd;
/*      */   }
/*      */ 
/*      */   private static int renderDistanceToFine(int rd)
/*      */   {
/* 2659 */     return 32 << 3 - rd;
/*      */   }
/*      */ 
/*      */   private static int fineToLimitFramerate(int fine)
/*      */   {
/* 2668 */     int limit = 2;
/*      */ 
/* 2670 */     if (fine > 35) {
/* 2671 */       limit = 1;
/*      */     }
/* 2673 */     if (fine >= 200) {
/* 2674 */       limit = 0;
/*      */     }
/* 2676 */     if (fine <= 0) {
/* 2677 */       limit = 0;
/*      */     }
/* 2679 */     return limit;
/*      */   }
/*      */ 
/*      */   private static int limitFramerateToFine(int limit)
/*      */   {
/* 2687 */     switch (limit)
/*      */     {
/*      */     case 0:
/* 2691 */       return 0;
/*      */     case 1:
/* 2694 */       return 120;
/*      */     case 2:
/* 2697 */       return 35;
/*      */     }
/*      */ 
/* 2700 */     return 0;
/*      */   }
/*      */ 
/*      */   public boolean shouldRenderClouds()
/*      */   {
/* 2710 */     return (this.ofRenderDistanceFine > 64) && (this.clouds);
/*      */   }
/*      */ }

/* Location:           D:\Minecraft_Job\Minecraft-adventures.ru\OptiFine.zip
 * Qualified Name:     net.minecraft.client.settings.GameSettings
 * JD-Core Version:    0.6.2
 */