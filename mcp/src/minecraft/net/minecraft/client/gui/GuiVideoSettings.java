package net.minecraft.client.gui;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.src.GuiAnimationSettingsOF;
import net.minecraft.src.GuiDetailSettingsOF;
import net.minecraft.src.GuiOtherSettingsOF;
import net.minecraft.src.GuiPerformanceSettingsOF;
import net.minecraft.src.GuiQualitySettingsOF;

public class GuiVideoSettings extends GuiScreen
{
    private GuiScreen parentGuiScreen;

    /** The title string that is displayed in the top-center of the screen. */
    protected String screenTitle = "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0412\u0438\u0434\u0435\u043e";

    /** GUI game settings */
    private GameSettings guiGameSettings;

    /**
     * True if the system is 64-bit (using a simple indexOf test on a system property)
     */
    private boolean is64bit;

    /** An array of all of EnumOption's video options. */
    private static EnumOptions[] videoOptions = new EnumOptions[] {EnumOptions.GRAPHICS, EnumOptions.RENDER_DISTANCE_FINE, EnumOptions.AMBIENT_OCCLUSION, EnumOptions.FRAMERATE_LIMIT_FINE, EnumOptions.AO_LEVEL, EnumOptions.VIEW_BOBBING, EnumOptions.GUI_SCALE, EnumOptions.ADVANCED_OPENGL, EnumOptions.GAMMA, EnumOptions.CHUNK_LOADING, EnumOptions.FOG_FANCY, EnumOptions.FOG_START, EnumOptions.ANAGLYPH};
    private int lastMouseX = 0;
    private int lastMouseY = 0;
    private long mouseStillTime = 0L;

    public GuiVideoSettings(GuiScreen par1GuiScreen, GameSettings par2GameSettings)
    {
        this.parentGuiScreen = par1GuiScreen;
        this.guiGameSettings = par2GameSettings;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        this.screenTitle = I18n.getString("options.videoTitle");
        this.buttonList.clear();
        this.is64bit = false;
        String[] var1 = new String[] {"sun.arch.data.model", "com.ibm.vm.bitmode", "os.arch"};
        String[] var2 = var1;
        int var3 = var1.length;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            String var5 = var2[var4];
            String var6 = System.getProperty(var5);

            if (var6 != null && var6.contains("64"))
            {
                this.is64bit = true;
                break;
            }
        }

        boolean var12 = false;
        boolean var11 = !this.is64bit;
        EnumOptions[] var13 = videoOptions;
        int var14 = var13.length;
        boolean var7 = false;
        int var9;
        int var15;

        for (var15 = 0; var15 < var14; ++var15)
        {
            EnumOptions var8 = var13[var15];
            var9 = this.width / 2 - 155 + var15 % 2 * 160;
            int var10 = this.height / 6 + 21 * (var15 / 2) - 10;

            if (var8.getEnumFloat())
            {
                this.buttonList.add(new GuiSlider(var8.returnEnumOrdinal(), var9, var10, var8, this.guiGameSettings.getKeyBinding(var8), this.guiGameSettings.getOptionFloatValue(var8)));
            }
            else
            {
                this.buttonList.add(new GuiSmallButton(var8.returnEnumOrdinal(), var9, var10, var8, this.guiGameSettings.getKeyBinding(var8)));
            }
        }

        int var16 = this.height / 6 + 21 * (var15 / 2) - 10;
        boolean var17 = false;
        var9 = this.width / 2 - 155 + 160;
        this.buttonList.add(new GuiSmallButton(102, var9, var16, "\u041a\u0430\u0447\u0435\u0441\u0442\u0432\u043e..."));
        var16 += 21;
        var9 = this.width / 2 - 155 + 0;
        this.buttonList.add(new GuiSmallButton(101, var9, var16, "\u0414\u0435\u0442\u0430\u043b\u0438..."));
        var9 = this.width / 2 - 155 + 160;
        this.buttonList.add(new GuiSmallButton(112, var9, var16, "\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c..."));
        var16 += 21;
        var9 = this.width / 2 - 155 + 0;
        this.buttonList.add(new GuiSmallButton(111, var9, var16, "\u0410\u043d\u0438\u043c\u0430\u0446\u0438\u044f..."));
        var9 = this.width / 2 - 155 + 160;
        this.buttonList.add(new GuiSmallButton(210, var9, var16, "\u0421\u0431\u0440\u043e\u0441\u0438\u0442\u044c \u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0413\u0440\u0430\u0444\u0438\u043a\u0438..."));
        this.buttonList.add(new GuiButton(200, this.width / 2 - 100, this.height / 6 + 168 + 11, I18n.getString("gui.done")));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton par1GuiButton)
    {
        if (par1GuiButton.enabled)
        {
            int var2 = this.guiGameSettings.guiScale;

            if (par1GuiButton.id < 100 && par1GuiButton instanceof GuiSmallButton)
            {
                this.guiGameSettings.setOptionValue(((GuiSmallButton)par1GuiButton).returnEnumOptions(), 1);
                par1GuiButton.displayString = this.guiGameSettings.getKeyBinding(EnumOptions.getEnumOptions(par1GuiButton.id));
            }

            if (par1GuiButton.id == 200)
            {
                this.mc.gameSettings.saveOptions();
                this.mc.displayGuiScreen(this.parentGuiScreen);
            }

            if (this.guiGameSettings.guiScale != var2)
            {
                ScaledResolution var3 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
                int var4 = var3.getScaledWidth();
                int var5 = var3.getScaledHeight();
                this.setWorldAndResolution(this.mc, var4, var5);
            }

            if (par1GuiButton.id == 101)
            {
                this.mc.gameSettings.saveOptions();
                GuiDetailSettingsOF var6 = new GuiDetailSettingsOF(this, this.guiGameSettings);
                this.mc.displayGuiScreen(var6);
            }

            if (par1GuiButton.id == 102)
            {
                this.mc.gameSettings.saveOptions();
                GuiQualitySettingsOF var7 = new GuiQualitySettingsOF(this, this.guiGameSettings);
                this.mc.displayGuiScreen(var7);
            }

            if (par1GuiButton.id == 111)
            {
                this.mc.gameSettings.saveOptions();
                GuiAnimationSettingsOF var8 = new GuiAnimationSettingsOF(this, this.guiGameSettings);
                this.mc.displayGuiScreen(var8);
            }

            if (par1GuiButton.id == 112)
            {
                this.mc.gameSettings.saveOptions();
                GuiPerformanceSettingsOF var9 = new GuiPerformanceSettingsOF(this, this.guiGameSettings);
                this.mc.displayGuiScreen(var9);
            }

            if (par1GuiButton.id == 122)
            {
                this.mc.gameSettings.saveOptions();
                GuiOtherSettingsOF var10 = new GuiOtherSettingsOF(this, this.guiGameSettings);
                this.mc.displayGuiScreen(var10);
            }
            
            if (par1GuiButton.id == 210)
            {
                this.mc.gameSettings.saveOptions();
                GuiYesNo scaledresolution = new GuiYesNo(this, "\u0421\u0431\u0440\u043e\u0441\u0438\u0442\u044c \u0432\u0438\u0434\u0435\u043e\u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u043a \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u043e\u043c\u0443 \u0441\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u044e?", "", 9999);
                this.mc.displayGuiScreen(scaledresolution);
            }
            
            if (par1GuiButton.id != EnumOptions.CLOUD_HEIGHT.ordinal())
            {
                ScaledResolution scaledresolution1 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
                int i = scaledresolution1.getScaledWidth();
                int j = scaledresolution1.getScaledHeight();
                this.setWorldAndResolution(this.mc, i, j);
            }
            if (par1GuiButton.id == EnumOptions.AO_LEVEL.ordinal())
            {
                return;
            }
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, this.screenTitle, this.width / 2, this.is64bit ? 20 : 5, 16777215);

        if (!this.is64bit && this.guiGameSettings.renderDistance == 0)
        {
            ;
        }

        super.drawScreen(par1, par2, par3);

        if (Math.abs(par1 - this.lastMouseX) <= 5 && Math.abs(par2 - this.lastMouseY) <= 5)
        {
            short var4 = 700;

            if (System.currentTimeMillis() >= this.mouseStillTime + (long)var4)
            {
                int var5 = this.width / 2 - 150;
                int var6 = this.height / 6 - 5;

                if (par2 <= var6 + 98)
                {
                    var6 += 105;
                }

                int var7 = var5 + 150 + 150;
                int var8 = var6 + 84 + 10;
                GuiButton var9 = this.getSelectedButton(par1, par2);

                if (var9 != null)
                {
                    String var10 = this.getButtonName(var9.displayString);
                    String[] var11 = this.getTooltipLines(var10);

                    if (var11 == null)
                    {
                        return;
                    }

                    this.drawGradientRect(var5, var6, var7, var8, -536870912, -536870912);

                    for (int var12 = 0; var12 < var11.length; ++var12)
                    {
                        String var13 = var11[var12];
                        this.fontRenderer.drawStringWithShadow(var13, var5 + 5, var6 + 5 + var12 * 11, 14540253);
                    }
                }
            }
        }
        else
        {
            this.lastMouseX = par1;
            this.lastMouseY = par2;
            this.mouseStillTime = System.currentTimeMillis();
        }
    }

    private String[] getTooltipLines(String btnName)
    {
        return btnName.equals("\u0413\u0440\u0430\u0444\u0438\u043a\u0430") ? new String[] {"\u041a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f", "  \u0411\u044b\u0441\u0442\u0440\u043e - \u043f\u043b\u043e\u0445\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e, (\u0431\u044b\u0441\u0442\u0440\u043e)", "  \u0414\u0435\u0442\u0430\u043b\u044c\u043d\u043e - \u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0432\u043d\u0435\u0448\u043d\u0435\u0433\u043e \u0432\u0438\u0434\u0430 \u043e\u0431\u043b\u0430\u043a\u043e\u0432, \u043b\u0438\u0441\u0442\u044c\u0435\u0432, \u0432\u043e\u0434\u044b,", "\u0442\u0435\u043d\u0435\u0439 \u0438 \u0442\u0440\u0430\u0432\u044b."}: (btnName.equals("\u0414\u0438\u0441\u0442\u0430\u043d\u0446\u0438\u044f") ? new String[] {"\u0412\u0438\u0434\u0438\u043c\u0430\u044f \u0434\u0438\u0441\u0442\u0430\u043d\u0446\u0438\u044f", "  \u0411\u043b\u0438\u0437\u043a\u043e - 32 \u043c. (\u0431\u044b\u0441\u0442\u0440\u043e)", "  \u0421\u0440\u0435\u0434\u043d\u0435 - 64 \u043c. (\u0431\u044b\u0441\u0442\u0440\u0435\u0435)", "  \u041d\u043e\u0440\u043c\u0430\u043b\u044c\u043d\u043e - 128 \u043c.", "  \u0414\u0430\u043b\u0435\u043a\u043e - 256 \u043c. (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u0435\u0435)", "  \u042d\u043a\u0441\u0442\u0440\u0430 - 512 \u043c. (\u043e\u0447\u0435\u043d\u044c \u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "\u0414\u0438\u0441\u0442\u0430\u043d\u0446\u0438\u044f \u042d\u043a\u0441\u0442\u0440\u0430 \u043e\u0447\u0435\u043d\u044c \u0442\u0440\u0435\u0431\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u0430 \u043a \u0440\u0435\u0441\u0443\u0440\u0441\u0430\u043c \u041f\u041a!"}: (btnName.equals("\u041c\u044f\u0433\u043a\u043e\u0435 \u041e\u0441\u0432\u0435\u0449\u0435\u043d\u0438\u0435") ? new String[] {"\u041c\u044f\u0433\u043a\u043e\u0435 \u041e\u0441\u0432\u0435\u0449\u0435\u043d\u0438\u0435 (\u0441\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u0435)", "  \u0412bI\u041a\u041b - \u043d\u0435\u0442 \u0441\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u044f (\u0431\u044b\u0441\u0442\u0440\u043e)", "  1% - \u0441\u043b\u0430\u0431\u043e\u0435 \u0441\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u0435 \u0441\u0432\u0435\u0442\u0430, (\u0441\u0440\u0435\u0434\u043d\u0435)", "  100% - \u043c\u0430\u043a\u0441. \u0441\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u0435 \u0441\u0432\u0435\u0442\u0430, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)"}: (btnName.equals("FPS") ? new String[] {"\u041a\u0430\u0434\u0440\u043e\u0432 \u0432 \u0441\u0435\u043a\u0443\u043d\u0434\u0443", "  \u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c - \u0431\u0435\u0437 \u043e\u0433\u0440\u0430\u043d\u0438\u0447\u0435\u043d\u0438\u0439 (\u0431\u044b\u0441\u0442\u0440\u043e)", "  \u0411\u0430\u043b\u0430\u043d\u0441 - \u043b\u0438\u043c\u0438\u0442 120 FPS (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u0435\u0435)", "  \u042d\u043d\u0435\u0433\u0440\u043e\u0441\u0431\u0435\u0440\u0435\u0433\u0430\u044e\u0449\u0438\u0439 - \u043b\u0438\u043c\u0438\u0442 40 FPS (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "  VSyns - \u043e\u0433\u0440\u0430\u043d\u0438\u0447\u0438\u0432\u0430\u0435\u0442\u0441\u044f \u0432\u043e\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442\u044c\u044e \u043c\u043e\u043d\u0438\u0442\u043e\u0440\u0430", "\u0411\u0430\u043b\u0430\u043d\u0441 \u0438 \u042d\u043d\u0435\u0440\u0433\u043e\u0441\u0431\u0435\u0440\u0435\u0436\u0435\u043d\u0438\u0435 \u0443\u043c\u0435\u043d\u044c\u0448\u0430\u044e\u0442 FPS, \u0434\u0430\u0436\u0435 \u0435\u0441\u043b\u0438", "\u043f\u0440\u0435\u0434\u0435\u043b\u044c\u043d\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043d\u0435 \u0434\u043e\u0441\u0442\u0438\u0433\u043d\u0443\u0442\u043e."}: (btnName.equals("3D \u0440\u0435\u0436\u0438\u043c") ? new String[] {"3D \u0440\u0435\u0436\u0438\u043c \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0441\u043e \u0441\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u043c\u0438 3D \u043e\u0447\u043a\u0430\u043c\u0438"}: (btnName.equals("\u0420\u0430\u0441\u043a\u0430\u0447\u0438\u0432. \u043a\u0430\u043c\u0435\u0440\u044b") ? new String[] {"\u0420\u0435\u0430\u043b\u0438\u0441\u0442\u0438\u0447\u043d\u043e\u0435 \u043f\u043e\u043a\u0430\u0447\u0438\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u043c\u0435\u0440\u044b \u043f\u0440\u0438 \u0431\u0435\u0433\u0435.", "\u0412\u044b\u043a\u043b\u044e\u0447\u0438\u0442\u0435 \u0434\u043b\u044f \u0434\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u044f \u043b\u0443\u0447\u0448\u0435\u0439 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438."}: (btnName.equals("\u0418\u043d\u0442\u0435\u0440\u0444\u0435\u0439\u0441") ? new String[] {"\u0418\u043d\u0442\u0435\u0440\u0444\u0435\u0439\u0441", "\u041c\u0435\u043b\u043a\u0438\u0439 \u0438\u043d\u0442\u0435\u0440\u0444\u0435\u0439\u0441 \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u0435\u0442\u0441\u044f \u0431\u044b\u0441\u0442\u0440\u0435\u0435."}: (btnName.equals("\u0423\u043b\u0443\u0447\u0448\u0435\u043d\u044b\u0439 OpenGL") ? new String[] {"\u041d\u0430\u0445\u043e\u0434\u0438\u0442 \u0438 \u043f\u0440\u043e\u0440\u0438\u0441\u043e\u0432\u044b\u0432\u0430\u0435\u0442 \u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0438\u0434\u0438\u043c\u0443\u044e \u0433\u0435\u043e\u043c\u0435\u0442\u0440\u0438\u044e", "  \u0412\u042b\u041a\u041b - \u0432\u0441\u044f \u0433\u0435\u043e\u043c\u0435\u0442\u0440\u0438\u044f \u043f\u0440\u043e\u0440\u0438\u0441\u043e\u0432\u044b\u0432\u0430\u0435\u0442\u0441\u044f, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "  \u0411\u044b\u0441\u0442\u0440\u043e - \u043f\u0440\u043e\u0440\u0438\u0441\u043e\u0432\u043a\u0430 \u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0438\u0434\u0438\u043c\u043e\u0439 \u0433\u0435\u043e\u043c\u0435\u0442\u0440\u0438\u0438.", "  \u0412\u041a\u041b -  \u043f\u043e\u0437\u0432\u043e\u043b\u044f\u0435\u0442 \u0438\u0437\u0431\u0435\u0436\u0430\u0442\u044c \u0432\u0438\u0437\u0443\u0430\u043b\u044c\u043d\u044b\u0445 \u0430\u0440\u0442\u0435\u0444\u0430\u043a\u0442\u043e\u0432.", "\u041e\u043f\u0446\u0438\u044f \u0434\u043e\u0441\u0442\u0443\u043f\u043d\u0430, \u0442\u043e\u043b\u044c\u043a\u043e \u0435\u0441\u043b\u0438 \u043e\u043d\u0430", "\u043f\u043e\u0434\u0434\u0435\u0440\u0436\u0438\u0432\u0430\u0435\u0442\u0441\u044f \u0432\u0438\u0434\u0435\u043e\u043a\u0430\u0440\u0442\u043e\u0439."}: (btnName.equals("\u0422\u0443\u043c\u0430\u043d") ? new String[] {"\u0422\u0438\u043f \u0442\u0443\u043c\u0430\u043d\u0430", "  \u0411\u044b\u0441\u0442\u0440\u043e - \u0431\u044b\u0441\u0442\u0440\u044b\u0439 \u0442\u0443\u043c\u0430\u043d", "  \u0412\u041a\u041b - \u043a\u0440\u0430\u0441\u0438\u0432\u044b\u0439 \u0442\u0443\u043c\u0430\u043d, \u043b\u0443\u0447\u0448\u0438\u0439 \u043e\u0431\u0437\u043e\u0440", "  \u0412\u042b\u041a\u041b - \u0431\u0435\u0437 \u0442\u0443\u043c\u0430\u043d\u0430, (\u0431\u044b\u0441\u0442\u0440\u043e)", "\u0412\u041a\u041b - \u043e\u043f\u0446\u0438\u044f \u0434\u043e\u0441\u0442\u0443\u043f\u043d\u0430, \u0435\u0441\u043b\u0438 \u0442\u043e\u043b\u044c\u043a\u043e \u043e\u043d\u0430", "\u043f\u043e\u0434\u0434\u0435\u0440\u0436\u0438\u0432\u0430\u0435\u0442\u0441\u044f \u0432\u0438\u0434\u0435\u043e\u043a\u0430\u0440\u0442\u043e\u0439."}: (btnName.equals("\u041d\u0430\u0447\u0430\u043b\u043e \u0422\u0443\u043c\u0430\u043d\u0430") ? new String[] {"\u041d\u0430\u0447\u0430\u043b\u043e \u0442\u0443\u043c\u0430\u043d\u0430", "  0.2 - \u0442\u0443\u043c\u0430\u043d \u043d\u0430\u0447\u0438\u043d\u0430\u0435\u0442\u0441\u044f \u0432\u043e\u0437\u043b\u0435 \u0438\u0433\u0440\u043e\u043a\u0430", "  0.8 - \u0442\u0443\u043c\u0430\u043d \u043d\u0430\u0447\u0438\u043d\u0430\u0435\u0442\u0441\u044f \u0434\u0430\u043b\u0435\u043a\u043e \u043e\u0442 \u0438\u0433\u0440\u043e\u043a\u0430", "\u042d\u0442\u0430 \u043e\u043f\u0446\u0438\u044f \u043e\u0431\u044b\u0447\u043d\u043e \u043d\u0435 \u0432\u043b\u0438\u044f\u0435\u0442 \u043d\u0430 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c."}: (btnName.equals("\u042f\u0440\u043a\u043e\u0441\u0442\u044c") ? new String[] {"\u0423\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u0438\u0435 \u044f\u0440\u043a\u043e\u0441\u0442\u0438 \u0434\u043b\u044f \u0442\u0435\u043c\u043d\u044b\u0445 \u043e\u0431\u044a\u0435\u043a\u0442\u043e\u0432", "  \u041d\u0438\u0437\u043a\u0430\u044f - \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u0430\u044f \u044f\u0440\u043a\u043e\u0441\u0442\u044c", "  \u0412\u044b\u0441\u043e\u043a\u0430\u044f - \u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c \u0441\u0432\u0435\u0442\u0430  \u0434\u043b\u044f \u0442\u0435\u043c\u043d\u044b\u0445 \u043e\u0431\u044a\u0435\u043a\u0442\u043e\u0432", "\u042d\u0442\u0430 \u043e\u043f\u0446\u0438\u044f \u043d\u0435 \u043c\u0435\u043d\u044f\u0435\u0442 \u044f\u0440\u043a\u043e\u0441\u0442\u044c \u0434\u043b\u044f", "\u043f\u043e\u043b\u043d\u043e\u0441\u0442\u044c\u044e \u043d\u0435 \u043e\u0441\u0432\u0435\u0449\u0435\u043d\u043d\u044b\u0445 \u043e\u0431\u044a\u0435\u043a\u0442\u043e\u0432"}: (btnName.equals("\u0417\u0430\u0433\u0440.\u0427\u0430\u043d\u043a\u043e\u0432") ? new String[] {"\u0417\u0430\u0433\u0440.\u0427\u0430\u043d\u043a\u043e\u0432", "  \u0421\u0442\u0430\u043d\u0434\u0430\u0440\u0442 - \u043d\u0435 \u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439 FPS \u043f\u0440\u0438 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0435 \u0447\u0430\u043d\u043a\u043e\u0432", "  \u0413\u043b\u0430\u0434\u043a\u0430\u044f - \u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439 FPS", "  Multi-Core - \u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439 FPS, \u0437\u0430\u0433\u0440. \u043c\u0438\u0440\u0430  \u04453", "\u0413\u043b\u0430\u0434\u043a\u0430\u044f \u0438 Multi-Core \u0441\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0438\u0440\u0443\u044e\u0442 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0443 \u0447\u0430\u043d\u043a\u043e\u0432.", "Multi-Core \u043c\u043e\u0436\u0435\u0442 \u0443\u0432\u0435\u043b\u0438\u0447\u0438\u0442\u044c \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0443 \u043c\u0438\u0440\u0430 \u0432 3 \u0440\u0430\u0437\u0430 \u0438", "\u0443\u0432\u0435\u043b\u0438\u0447\u0438\u0442\u044c FPS \u0431\u043b\u0430\u0433\u043e\u0434\u0430\u0440\u044f \u0432\u0442\u043e\u0440\u043e\u043c\u0443 \u044f\u0434\u0440\u0443 CPU.", "trespasser edited for www.minecraft.dp.ua ..."}: null)))))))))));
    }

    private String getButtonName(String displayString)
    {
        int pos = displayString.indexOf(58);
        return pos < 0 ? displayString : displayString.substring(0, pos);
    }

    private GuiButton getSelectedButton(int i, int j)
    {
        for (int k = 0; k < this.buttonList.size(); ++k)
        {
            GuiButton btn = (GuiButton)this.buttonList.get(k);
            boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;

            if (flag)
            {
                return btn;
            }
        }

        return null;
    }
    
    public void confirmClicked(boolean flag, int i)
    {
        if (flag)
        {
            this.mc.gameSettings.resetSettings();
        }

        this.mc.displayGuiScreen(this);
    }
}
