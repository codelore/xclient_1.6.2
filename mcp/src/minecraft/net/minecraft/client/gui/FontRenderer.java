/*      */ package net.minecraft.client.gui;
/*      */ 
/*      */ import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.ResourceManager;
import net.minecraft.client.resources.ResourceManagerReloadListener;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.src.Config;
import net.minecraft.util.ChatAllowedCharacters;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.Bidi;
import java.util.*;

/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */ 
/*      */ public class FontRenderer
/*      */   implements ResourceManagerReloadListener
/*      */ {
/*   21 */   private static final ResourceLocation[] unicodePageLocations = new ResourceLocation[256];
/*      */ 
/*   25 */   private float[] charWidth = new float[256];
/*      */ 
/*   28 */   public int FONT_HEIGHT = 9;
/*   29 */   public Random fontRandom = new Random();
/*      */ 
/*   34 */   private byte[] glyphWidth = new byte[65536];
/*      */ 
/*   40 */   private int[] colorCode = new int[32];
/*      */   private ResourceLocation locationFontTexture;
/*      */   private final TextureManager renderEngine;
/*      */   private float posX;
/*      */   private float posY;
/*      */   private boolean unicodeFlag;
/*      */   private boolean bidiFlag;
/*      */   private float red;
/*      */   private float blue;
/*      */   private float green;
/*      */   private float alpha;
/*      */   private int textColor;
/*      */   private boolean randomStyle;
/*      */   private boolean boldStyle;
/*      */   private boolean italicStyle;
/*      */   private boolean underlineStyle;
/*      */   private boolean strikethroughStyle;
/*      */   public GameSettings gameSettings;
/*      */   public ResourceLocation locationFontTextureBase;
/*  100 */   public boolean enabled = true;
/*      */ 
/*      */   public FontRenderer(GameSettings par1GameSettings, ResourceLocation par2ResourceLocation, TextureManager par3TextureManager, boolean par4)
/*      */   {
/*  105 */     this.gameSettings = par1GameSettings;
/*  106 */     this.locationFontTextureBase = par2ResourceLocation;
/*      */ 
/*  108 */     this.locationFontTexture = par2ResourceLocation;
/*  109 */     this.renderEngine = par3TextureManager;
/*  110 */     this.unicodeFlag = par4;
/*      */ 
/*  112 */     this.locationFontTexture = getHdFontLocation(this.locationFontTextureBase);
/*      */ 
/*  114 */     par3TextureManager.bindTexture(this.locationFontTexture);
/*      */ 
/*  116 */     for (int var5 = 0; var5 < 32; var5++)
/*      */     {
/*  118 */       int var6 = (var5 >> 3 & 0x1) * 85;
/*  119 */       int var7 = (var5 >> 2 & 0x1) * 170 + var6;
/*  120 */       int var8 = (var5 >> 1 & 0x1) * 170 + var6;
/*  121 */       int var9 = (var5 >> 0 & 0x1) * 170 + var6;
/*      */ 
/*  123 */       if (var5 == 6)
/*      */       {
/*  125 */         var7 += 85;
/*      */       }
/*      */ 
/*  128 */       if (par1GameSettings.anaglyph)
/*      */       {
/*  130 */         int var10 = (var7 * 30 + var8 * 59 + var9 * 11) / 100;
/*  131 */         int var11 = (var7 * 30 + var8 * 70) / 100;
/*  132 */         int var12 = (var7 * 30 + var9 * 70) / 100;
/*  133 */         var7 = var10;
/*  134 */         var8 = var11;
/*  135 */         var9 = var12;
/*      */       }
/*      */ 
/*  138 */       if (var5 >= 16)
/*      */       {
/*  140 */         var7 /= 4;
/*  141 */         var8 /= 4;
/*  142 */         var9 /= 4;
/*      */       }
/*      */ 
/*  145 */       this.colorCode[var5] = ((var7 & 0xFF) << 16 | (var8 & 0xFF) << 8 | var9 & 0xFF);
/*      */     }
/*      */ 
/*  148 */     readGlyphSizes();
/*      */   }
/*      */ 
/*      */   public void onResourceManagerReload(ResourceManager par1ResourceManager)
/*      */   {
/*  154 */     this.locationFontTexture = getHdFontLocation(this.locationFontTextureBase);
/*      */ 
/*  156 */     for (int i = 0; i < unicodePageLocations.length; i++)
/*      */     {
/*  158 */       unicodePageLocations[i] = null;
/*      */     }
/*      */ 
/*  161 */     readFontTexture();
/*      */   }
/*      */ 
/*      */   private void readFontTexture()
/*      */   {
/*      */     BufferedImage bufferedimage;
/*      */     try
/*      */     {
/*  170 */       bufferedimage = ImageIO.read(Minecraft.getMinecraft().getResourceManager().getResource(this.locationFontTexture).getInputStream());
/*      */     }
/*      */     catch (IOException var17)
/*      */     {
/*  174 */       throw new RuntimeException(var17);
/*      */     }
/*      */ 
/*  177 */     int imgWidth = bufferedimage.getWidth();
/*  178 */     int imgHeight = bufferedimage.getHeight();
/*      */ 
/*  180 */     int charW = imgWidth / 16;
/*  181 */     int charH = imgHeight / 16;
/*      */ 
/*  183 */     float kx = imgWidth / 128.0F;
/*      */ 
/*  185 */     int[] ai = new int[imgWidth * imgHeight];
/*  186 */     bufferedimage.getRGB(0, 0, imgWidth, imgHeight, ai, 0, imgWidth);
/*  187 */     for (int k = 0; k < 256; k++)
/*      */     {
/*  189 */       int cx = k % 16;
/*  190 */       int cy = k / 16;
/*  191 */       int px = 0;
/*  192 */       for (px = charW - 1; px >= 0; px--)
/*      */       {
/*  194 */         int x = cx * charW + px;
/*  195 */         boolean flag = true;
/*  196 */         for (int py = 0; (py < charH) && (flag); py++)
/*      */         {
/*  198 */           int ypos = (cy * charH + py) * imgWidth;
/*  199 */           int col = ai[(x + ypos)];
/*  200 */           int al = col >> 24 & 0xFF;
/*  201 */           if (al > 16)
/*  202 */             flag = false;
/*      */         }
/*  204 */         if (!flag)
/*      */           break;
/*      */       }
/*  207 */       if (k == 65) {
/*  208 */         k = k;
/*      */       }
/*  210 */       if (k == 32)
/*      */       {
/*  212 */         if (charW <= 8)
/*  213 */           px = (int)(2.0F * kx);
/*      */         else {
/*  215 */           px = (int)(1.5F * kx);
/*      */         }
/*      */       }
/*  218 */       this.charWidth[k] = ((px + 1) / kx + 1.0F);
/*      */     }
/*      */ 
/*  221 */     readCustomCharWidths();
/*      */   }
/*      */ 
/*      */   private void readGlyphSizes()
/*      */   {
/*      */     try
/*      */     {
/*  228 */       InputStream var1 = Minecraft.getMinecraft().getResourceManager().getResource(new ResourceLocation("font/glyph_sizes.bin")).getInputStream();
/*  229 */       var1.read(this.glyphWidth);
/*      */     }
/*      */     catch (IOException var2)
/*      */     {
/*  233 */       throw new RuntimeException(var2);
/*      */     }
/*      */   }
/*      */ 
/*      */   private float renderCharAtPos(int par1, char par2, boolean par3)
/*      */   {
/*  243 */     if (par2 == ' ')
/*      */     {
/*  245 */       return this.charWidth[par2];
/*      */     }
/*      */ 
/*  248 */     if ((par1 > 0) && (!this.unicodeFlag))
/*      */     {
/*  250 */       return renderDefaultChar(par1 + 32, par3);
/*      */     }
/*      */ 
/*  254 */     return renderUnicodeChar(par2, par3);
/*      */   }
/*      */ 
/*      */   private float renderDefaultChar(int par1, boolean par2)
/*      */   {
/*  263 */     float var3 = par1 % 16 * 8;
/*  264 */     float var4 = par1 / 16 * 8;
/*  265 */     float var5 = par2 ? 1.0F : 0.0F;
/*  266 */     this.renderEngine.bindTexture(this.locationFontTexture);
/*  267 */     float var6 = this.charWidth[par1] - 0.01F;
/*  268 */     GL11.glBegin(5);
/*  269 */     GL11.glTexCoord2f(var3 / 128.0F, var4 / 128.0F);
/*  270 */     GL11.glVertex3f(this.posX + var5, this.posY, 0.0F);
/*  271 */     GL11.glTexCoord2f(var3 / 128.0F, (var4 + 7.99F) / 128.0F);
/*  272 */     GL11.glVertex3f(this.posX - var5, this.posY + 7.99F, 0.0F);
/*  273 */     GL11.glTexCoord2f((var3 + var6) / 128.0F, var4 / 128.0F);
/*  274 */     GL11.glVertex3f(this.posX + var6 + var5, this.posY, 0.0F);
/*  275 */     GL11.glTexCoord2f((var3 + var6) / 128.0F, (var4 + 7.99F) / 128.0F);
/*  276 */     GL11.glVertex3f(this.posX + var6 - var5, this.posY + 7.99F, 0.0F);
/*  277 */     GL11.glEnd();
/*  278 */     return this.charWidth[par1];
/*      */   }
/*      */ 
/*      */   private ResourceLocation getUnicodePageLocation(int par1)
/*      */   {
/*  283 */     if (unicodePageLocations[par1] == null)
/*      */     {
/*  285 */       unicodePageLocations[par1] = new ResourceLocation(String.format("textures/font/unicode_page_%02x.png", new Object[] { Integer.valueOf(par1) }));
/*      */ 
/*  287 */       unicodePageLocations[par1] = getHdFontLocation(unicodePageLocations[par1]);
/*      */     }
/*      */ 
/*  290 */     return unicodePageLocations[par1];
/*      */   }
/*      */ 
/*      */   private void loadGlyphTexture(int par1)
/*      */   {
/*  298 */     this.renderEngine.bindTexture(getUnicodePageLocation(par1));
/*      */   }
/*      */ 
/*      */   private float renderUnicodeChar(char par1, boolean par2)
/*      */   {
/*  306 */     if (this.glyphWidth[par1] == 0)
/*      */     {
/*  308 */       return 0.0F;
/*      */     }
/*      */ 
/*  312 */     int var3 = par1 / 'Ā';
/*  313 */     loadGlyphTexture(var3);
/*  314 */     int var4 = this.glyphWidth[par1] >>> 4;
/*  315 */     int var5 = this.glyphWidth[par1] & 0xF;
/*  316 */     float var6 = var4;
/*  317 */     float var7 = var5 + 1;
/*  318 */     float var8 = par1 % '\020' * 16 + var6;
/*  319 */     float var9 = (par1 & 0xFF) / '\020' * 16;
/*  320 */     float var10 = var7 - var6 - 0.02F;
/*  321 */     float var11 = par2 ? 1.0F : 0.0F;
/*  322 */     GL11.glBegin(5);
/*  323 */     GL11.glTexCoord2f(var8 / 256.0F, var9 / 256.0F);
/*  324 */     GL11.glVertex3f(this.posX + var11, this.posY, 0.0F);
/*  325 */     GL11.glTexCoord2f(var8 / 256.0F, (var9 + 15.98F) / 256.0F);
/*  326 */     GL11.glVertex3f(this.posX - var11, this.posY + 7.99F, 0.0F);
/*  327 */     GL11.glTexCoord2f((var8 + var10) / 256.0F, var9 / 256.0F);
/*  328 */     GL11.glVertex3f(this.posX + var10 / 2.0F + var11, this.posY, 0.0F);
/*  329 */     GL11.glTexCoord2f((var8 + var10) / 256.0F, (var9 + 15.98F) / 256.0F);
/*  330 */     GL11.glVertex3f(this.posX + var10 / 2.0F - var11, this.posY + 7.99F, 0.0F);
/*  331 */     GL11.glEnd();
/*  332 */     return (var7 - var6) / 2.0F + 1.0F;
/*      */   }
/*      */ 
/*      */   public int drawStringWithShadow(String par1Str, int par2, int par3, int par4)
/*      */   {
/*  341 */     return drawString(par1Str, par2, par3, par4, true);
/*      */   }
/*      */ 
/*      */   public int drawString(String par1Str, int par2, int par3, int par4)
/*      */   {
/*  350 */     if (!this.enabled) {
/*  351 */       return 0;
/*      */     }
/*  353 */     return drawString(par1Str, par2, par3, par4, false);
/*      */   }
/*      */ 
/*      */   public int drawString(String par1Str, int par2, int par3, int par4, boolean par5)
/*      */   {
/*  361 */     resetStyles();
/*      */ 
/*  363 */     if (this.bidiFlag)
/*      */     {
/*  365 */       par1Str = bidiReorder(par1Str);
/*      */     }
/*      */     int var6;
/*  370 */     if (par5)
/*      */     {
/*  372 */       var6 = renderString(par1Str, par2 + 1, par3 + 1, par4, true);
/*  373 */       var6 = Math.max(var6, renderString(par1Str, par2, par3, par4, false));
/*      */     }
/*      */     else
/*      */     {
/*  377 */       var6 = renderString(par1Str, par2, par3, par4, false);
/*      */     }
/*      */ 
/*  380 */     return var6;
/*      */   }
/*      */ 
/*      */   private String bidiReorder(String par1Str)
/*      */   {
/*  388 */     if ((par1Str != null) && (Bidi.requiresBidi(par1Str.toCharArray(), 0, par1Str.length())))
/*      */     {
/*  390 */       Bidi var2 = new Bidi(par1Str, -2);
/*  391 */       byte[] var3 = new byte[var2.getRunCount()];
/*  392 */       String[] var4 = new String[var3.length];
/*      */ 
/*  395 */       for (int var5 = 0; var5 < var3.length; var5++)
/*      */       {
/*  397 */         int var6 = var2.getRunStart(var5);
/*  398 */         int var7 = var2.getRunLimit(var5);
/*  399 */         int var8 = var2.getRunLevel(var5);
/*  400 */         String var9 = par1Str.substring(var6, var7);
/*  401 */         var3[var5] = ((byte)var8);
/*  402 */         var4[var5] = var9;
/*      */       }
/*      */ 
/*  405 */       String[] var11 = (String[])var4.clone();
/*  406 */       Bidi.reorderVisually(var3, 0, var4, 0, var3.length);
/*  407 */       StringBuilder var12 = new StringBuilder();
/*  408 */       int var7 = 0;
/*      */ 
/*  410 */       while (var7 < var4.length)
/*      */       {
/*  412 */         byte var13 = var3[var7];
/*  413 */         int var14 = 0;
/*      */ 
/*  417 */         while (var14 < var11.length)
/*      */         {
/*  419 */           if (!var11[var14].equals(var4[var7]))
/*      */           {
/*  421 */             var14++;
/*      */           }
/*      */           else
/*      */           {
/*  425 */             var13 = var3[var14++];
/*      */           }
/*      */         }
/*  428 */         if ((var13 & 0x1) == 0)
/*      */         {
/*  430 */           var12.append(var4[var7]);
/*      */         }
/*      */         else
/*      */         {
/*  434 */           for (var14 = var4[var7].length() - 1; var14 >= 0; var14--)
/*      */           {
/*  436 */             char var10 = var4[var7].charAt(var14);
/*      */ 
/*  438 */             if (var10 == '(')
/*      */             {
/*  440 */               var10 = ')';
/*      */             }
/*  442 */             else if (var10 == ')')
/*      */             {
/*  444 */               var10 = '(';
/*      */             }
/*      */ 
/*  447 */             var12.append(var10);
/*      */           }
/*      */         }
/*      */ 
/*  451 */         var7++;
/*      */       }
/*      */ 
/*  456 */       return var12.toString();
/*      */     }
/*      */ 
/*  460 */     return par1Str;
/*      */   }
/*      */ 
/*      */   private void resetStyles()
/*      */   {
/*  469 */     this.randomStyle = false;
/*  470 */     this.boldStyle = false;
/*  471 */     this.italicStyle = false;
/*  472 */     this.underlineStyle = false;
/*  473 */     this.strikethroughStyle = false;
/*      */   }
/*      */ 
/*      */   private void renderStringAtPos(String par1Str, boolean par2)
/*      */   {
/*  481 */     for (int var3 = 0; var3 < par1Str.length(); var3++)
/*      */     {
/*  483 */       char var4 = par1Str.charAt(var3);
/*      */ 
/*  487 */       if ((var4 == '§') && (var3 + 1 < par1Str.length()))
/*      */       {
/*  489 */         int var5 = "0123456789abcdefklmnor".indexOf(par1Str.toLowerCase().charAt(var3 + 1));
/*      */ 
/*  491 */         if (var5 < 16)
/*      */         {
/*  493 */           this.randomStyle = false;
/*  494 */           this.boldStyle = false;
/*  495 */           this.strikethroughStyle = false;
/*  496 */           this.underlineStyle = false;
/*  497 */           this.italicStyle = false;
/*      */ 
/*  499 */           if ((var5 < 0) || (var5 > 15))
/*      */           {
/*  501 */             var5 = 15;
/*      */           }
/*      */ 
/*  504 */           if (par2)
/*      */           {
/*  506 */             var5 += 16;
/*      */           }
/*      */ 
/*  509 */           int var6 = this.colorCode[var5];
/*  510 */           this.textColor = var6;
/*  511 */           GL11.glColor4f((var6 >> 16) / 255.0F, (var6 >> 8 & 0xFF) / 255.0F, (var6 & 0xFF) / 255.0F, this.alpha);
/*      */         }
/*  513 */         else if (var5 == 16)
/*      */         {
/*  515 */           this.randomStyle = true;
/*      */         }
/*  517 */         else if (var5 == 17)
/*      */         {
/*  519 */           this.boldStyle = true;
/*      */         }
/*  521 */         else if (var5 == 18)
/*      */         {
/*  523 */           this.strikethroughStyle = true;
/*      */         }
/*  525 */         else if (var5 == 19)
/*      */         {
/*  527 */           this.underlineStyle = true;
/*      */         }
/*  529 */         else if (var5 == 20)
/*      */         {
/*  531 */           this.italicStyle = true;
/*      */         }
/*  533 */         else if (var5 == 21)
/*      */         {
/*  535 */           this.randomStyle = false;
/*  536 */           this.boldStyle = false;
/*  537 */           this.strikethroughStyle = false;
/*  538 */           this.underlineStyle = false;
/*  539 */           this.italicStyle = false;
/*  540 */           GL11.glColor4f(this.red, this.blue, this.green, this.alpha);
/*      */         }
/*      */ 
/*  543 */         var3++;
/*      */       }
/*      */       else
/*      */       {
/*  547 */         int var5 = ChatAllowedCharacters.allowedCharacters.indexOf(var4);
/*      */ 
/*  549 */         if ((this.randomStyle) && (var5 > 0))
/*      */         {
/*      */           int var6;
/*      */           do {
/*  553 */             var6 = this.fontRandom.nextInt(ChatAllowedCharacters.allowedCharacters.length());
/*      */           }
/*  555 */           while ((int)this.charWidth[(var5 + 32)] != (int)this.charWidth[(var6 + 32)]);
/*      */ 
/*  557 */           var5 = var6;
/*      */         }
/*      */ 
/*  560 */         float var11 = this.unicodeFlag ? 0.5F : 1.0F;
/*  561 */         boolean var7 = ((var5 <= 0) || (this.unicodeFlag)) && (par2);
/*      */ 
/*  563 */         if (var7)
/*      */         {
/*  565 */           this.posX -= var11;
/*  566 */           this.posY -= var11;
/*      */         }
/*      */ 
/*  569 */         float var8 = renderCharAtPos(var5, var4, this.italicStyle);
/*      */ 
/*  571 */         if (var7)
/*      */         {
/*  573 */           this.posX += var11;
/*  574 */           this.posY += var11;
/*      */         }
/*      */ 
/*  577 */         if (this.boldStyle)
/*      */         {
/*  579 */           this.posX += var11;
/*      */ 
/*  581 */           if (var7)
/*      */           {
/*  583 */             this.posX -= var11;
/*  584 */             this.posY -= var11;
/*      */           }
/*      */ 
/*  587 */           renderCharAtPos(var5, var4, this.italicStyle);
/*  588 */           this.posX -= var11;
/*      */ 
/*  590 */           if (var7)
/*      */           {
/*  592 */             this.posX += var11;
/*  593 */             this.posY += var11;
/*      */           }
/*      */ 
/*  596 */           var8 += 1.0F;
/*      */         }
/*      */ 
/*  601 */         if (this.strikethroughStyle)
/*      */         {
/*  603 */           Tessellator var9 = Tessellator.instance;
/*  604 */           GL11.glDisable(3553);
/*  605 */           var9.startDrawingQuads();
/*  606 */           var9.addVertex(this.posX, this.posY + this.FONT_HEIGHT / 2, 0.0D);
/*  607 */           var9.addVertex(this.posX + var8, this.posY + this.FONT_HEIGHT / 2, 0.0D);
/*  608 */           var9.addVertex(this.posX + var8, this.posY + this.FONT_HEIGHT / 2 - 1.0F, 0.0D);
/*  609 */           var9.addVertex(this.posX, this.posY + this.FONT_HEIGHT / 2 - 1.0F, 0.0D);
/*  610 */           var9.draw();
/*  611 */           GL11.glEnable(3553);
/*      */         }
/*      */ 
/*  614 */         if (this.underlineStyle)
/*      */         {
/*  616 */           Tessellator var9 = Tessellator.instance;
/*  617 */           GL11.glDisable(3553);
/*  618 */           var9.startDrawingQuads();
/*  619 */           int var10 = this.underlineStyle ? -1 : 0;
/*  620 */           var9.addVertex(this.posX + var10, this.posY + this.FONT_HEIGHT, 0.0D);
/*  621 */           var9.addVertex(this.posX + var8, this.posY + this.FONT_HEIGHT, 0.0D);
/*  622 */           var9.addVertex(this.posX + var8, this.posY + this.FONT_HEIGHT - 1.0F, 0.0D);
/*  623 */           var9.addVertex(this.posX + var10, this.posY + this.FONT_HEIGHT - 1.0F, 0.0D);
/*  624 */           var9.draw();
/*  625 */           GL11.glEnable(3553);
/*      */         }
/*      */ 
/*  628 */         this.posX += var8;
/*      */       }
/*      */     }
/*      */   }
/*      */ 
/*      */   private int renderStringAligned(String par1Str, int par2, int par3, int par4, int par5, boolean par6)
/*      */   {
/*  638 */     if (this.bidiFlag)
/*      */     {
/*  640 */       par1Str = bidiReorder(par1Str);
/*  641 */       int var7 = getStringWidth(par1Str);
/*  642 */       par2 = par2 + par4 - var7;
/*      */     }
/*      */ 
/*  645 */     return renderString(par1Str, par2, par3, par5, par6);
/*      */   }
/*      */ 
/*      */   private int renderString(String par1Str, int par2, int par3, int par4, boolean par5)
/*      */   {
/*  653 */     if (par1Str == null)
/*      */     {
/*  655 */       return 0;
/*      */     }
/*      */ 
/*  659 */     if ((par4 & 0xFC000000) == 0)
/*      */     {
/*  661 */       par4 |= -16777216;
/*      */     }
/*      */ 
/*  664 */     if (par5)
/*      */     {
/*  666 */       par4 = (par4 & 0xFCFCFC) >> 2 | par4 & 0xFF000000;
/*      */     }
/*      */ 
/*  669 */     this.red = ((par4 >> 16 & 0xFF) / 255.0F);
/*  670 */     this.blue = ((par4 >> 8 & 0xFF) / 255.0F);
/*  671 */     this.green = ((par4 & 0xFF) / 255.0F);
/*  672 */     this.alpha = ((par4 >> 24 & 0xFF) / 255.0F);
/*  673 */     GL11.glColor4f(this.red, this.blue, this.green, this.alpha);
/*  674 */     this.posX = par2;
/*  675 */     this.posY = par3;
/*  676 */     renderStringAtPos(par1Str, par5);
/*  677 */     return (int)this.posX;
/*      */   }
/*      */ 
/*      */   public int getStringWidth(String par1Str)
/*      */   {
/*  686 */     if (par1Str == null)
/*      */     {
/*  688 */       return 0;
/*      */     }
/*      */ 
/*  693 */     float var2 = 0.0F;
/*  694 */     boolean var3 = false;
/*      */ 
/*  696 */     for (int var4 = 0; var4 < par1Str.length(); var4++)
/*      */     {
/*  698 */       char var5 = par1Str.charAt(var4);
/*      */ 
/*  700 */       float var6 = getCharWidthFloat(var5);
/*      */ 
/*  702 */       if ((var6 < 0.0F) && (var4 < par1Str.length() - 1))
/*      */       {
/*  704 */         var4++;
/*  705 */         var5 = par1Str.charAt(var4);
/*      */ 
/*  707 */         if ((var5 != 'l') && (var5 != 'L'))
/*      */         {
/*  709 */           if ((var5 == 'r') || (var5 == 'R'))
/*      */           {
/*  711 */             var3 = false;
/*      */           }
/*      */         }
/*      */         else
/*      */         {
/*  716 */           var3 = true;
/*      */         }
/*      */ 
/*  719 */         var6 = 0.0F;
/*      */       }
/*      */ 
/*  722 */       var2 += var6;
/*      */ 
/*  724 */       if (var3)
/*      */       {
/*  726 */         var2 += 1.0F;
/*      */       }
/*      */     }
/*      */ 
/*  730 */     return (int)var2;
/*      */   }
/*      */ 
/*      */   public int getCharWidth(char par1)
/*      */   {
/*  740 */     return Math.round(getCharWidthFloat(par1));
/*      */   }
/*      */ 
/*      */   private float getCharWidthFloat(char par1)
/*      */   {
/*  746 */     if (par1 == '§')
/*      */     {
/*  748 */       return -1.0F;
/*      */     }
/*  750 */     if (par1 == ' ')
/*      */     {
/*  753 */       return this.charWidth[32];
/*      */     }
/*      */ 
/*  757 */     int var2 = ChatAllowedCharacters.allowedCharacters.indexOf(par1);
/*      */ 
/*  759 */     if ((var2 >= 0) && (!this.unicodeFlag))
/*      */     {
/*  761 */       return this.charWidth[(var2 + 32)];
/*      */     }
/*  763 */     if (this.glyphWidth[par1] != 0)
/*      */     {
/*  765 */       int var3 = this.glyphWidth[par1] >>> 4;
/*  766 */       int var4 = this.glyphWidth[par1] & 0xF;
/*      */ 
/*  768 */       if (var4 > 7)
/*      */       {
/*  770 */         var4 = 15;
/*  771 */         var3 = 0;
/*      */       }
/*      */ 
/*  774 */       var4++;
/*  775 */       return (var4 - var3) / 2 + 1;
/*      */     }
/*      */ 
/*  779 */     return 0.0F;
/*      */   }
/*      */ 
/*      */   public String trimStringToWidth(String par1Str, int par2)
/*      */   {
/*  789 */     return trimStringToWidth(par1Str, par2, false);
/*      */   }
/*      */ 
/*      */   public String trimStringToWidth(String par1Str, int par2, boolean par3)
/*      */   {
/*  797 */     StringBuilder var4 = new StringBuilder();
/*      */ 
/*  799 */     float var5 = 0.0F;
/*  800 */     int var6 = par3 ? par1Str.length() - 1 : 0;
/*  801 */     int var7 = par3 ? -1 : 1;
/*  802 */     boolean var8 = false;
/*  803 */     boolean var9 = false;
/*      */ 
/*  805 */     for (int var10 = var6; (var10 >= 0) && (var10 < par1Str.length()) && (var5 < par2); var10 += var7)
/*      */     {
/*  807 */       char var11 = par1Str.charAt(var10);
/*      */ 
/*  809 */       float var12 = getCharWidthFloat(var11);
/*      */ 
/*  811 */       if (var8)
/*      */       {
/*  813 */         var8 = false;
/*      */ 
/*  815 */         if ((var11 != 'l') && (var11 != 'L'))
/*      */         {
/*  817 */           if ((var11 == 'r') || (var11 == 'R'))
/*      */           {
/*  819 */             var9 = false;
/*      */           }
/*      */         }
/*      */         else
/*      */         {
/*  824 */           var9 = true;
/*      */         }
/*      */       }
/*  827 */       else if (var12 < 0.0F)
/*      */       {
/*  829 */         var8 = true;
/*      */       }
/*      */       else
/*      */       {
/*  833 */         var5 += var12;
/*      */ 
/*  835 */         if (var9)
/*      */         {
/*  837 */           var5 += 1.0F;
/*      */         }
/*      */       }
/*      */ 
/*  841 */       if (var5 > par2)
/*      */       {
/*      */         break;
/*      */       }
/*      */ 
/*  846 */       if (par3)
/*      */       {
/*  848 */         var4.insert(0, var11);
/*      */       }
/*      */       else
/*      */       {
/*  852 */         var4.append(var11);
/*      */       }
/*      */     }
/*      */ 
/*  856 */     return var4.toString();
/*      */   }
/*      */ 
/*      */   private String trimStringNewline(String par1Str)
/*      */   {
/*  864 */     while ((par1Str != null) && (par1Str.endsWith("\n")))
/*      */     {
/*  866 */       par1Str = par1Str.substring(0, par1Str.length() - 1);
/*      */     }
/*      */ 
/*  869 */     return par1Str;
/*      */   }
/*      */ 
/*      */   public void drawSplitString(String par1Str, int par2, int par3, int par4, int par5)
/*      */   {
/*  877 */     resetStyles();
/*  878 */     this.textColor = par5;
/*  879 */     par1Str = trimStringNewline(par1Str);
/*  880 */     renderSplitString(par1Str, par2, par3, par4, false);
/*      */   }
/*      */ 
/*      */   private void renderSplitString(String par1Str, int par2, int par3, int par4, boolean par5)
/*      */   {
/*  889 */     List var6 = listFormattedStringToWidth(par1Str, par4);
/*      */ 
/*  891 */     for (Iterator var7 = var6.iterator(); var7.hasNext(); par3 += this.FONT_HEIGHT)
/*      */     {
/*  893 */       String var8 = (String)var7.next();
/*  894 */       renderStringAligned(var8, par2, par3, par4, this.textColor, par5);
/*      */     }
/*      */   }
/*      */ 
/*      */   public int splitStringWidth(String par1Str, int par2)
/*      */   {
/*  903 */     return this.FONT_HEIGHT * listFormattedStringToWidth(par1Str, par2).size();
/*      */   }
/*      */ 
/*      */   public void setUnicodeFlag(boolean par1)
/*      */   {
/*  912 */     this.unicodeFlag = par1;
/*      */   }
/*      */ 
/*      */   public boolean getUnicodeFlag()
/*      */   {
/*  921 */     return this.unicodeFlag;
/*      */   }
/*      */ 
/*      */   public void setBidiFlag(boolean par1)
/*      */   {
/*  929 */     this.bidiFlag = par1;
/*      */   }
/*      */ 
/*      */   public List listFormattedStringToWidth(String par1Str, int par2)
/*      */   {
/*  937 */     return Arrays.asList(wrapFormattedStringToWidth(par1Str, par2).split("\n"));
/*      */   }
/*      */ 
/*      */   String wrapFormattedStringToWidth(String par1Str, int par2)
/*      */   {
/*  945 */     int var3 = sizeStringToWidth(par1Str, par2);
/*      */ 
/*  947 */     if (par1Str.length() <= var3)
/*      */     {
/*  949 */       return par1Str;
/*      */     }
/*      */ 
/*  953 */     String var4 = par1Str.substring(0, var3);
/*  954 */     char var5 = par1Str.charAt(var3);
/*  955 */     boolean var6 = (var5 == ' ') || (var5 == '\n');
/*  956 */     String var7 = getFormatFromString(var4) + par1Str.substring(var3 + (var6 ? 1 : 0));
/*  957 */     return var4 + "\n" + wrapFormattedStringToWidth(var7, par2);
/*      */   }
/*      */ 
/*      */   private int sizeStringToWidth(String par1Str, int par2)
/*      */   {
/*  966 */     int var3 = par1Str.length();
/*      */ 
/*  968 */     float var4 = 0.0F;
/*  969 */     int var5 = 0;
/*  970 */     int var6 = -1;
/*      */ 
/*  972 */     for (boolean var7 = false; var5 < var3; var5++)
/*      */     {
/*  974 */       char var8 = par1Str.charAt(var5);
/*      */ 
/*  976 */       switch (var8)
/*      */       {
/*      */       case '\n':
/*  979 */         var5--;
/*  980 */         break;
/*      */       case '§':
/*  983 */         if (var5 < var3 - 1)
/*      */         {
/*  985 */           var5++;
/*  986 */           char var9 = par1Str.charAt(var5);
/*      */ 
/*  988 */           if ((var9 != 'l') && (var9 != 'L'))
/*      */           {
/*  990 */             if ((var9 == 'r') || (var9 == 'R') || (isFormatColor(var9)))
/*      */             {
/*  992 */               var7 = false;
/*      */             }
/*      */           }
/*      */           else
/*      */           {
/*  997 */             var7 = true;
/*      */           }
/*      */         }
/*  999 */         break;
/*      */       case ' ':
/* 1004 */         var6 = var5;
/*      */       default:
/* 1008 */         var4 += getCharWidthFloat(var8);
/*      */ 
/* 1010 */         if (var7)
/*      */         {
/* 1012 */           var4 += 1.0F;
/*      */         }
/*      */         break;
/*      */       }
/* 1016 */       if (var8 == '\n')
/*      */       {
/* 1018 */         var5++;
/* 1019 */         var6 = var5;
/*      */       }
/*      */       else
/*      */       {
/* 1023 */         if (var4 > par2)
/*      */         {
/*      */           break;
/*      */         }
/*      */       }
/*      */     }
/* 1029 */     return (var5 != var3) && (var6 != -1) && (var6 < var5) ? var6 : var5;
/*      */   }
/*      */ 
/*      */   private static boolean isFormatColor(char par0)
/*      */   {
/* 1037 */     return ((par0 >= '0') && (par0 <= '9')) || ((par0 >= 'a') && (par0 <= 'f')) || ((par0 >= 'A') && (par0 <= 'F'));
/*      */   }
/*      */ 
/*      */   private static boolean isFormatSpecial(char par0)
/*      */   {
/* 1045 */     return ((par0 >= 'k') && (par0 <= 'o')) || ((par0 >= 'K') && (par0 <= 'O')) || (par0 == 'r') || (par0 == 'R');
/*      */   }
/*      */ 
/*      */   private static String getFormatFromString(String par0Str)
/*      */   {
/* 1053 */     String var1 = "";
/* 1054 */     int var2 = -1;
/* 1055 */     int var3 = par0Str.length();
/*      */ 
/* 1057 */     while ((var2 = par0Str.indexOf('§', var2 + 1)) != -1)
/*      */     {
/* 1059 */       if (var2 < var3 - 1)
/*      */       {
/* 1061 */         char var4 = par0Str.charAt(var2 + 1);
/*      */ 
/* 1063 */         if (isFormatColor(var4))
/*      */         {
/* 1065 */           var1 = "§" + var4;
/*      */         }
/* 1067 */         else if (isFormatSpecial(var4))
/*      */         {
/* 1069 */           var1 = var1 + "§" + var4;
/*      */         }
/*      */       }
/*      */     }
/*      */ 
/* 1074 */     return var1;
/*      */   }
/*      */ 
/*      */   public boolean getBidiFlag()
/*      */   {
/* 1082 */     return this.bidiFlag;
/*      */   }
/*      */ 
/*      */   private void readCustomCharWidths()
/*      */   {
/* 1089 */     String fontFileName = this.locationFontTexture.getResourcePath();
/*      */ 
/* 1091 */     String suffix = ".png";
/* 1092 */     if (!fontFileName.endsWith(suffix))
/* 1093 */       return;
/* 1094 */     String fileName = fontFileName.substring(0, fontFileName.length() - suffix.length()) + ".properties";
/*      */     try
/*      */     {
/* 1098 */       ResourceLocation locProp = new ResourceLocation(this.locationFontTexture.getResourceDomain(), fileName);
/* 1099 */       InputStream in = Config.getResourceStream(Config.getResourceManager(), locProp);
/* 1100 */       if (in == null)
/* 1101 */         return;
/* 1102 */       Config.log("Loading " + fileName);
/* 1103 */       Properties props = new Properties();
/* 1104 */       props.load(in);
/* 1105 */       Set keySet = props.keySet();
/* 1106 */       for (Iterator iter = keySet.iterator(); iter.hasNext(); )
/*      */       {
/* 1108 */         String key = (String)iter.next();
/*      */ 
/* 1110 */         String prefix = "width.";
/* 1111 */         if (key.startsWith(prefix))
/*      */         {
/* 1113 */           String numStr = key.substring(prefix.length());
/* 1114 */           int num = Config.parseInt(numStr, -1);
/* 1115 */           if ((num >= 0) && (num < this.charWidth.length))
/*      */           {
/* 1117 */             String value = props.getProperty(key);
/* 1118 */             float width = Config.parseFloat(value, -1.0F);
/* 1119 */             if (width >= 0.0F)
/* 1120 */               this.charWidth[num] = width;
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (FileNotFoundException e)
/*      */     {
/*      */     }
/*      */     catch (IOException e)
/*      */     {
/*      */       Properties props;
/*      */       Iterator iter;
/* 1132 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */ 
/*      */   private static ResourceLocation getHdFontLocation(ResourceLocation fontLoc)
/*      */   {
/* 1139 */     if (!Config.isCustomFonts()) {
/* 1140 */       return fontLoc;
/*      */     }
/* 1142 */     if (fontLoc == null) {
/* 1143 */       return fontLoc;
/*      */     }
/* 1145 */     String fontName = fontLoc.getResourcePath();
/* 1146 */     String texturesStr = "textures/";
/* 1147 */     String mcpatcherStr = "mcpatcher/";
/* 1148 */     if (!fontName.startsWith(texturesStr)) {
/* 1149 */       return fontLoc;
/*      */     }
/* 1151 */     fontName = fontName.substring(texturesStr.length());
/* 1152 */     fontName = mcpatcherStr + fontName;
/*      */ 
/* 1154 */     ResourceLocation fontLocHD = new ResourceLocation(fontLoc.getResourceDomain(), fontName);
/* 1155 */     if (Config.hasResource(Config.getResourceManager(), fontLocHD)) {
/* 1156 */       return fontLocHD;
/*      */     }
/* 1158 */     return fontLoc;
/*      */   }
/*      */ }

/* Location:           D:\Minecraft_Job\Minecraft-adventures.ru\OptiFine.zip
 * Qualified Name:     net.minecraft.client.gui.FontRenderer
 * JD-Core Version:    0.6.2
 */