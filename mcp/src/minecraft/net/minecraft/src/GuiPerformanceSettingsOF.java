package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiPerformanceSettingsOF extends GuiScreen
{
    private GuiScreen prevScreen;
    protected String title = "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438";
    private GameSettings settings;
    private static EnumOptions[] enumOptions = new EnumOptions[] {EnumOptions.SMOOTH_FPS, EnumOptions.SMOOTH_WORLD, EnumOptions.LOAD_FAR, EnumOptions.PRELOADED_CHUNKS, EnumOptions.CHUNK_UPDATES, EnumOptions.CHUNK_UPDATES_DYNAMIC, EnumOptions.LAZY_CHUNK_LOADING};
    private int lastMouseX = 0;
    private int lastMouseY = 0;
    private long mouseStillTime = 0L;

    public GuiPerformanceSettingsOF(GuiScreen guiscreen, GameSettings gamesettings)
    {
        this.prevScreen = guiscreen;
        this.settings = gamesettings;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        StringTranslate stringtranslate = StringTranslate.getInstance();
        int i = 0;
        EnumOptions[] aenumoptions = enumOptions;
        int j = aenumoptions.length;

        for (int k = 0; k < j; ++k)
        {
            EnumOptions enumoptions = aenumoptions[k];
            int x = this.width / 2 - 155 + i % 2 * 160;
            int y = this.height / 6 + 21 * (i / 2) - 10;

            if (!enumoptions.getEnumFloat())
            {
                this.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
            }
            else
            {
                this.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
            }

            ++i;
        }

        this.buttonList.add(new GuiButton(200, this.width / 2 - 100, this.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton guibutton)
    {
        if (guibutton.enabled)
        {
            if (guibutton.id < 100 && guibutton instanceof GuiSmallButton)
            {
                this.settings.setOptionValue(((GuiSmallButton)guibutton).returnEnumOptions(), 1);
                guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
            }

            if (guibutton.id == 200)
            {
                this.mc.gameSettings.saveOptions();
                this.mc.displayGuiScreen(this.prevScreen);
            }

            if (guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal())
            {
                ScaledResolution scaledresolution = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
                int i = scaledresolution.getScaledWidth();
                int j = scaledresolution.getScaledHeight();
                this.setWorldAndResolution(this.mc, i, j);
            }
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int x, int y, float f)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, this.title, this.width / 2, 20, 16777215);
        super.drawScreen(x, y, f);

        if (Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5)
        {
            short activateDelay = 700;

            if (System.currentTimeMillis() >= this.mouseStillTime + (long)activateDelay)
            {
                int x1 = this.width / 2 - 150;
                int y1 = this.height / 6 - 5;

                if (y <= y1 + 98)
                {
                    y1 += 105;
                }

                int x2 = x1 + 150 + 150;
                int y2 = y1 + 84 + 10;
                GuiButton btn = this.getSelectedButton(x, y);

                if (btn != null)
                {
                    String s = this.getButtonName(btn.displayString);
                    String[] lines = this.getTooltipLines(s);

                    if (lines == null)
                    {
                        return;
                    }

                    this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

                    for (int i = 0; i < lines.length; ++i)
                    {
                        String line = lines[i];
                        this.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, 14540253);
                    }
                }
            }
        }
        else
        {
            this.lastMouseX = x;
            this.lastMouseY = y;
            this.mouseStillTime = System.currentTimeMillis();
        }
    }

    private String[] getTooltipLines(String btnName)
    {
        return btnName.equals("\u0421\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u0435 FPS") ? new String[] {"\u0421\u0442\u0430\u0431\u0438\u043b. FPS \u0441 \u043f\u043e\u043c\u043e\u0449\u044c\u044e \u043e\u0447\u0438\u0441\u0442\u043a\u0438 \u0431\u0443\u0444\u0435\u0440\u0430 \u0432\u0438\u0434\u0435\u043e\u0434\u0440\u0430\u0439\u0432\u0435\u0440\u0430.", "  \u0412\u042b\u041a\u041b - \u0431\u0435\u0437 \u0441\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0430\u0446\u0438\u0438, FPS \u043d\u0435\u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439", "  \u0412\u041a\u041b - FPS \u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439", "\u042d\u0442\u043e \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 \u0437\u0430\u0432\u0438\u0441\u0438\u0442 \u043e\u0442 \u0432\u0438\u0434\u0435\u043e\u0434\u0440\u0430\u0439\u0432\u0435\u0440\u0430 \u0438", "\u044d\u0444\u0444\u0435\u043a\u0442 \u043d\u0435 \u0432\u0441\u0435\u0433\u0434\u0430 \u0432\u0438\u0434\u0435\u043d."}: (btnName.equals("\u0421\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u043d\u0438\u0435 \u041c\u0438\u0440\u0430") ? new String[] {"\u0423\u0431\u0438\u0440\u0430\u0435\u0442 \u0437\u0430\u0432\u0438\u0441\u0430\u043d\u0438\u044f \u0438 \u0441\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0438\u0440\u0443\u0435\u0442 \u0438\u0433\u0440\u0443.", "  \u0412\u042b\u041a\u041b - \u0431\u0435\u0437 \u0441\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0430\u0446\u0438\u0438, FPS \u043d\u0435\u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439", "  \u0412\u041a\u041b - FPS \u0441\u0442\u0430\u0431\u0438\u043b\u044c\u043d\u044b\u0439", "\u0421\u0442\u0430\u0431\u0438\u043b\u0438\u0437\u0438\u0440\u0443\u0435\u0442 FPS, \u0440\u0430\u0441\u043f\u0440\u0435\u0434\u0435\u043b\u044f\u044f \u043d\u0430\u0433\u0440\u0443\u0437\u043a\u0443.", "\u0422\u043e\u043b\u044c\u043a\u043e \u0434\u043b\u044f \u043b\u043e\u043a\u0430\u043b\u044c\u043d\u044b\u0445 \u043c\u0438\u0440\u043e\u0432 \u0438 \u043e\u0434\u043d\u043e\u044f\u0434\u0435\u0440\u043d\u044b\u0445 CPU."}: (btnName.equals("\u0414\u0438\u0441\u0442\u0430\u043d\u0446\u0438\u044f \u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0438") ? new String[] {"\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0447\u0430\u043d\u043a\u043e\u0432 \u043d\u0430 \u0440\u0430\u0441\u0441\u0442\u043e\u044f\u043d\u0438\u0438 \u0414\u0430\u043b\u0435\u043a\u043e.", "\u041f\u0435\u0440\u0435\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u0435 \u0434\u0438\u0441\u0442\u0430\u043d\u0446\u0438\u0438 \u0431\u0435\u0437 \u0442\u0440\u0435\u0431\u043e\u0432\u0430\u043d\u0438\u044f \u043d\u0430 \u043f\u043e\u0432\u0442\u043e\u0440\u043d\u0443\u044e", "\u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0443 \u0447\u0430\u043d\u043a\u043e\u0432 (\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0441 \'\u0411\u043b\u0438\u0437\u043a\u043e\' \u043d\u0430 \'\u0414\u0430\u043b\u0435\u043a\u043e\')", "  \u0412bI\u041a\u041b - \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0447\u0430\u043d\u043a\u043e\u0432.", "  \u0412\u041a\u041b - \u043f\u043e\u0437\u0432\u043e\u043b\u044f\u0435\u0442 \u043f\u0440\u043e\u0433\u0440\u0443\u0436\u0430\u0442\u044c \u0447\u0430\u043d\u043a\u0438 \u043d\u0430 \u0440\u0430\u0441\u0442\u043e\u044f\u043d\u0438\u0438.", " "}: (btnName.equals("\u041f\u0440\u0435\u0434\u0437\u0430\u0433\u0440. \u0427\u0430\u043d\u043a\u043e\u0432") ? new String[] {"\u041e\u043f\u0440\u0435\u0434\u0435\u043b\u044f\u0435\u0442 \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0432 \u043a\u043e\u0442\u043e\u0440\u043e\u0439 \u0447\u0430\u043d\u043a\u0438 \u043d\u0435 \u0431\u0443\u0434\u0443\u0442 \u0437\u0430\u0433\u0440\u0443\u0436\u0435\u043d\u044b.", "  \u0412bI\u041a\u041b - \u043d\u043e\u0432\u044b\u0435 \u0447\u0430\u043d\u043a\u0438 \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u044e\u0442\u0441\u044f \u043f\u043e\u0441\u043b\u0435 5 \u043c.", "  2 - \u043d\u043e\u0432\u044b\u0435 \u0447\u0430\u043d\u043a\u0438 \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u044e\u0442\u0441\u044f \u043f\u043e\u0441\u043b\u0435 32 \u043c.", "  8 - \u043d\u043e\u0432\u044b\u0435 \u0447\u0430\u043d\u043a\u0438 \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u044e\u0442\u0441\u044f \u043f\u043e\u0441\u043b\u0435 128 \u043c.", "\u0414\u043b\u044f \u043c\u0430\u043a\u0441. \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u043d\u0443\u0436\u043d\u043e \u0431\u043e\u043b\u044c\u0448\u0435 \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u0434\u043b\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438."}: (btnName.equals("\u041e\u0431\u043d\u043e\u0432\u043b. \u0427\u0430\u043d\u043a\u043e\u0432 \u0437\u0430 \u041a\u0430\u0434\u0440") ? new String[] {"\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u0447\u0430\u043d\u043a\u043e\u0432 \u0437\u0430 1 \u043a\u0430\u0434\u0440", "  1 - \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u043c\u0438\u0440\u0430, \u0432\u044b\u0441\u043e\u043a\u0438\u0439 FPS", "  3 - \u0431\u044b\u0441\u0442\u0440\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u043c\u0438\u0440\u0430, \u0441\u0440\u0435\u0434\u043d\u0438\u0439 FPS", "  5 - \u043e\u0447\u0435\u043d\u044c \u0431\u044b\u0441\u0442\u0440\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u043c\u0438\u0440\u0430, \u043d\u0438\u0437\u043a\u0438\u0439 FPS"}: (btnName.equals("\u0414\u0438\u043d\u0430\u043c\u0438\u0447. \u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435") ? new String[] {"\u0414\u0438\u043d\u0430\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u0447\u0430\u043d\u043a\u043e\u0432.", "  \u0412bI\u041a\u041b - \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u043e\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u0447\u0430\u043d\u043a\u043e\u0432 \u0437\u0430 \u043a\u0430\u0434\u0440", "  \u0412\u041a\u041b - \u0431\u043e\u043b\u044c\u0448\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0439, \u043a\u043e\u0433\u0434\u0430 \u0438\u0433\u0440\u043e\u043a \u0441\u0442\u043e\u0438\u0442 \u043d\u0430 \u043c\u0435\u0441\u0442\u0435", "\u0414\u0438\u043d\u0430\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u0437\u0430\u0441\u0442\u0430\u0432\u0438\u0442 \u0447\u0430\u043d\u043a\u0438 \u043e\u0431\u043d\u043e\u0432\u043b\u044f\u0442\u044c\u0441\u044f", "\u0447\u0430\u0449\u0435 \u0434\u043b\u044f \u0431\u043e\u043b\u0435\u0435 \u0431\u044b\u0441\u0442\u0440\u043e\u0439 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438 \u043c\u0438\u0440\u0430"}: (btnName.equals("\u041e\u0442\u043b\u043e\u0436\u0435\u043d\u043d\u0430\u044f \u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430") ? new String[] {"\u041e\u0442\u043b\u043e\u0436\u0435\u043d\u043d\u0430\u044f \u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430", "  \u0412\u042b\u041a\u041b - \u0441\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0447\u0430\u043d\u043a\u043e\u0432 \u0441 \u0441\u0435\u0440\u0432\u0435\u0440\u0430.", "  \u0412\u041a\u041b - \u043e\u0442\u043b\u043e\u0436\u0435\u043d\u043d\u0430\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0447\u0430\u043d\u043a\u043e\u0432 \u0441 \u0441\u0435\u0440\u0432\u0435\u0440\u0430.", "\u0421\u0433\u043b\u0430\u0436\u0438\u0432\u0430\u0435\u0442 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0443 \u0447\u0430\u043d\u043a\u043e\u0432 \u0441 \u0441\u0435\u0440\u0432\u0435\u0440\u0430,", "\u0440\u0430\u0437\u0434\u0435\u043b\u044f\u044f \u0447\u0430\u043d\u043a\u0438 \u043d\u0430 \u043d\u0435\u0441\u043a\u043e\u043b\u044c\u043a\u043e \u0447\u0430\u0441\u0442\u0435\u0439.", "\u0412\u044b\u043a\u043b\u044e\u0447\u0438\u0442\u0435 \u0435\u0441\u043b\u0438 \u0447\u0430\u043d\u043a\u0438 \u043f\u043e\u0434\u0433\u0440\u0443\u0436\u0430\u044e\u0442\u0441\u044f \u043d\u0435\u043f\u0440\u0430\u0432\u0438\u043b\u044c\u043d\u043e.", "\u0422\u043e\u043b\u044c\u043a\u043e \u0434\u043b\u044f \u043b\u043e\u043a\u0430\u043b\u044c\u043d\u044b\u0445 \u043c\u0438\u0440\u043e\u0432 \u0438 \u043e\u0434\u043d\u043e\u044f\u0434\u0435\u0440\u043d\u044b\u0445 CPU."}: null))))));
    }

    private String getButtonName(String displayString)
    {
        int pos = displayString.indexOf(58);
        return pos < 0 ? displayString : displayString.substring(0, pos);
    }

    private GuiButton getSelectedButton(int i, int j)
    {
        for (int k = 0; k < this.buttonList.size(); ++k)
        {
            GuiButton btn = (GuiButton)this.buttonList.get(k);
            boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;

            if (flag)
            {
                return btn;
            }
        }

        return null;
    }
}
