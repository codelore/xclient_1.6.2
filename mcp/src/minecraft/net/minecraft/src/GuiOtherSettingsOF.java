package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiOtherSettingsOF extends GuiScreen
{
    private GuiScreen prevScreen;
    protected String title = "\u041f\u0440\u043e\u0447\u0438\u0435 \u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438";
    private GameSettings settings;
    private static EnumOptions[] enumOptions = new EnumOptions[] {EnumOptions.LAGOMETER, EnumOptions.PROFILER, EnumOptions.WEATHER, EnumOptions.TIME, EnumOptions.USE_FULLSCREEN, EnumOptions.FULLSCREEN_MODE, EnumOptions.AUTOSAVE_TICKS};
    private int lastMouseX = 0;
    private int lastMouseY = 0;
    private long mouseStillTime = 0L;

    public GuiOtherSettingsOF(GuiScreen guiscreen, GameSettings gamesettings)
    {
        this.prevScreen = guiscreen;
        this.settings = gamesettings;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        StringTranslate stringtranslate = StringTranslate.getInstance();
        int i = 0;
        EnumOptions[] aenumoptions = enumOptions;
        int j = aenumoptions.length;

        for (int k = 0; k < j; ++k)
        {
            EnumOptions enumoptions = aenumoptions[k];
            int x = this.width / 2 - 155 + i % 2 * 160;
            int y = this.height / 6 + 21 * (i / 2) - 10;

            if (!enumoptions.getEnumFloat())
            {
                this.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
            }
            else
            {
                this.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
            }
            ++i;
        }
        
        this.buttonList.add(new GuiButton(200, this.width / 2 - 100, this.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton guibutton)
    {
        if (guibutton.enabled)
        {
            if (guibutton.id < 100 && guibutton instanceof GuiSmallButton)
            {
                this.settings.setOptionValue(((GuiSmallButton)guibutton).returnEnumOptions(), 1);
                guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
            }

            if (guibutton.id == 200)
            {
                this.mc.gameSettings.saveOptions();
                this.mc.displayGuiScreen(this.prevScreen);
            }

            

            if (guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal())
            {
                ScaledResolution scaledresolution1 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
                int i = scaledresolution1.getScaledWidth();
                int j = scaledresolution1.getScaledHeight();
                this.setWorldAndResolution(this.mc, i, j);
            }
            
            
        }
    }

    public void confirmClicked(boolean flag, int i)
    {
        if (flag)
        {
            this.mc.gameSettings.resetSettings();
        }

        this.mc.displayGuiScreen(this);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int x, int y, float f)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, this.title, this.width / 2, 20, 16777215);
        super.drawScreen(x, y, f);

        if (Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5)
        {
            short activateDelay = 700;

            if (System.currentTimeMillis() >= this.mouseStillTime + (long)activateDelay)
            {
                int x1 = this.width / 2 - 150;
                int y1 = this.height / 6 - 5;

                if (y <= y1 + 98)
                {
                    y1 += 105;
                }

                int x2 = x1 + 150 + 150;
                int y2 = y1 + 84 + 10;
                GuiButton btn = this.getSelectedButton(x, y);

                if (btn != null)
                {
                    String s = this.getButtonName(btn.displayString);
                    String[] lines = this.getTooltipLines(s);

                    if (lines == null)
                    {
                        return;
                    }

                    this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

                    for (int i = 0; i < lines.length; ++i)
                    {
                        String line = lines[i];
                        this.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, 14540253);
                    }
                }
            }
        }
        else
        {
            this.lastMouseX = x;
            this.lastMouseY = y;
            this.mouseStillTime = System.currentTimeMillis();
        }
    }

    private String[] getTooltipLines(String btnName)
    {
        return btnName.equals("\u0410\u0432\u0442\u043e\u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435") ? new String[] {"\u0418\u043d\u0442\u0435\u0440\u0432\u0430\u043b \u0430\u0432\u0442\u043e\u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u044f", "\u0421\u0442\u0430\u043d\u0434. \u0438\u043d\u0442\u0435\u0440\u0432\u0430\u043b \u0430\u0432\u0442\u043e\u0441\u043e\u0445\u0440. (2\u0441\u0435\u043a.) \u041d\u0415 \u0420\u0415\u041a\u041e\u041c\u0415\u041d\u0414\u0423\u0415\u0422\u0421\u042f.", "\u0410\u0432\u0442\u043e\u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u0432\u044b\u043b\u0435\u0442\u044b \u0438\u0437 \u0438\u0433\u0440\u044b."}: (btnName.equals("\u041e\u0442\u043b\u0430\u0434\u043e\u0447. \u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f") ? new String[] {"\u041e\u0442\u043b\u0430\u0434\u043e\u0447. \u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f", "  \u0412\u042b\u041a\u041b - \u043d\u0435\u0442 \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u0433\u043e \u0438\u043d\u0444\u043e, (\u0431\u044b\u0441\u0442\u0440\u043e)", "  \u0412\u041a\u041b - \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u044b\u0439 \u043c\u043e\u043d\u0438\u0442\u043e\u0440 \u0441 \u0433\u0440\u0430\u0444\u0438\u043a\u043e\u043c, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044e \u043d\u0430 \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u043c \u043c\u043e\u043d\u0438\u0442\u043e\u0440\u0435 (F3).", "* \u0411\u0435\u043b\u044b\u0439 - \u0442\u0430\u043a\u0442", "* \u041a\u0440\u0430\u0441\u043d\u044b\u0439 - \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0447\u0430\u043d\u043a\u043e\u0432", "* \u0417\u0435\u043b\u0435\u043d\u044b\u0439 - \u0440\u0435\u043d\u0434\u0435\u0440\u0438\u043d\u0433 \u043a\u0430\u0434\u0440\u0430 + \u0432\u043d\u0443\u0442\u0440\u0435\u043d\u043d\u0438\u0439 \u0441\u0435\u0440\u0432\u0435\u0440"}: (btnName.equals("\u041e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u044b\u0439 \u041f\u0440\u043e\u0444\u0438\u043b\u044c") ? new String[] {"\u041e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u044b\u0439 \u041f\u0440\u043e\u0444\u0438\u043b\u044c", "  \u0412\u041a\u041b - \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0430\u043a\u0442\u0438\u0432\u043d\u043e, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "  \u0412\u042b\u041a\u041b - \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u043d\u0435 \u0430\u043a\u0442\u0438\u0432\u043d\u043e, (\u0431\u044b\u0441\u0442\u0440\u043e)", "\u041e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0441\u043e\u0431\u0438\u0440\u0430\u0435\u0442 \u0438 \u043f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u0442", "\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044e \u0432 \u043e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u043e\u043c \u043c\u043e\u043d\u0438\u0442\u043e\u0440\u0435 (F3)"}: (btnName.equals("\u0412\u0440\u0435\u043c\u044f") ? new String[] {"\u0412\u0440\u0435\u043c\u044f", "  \u0421\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u043e - \u043d\u043e\u0440\u043c\u0430\u043b\u044c\u043d\u044b\u0439 \u0446\u0438\u043a\u043b \u0434\u043d\u044f \u0438 \u043d\u043e\u0447\u0438", "  \u0422\u043e\u043b\u044c\u043a\u043e \u0414\u0435\u043d\u044c - \u0432\u0441\u0435\u0433\u0434\u0430 \u0442\u043e\u043b\u044c\u043a\u043e \u0434\u0435\u043d\u044c", "  \u0422\u043e\u043b\u044c\u043a\u043e \u041d\u043e\u0447\u044c - \u0432\u0441\u0435\u0433\u0434\u0430 \u0431\u0443\u0434\u0435\u0442 \u043d\u043e\u0447\u044c", "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u0440\u0430\u0431\u043e\u0442\u0430\u044e\u0442 \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0440\u0435\u0436\u0438\u043c\u0435 \u0422\u0412\u041e\u0420\u0427\u0415\u0421\u0422\u0412\u041e"}: (btnName.equals("\u041f\u043e\u0433\u043e\u0434\u0430") ? new String[] {"\u041f\u043e\u0433\u043e\u0434\u0430", "  \u0412\u041a\u041b - \u043f\u043e\u0433\u043e\u0434\u0430 \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u0430, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "  \u0412bI\u041a\u041b - \u043f\u043e\u0433\u043e\u0434\u0430 \u0432\u044b\u043a\u043b\u044e\u0447\u0435\u043d\u0430, (\u0431\u044b\u0441\u0442\u0440\u043e)", "\u041a\u043e\u043d\u0442\u0440\u043e\u043b\u044c \u043f\u043e\u0433\u043e\u0434\u044b: \u0434\u043e\u0436\u0434\u044f, \u0441\u043d\u0435\u0433\u0430 \u0438 \u043c\u043e\u043b\u043d\u0438\u0439."}: (btnName.equals("\u042d\u043a\u0440\u0430\u043d") ? new String[] {"\u0420\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u0435 \u044d\u043a\u0440\u0430\u043d\u0430", "  \u0421\u0442\u0430\u043d\u0434\u0430\u0440\u0442 - \u0438\u0441\u043f\u043e\u043b\u044c\u0437. \u0440\u0430\u0437\u0440\u0435\u0448. \u044d\u043a\u0440\u0430\u043d\u0430 \u0440\u0430\u0431. \u0441\u0442\u043e\u043b\u0430, (\u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e)", "  \u0428x\u0412 - \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0441\u0432\u043e\u0435 \u0440\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u0435 \u044d\u043a\u0440\u0430\u043d\u0430, (\u0431\u044b\u0441\u0442\u0440\u0435\u0435)", "\u0412\u044b\u0431\u0440\u0430\u043d\u043d\u043e\u0435 \u0440\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u0435 \u0438\u0441\u043f. \u0432 \u043f\u043e\u043b\u043d\u043e\u044d\u043a\u0440\u0430\u043d\u043d\u043e\u043c \u0440\u0435\u0436\u0438\u043c\u0435 (F11)."}: null)))));
    }

    private String getButtonName(String displayString)
    {
        int pos = displayString.indexOf(58);
        return pos < 0 ? displayString : displayString.substring(0, pos);
    }

    private GuiButton getSelectedButton(int i, int j)
    {
        for (int k = 0; k < this.buttonList.size(); ++k)
        {
            GuiButton btn = (GuiButton)this.buttonList.get(k);
            boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;

            if (flag)
            {
                return btn;
            }
        }

        return null;
    }
}
