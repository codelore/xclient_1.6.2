package net.minecraft.src;

import net.minecraft.client.resources.ResourceManager;
import net.minecraft.client.resources.ResourceManagerReloadListener;

final class TextureUtils$1 implements ResourceManagerReloadListener
{
    public void onResourceManagerReload(ResourceManager var1)
    {
        TextureUtils.resourcesReloaded(var1);
    }
}
