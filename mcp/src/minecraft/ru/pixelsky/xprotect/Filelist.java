package ru.pixelsky.xprotect;

import java.io.*;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Класс, описывающий набор пар (файл, хэш)
 * Используется для проверки целостности файлов.
 */
public final class Filelist {
    private List<FileEntry> fileEntries = new ArrayList<FileEntry>();

    /**
     * Проверяет, является ли другой список файлов корректным.
     *
     * Корректный = хэши всех его файлов совпадают с хэшами файлов этого списка.
     * Если в проверяемом списке есть лишние файлы, то проверяемый список не считается корректным.
     *
     * @return корректность списка
     */
    public boolean check(Filelist another)
    {
        for (FileEntry entry : another.fileEntries)
        {
            if (!fileEntries.contains(entry)) {
                // Либо лишний, либо несоотвествие хэша
                return false;
            }
        }
        // Все файлы хорошие
        return true;
    }

    /**
     * Возвращает список относительных путей файлов из проверяемого листа, которые надо удалить/изменить, или же добавить, чтобы список стал равен этому
     * Функция должна использоваться при проверке лаунчером, так как находить отсутствующие файлы надо только ему.
     *
     * @return изменяемый список файлов переденного листа, не соответствующих этому листу
     */
    public List<String> getCorrupted(Filelist filelist) {
        List<String> corrupted = new ArrayList<String>();
        // Смотрим все файлы проверяемого списка
        for (FileEntry file : filelist.fileEntries) {
            if (!fileEntries.contains(file)) {
                // Неизвестный или поврежденный файл
                corrupted.add(file.fileName);
            }
        }
        // Смотрим все файлы этого списка
        for (FileEntry file : fileEntries) {
            if (!filelist.fileEntries.contains(file)) {
                // Либо файл не такой, либо его просто нет
                if (!corrupted.contains(file.fileName))
                    corrupted.add(file.fileName);
            }
        }
        return corrupted;
    }

    /**
     * @return список, закодированный в байты, читается с помощью fromBytes
     */
    public byte[] toBytes() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(out);
            dos.writeInt(fileEntries.size());
            for (FileEntry entry : fileEntries) {
                dos.writeUTF(entry.fileName);
                // Все хэши одной харанее известной длины
                dos.write(entry.fileHash);
            }
            return out.toByteArray();
        } catch (IOException ex) {
            // Не должно никогда выполнятся
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Восстанавливает список, закодированный toBytes
     * @param bytes закодированный список
     * @return раскодированный список, null при ошибке
     */
    public static Filelist fromBytes(byte[] bytes) {
        try {
            Filelist filelist = new Filelist();
            ByteArrayInputStream in = new ByteArrayInputStream(bytes);
            DataInputStream dis = new DataInputStream(in);
            int count = dis.readInt();
            for (int i = 0; i < count; i++) {
                byte[] hash = new byte[16];
                String filename = dis.readUTF();
                int hashLen = dis.read(hash);
                if (hashLen == 16) {
                    FileEntry entry = new FileEntry();
                    entry.fileName = filename;
                    entry.fileHash = hash;
                    filelist.fileEntries.add(entry);
                } else {
                    // Что-то не так
                    return null;
                }
            }
            return filelist;
        } catch (IOException ex) {
            // Не должно возникать при соблюдении протокола
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Создает список, соответствующий файлам в файловой системе
     * @param root корень (обычно .minecraft)
     * @param checkedDirs названия проверяемых папок (bin, mods, coremods) , подпапки не проверяются
     * @return список файлов, null при ошибке
     */
    public static Filelist fromFilesystem(File root, String[] checkedDirs) {
        Filelist list = new Filelist();
        for (String checkedDir : checkedDirs) {
            File dir = new File(root, checkedDir);
            File[] files = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.isFile()) {
                        FileEntry entry = new FileEntry();

                        entry.fileName = checkedDir + "/" + f.getName();
                        entry.fileHash = md5(f);
                        list.fileEntries.add(entry);
                    }
                }
            }
        }
        return list;
    }

    /**
     * Возвращает хэш файла в этом списке
     * @param filename относительный путь к файлу
     * @return хэш файла или null, если файла в списке нет
     */
    public byte[] getCorrectHash(String filename) {
        for (FileEntry entry : fileEntries) {
            if(entry.fileName.equals(filename))
                return Arrays.copyOf(entry.fileHash, 16);
        }
        return null;
    }

    /**
     * Считает md5 файла
     * @param f файл
     * @return md5 файла, в случае ошибки null
     */
    private static byte[] md5(File f) {
        InputStream inputStream = null;
        try  {
            inputStream = new FileInputStream(f);
            MessageDigest digest = MessageDigest.getInstance("MD5");
            DigestInputStream dis = new DigestInputStream(inputStream, digest);
            byte[] buf = new byte[1024 * 1024];
            while (dis.read(buf) >= 0) {
                // Читаем файл
            }
            byte[] dig = digest.digest();
            if (dig.length == 16) {
                return dig;
            } else {
                // Что-то тут не так, вернем null
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    private static class FileEntry {
        // Пусть относительно .minecraft, в качестве разделителя пути используется '/'
        private String fileName;
        // Хэш этого файла, 16 байт
        private byte[] fileHash;

        // Используется для поиска в коллекциях
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            FileEntry fileEntry = (FileEntry) o;

            return Arrays.equals(fileHash, fileEntry.fileHash) && fileName.equals(fileEntry.fileName);
        }

        // Используется для поиска в коллекциях
        @Override
        public int hashCode() {
            int result = fileName.hashCode();
            result = 31 * result + Arrays.hashCode(fileHash);
            return result;
        }
    }
}