package ru.pixelsky.colorme;

import net.minecraft.item.ItemStack;

public class Colorizer {
    public static String getDamageString(ItemStack is) {
        if (is == null || is.getMaxDamage() <= 0)
            return "§aНеразрушимый";

        int percent = ((is.getMaxDamage() - is.getItemDamage()) * 100 / is.getMaxDamage());

        String damageString = (is.getMaxDamage() - is.getItemDamage()) + "§r/" + "§2" + (is.getMaxDamage()) + "§r";

        if (percent <= 25)
        {
            return "§4" + damageString;
        }
        else if (percent <= 50)
        {
            return "§6" + damageString;
        }
        else
        {
            return "§2" + damageString;
        }
    }

    public static String getDamageTooltip(ItemStack itemStack) {
        return "Прочность: " + getDamageString(itemStack);
    }

    public static String getXLightTooltip(ItemStack itemStack, int level) {
        return "Эффект свечения: " + level;
    }
}
