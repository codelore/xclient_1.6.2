package ru.pixelsky.xmod.blocks.resource;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Used for coal storing
 */
public class BlockCoal extends Block {

    public BlockCoal(int par1) 
    {
        super(par1, Material.ground);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    @Override
    public void registerIcons(IconRegister par1IconRegister) 
    {
        this.blockIcon = par1IconRegister.registerIcon("xmod:resource/coalblock");
    }
}

