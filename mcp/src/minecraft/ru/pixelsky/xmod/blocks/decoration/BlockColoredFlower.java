package ru.pixelsky.xmod.blocks.decoration;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;
import ru.pixelsky.xmod.lang.NamesRegistry;

import java.util.List;
import java.util.Random;

/**
 * Self-breeding flowers
 */
public class BlockColoredFlower extends BlockFlower {
    protected Icon[] flowerIcons = new Icon[4];

    public BlockColoredFlower(int id) {
        super(id, Material.plants);
        this.setCreativeTab(XMod.xModTab);
        this.setTickRandomly(true);
        setUnlocalizedName("coloredflower");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public Icon getIcon(int side, int metadata) {
        return flowerIcons[metadata];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister iconRegister) {
        flowerIcons[0] = iconRegister.registerIcon("xmod:flowers/camomile");
        flowerIcons[1] = iconRegister.registerIcon("xmod:flowers/cornflower");
        flowerIcons[2] = iconRegister.registerIcon("xmod:flowers/lavender");
        flowerIcons[3] = iconRegister.registerIcon("xmod:flowers/tulip");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(int id, CreativeTabs tabs, List list) {
        for (int i = 0; i < 4; i++)
            list.add(new ItemStack(id, 1, i));
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random)
    {
        if (world.isRemote)
            return;
        int metadata = world.getBlockMetadata(x, y, z);
        byte byte0 = 4;
        int l = 5;
        for(int i1 = x - byte0; i1 <= x + byte0; i1++)
        {
            for(int k1 = z - byte0; k1 <= z + byte0; k1++)
            {
                for(int i2 = y - 1; i2 <= y + 1; i2++)
                {
                    if(world.getBlockId(i1, i2, k1) == blockID && --l <= 0)
                    {
                        return;
                    }
                }
            }
        }
        int j1 = (x + random.nextInt(3)) - 1;
        int l1 = (y + random.nextInt(2)) - random.nextInt(2);
        int j2 = (z + random.nextInt(3)) - 1;
        for(int k2 = 0; k2 < 4; k2++)
        {
            if(world.isAirBlock(j1, l1, j2) && canBlockStay(world, j1, l1, j2))
            {
                x = j1;
                y = l1;
                z = j2;
            }
            j1 = (x + random.nextInt(3)) - 1;
            l1 = (y + random.nextInt(2)) - random.nextInt(2);
            j2 = (z + random.nextInt(3)) - 1;
        }
        if(world.isAirBlock(j1, l1, j2) && canBlockStay(world, j1, l1, j2))
        {
            if(random.nextInt(5) == 0) {
                // 3 means: 1 | 2. 1 means block update, 2 means send update to client
                world.setBlock(j1, l1, j2, blockID, metadata, 3);
            }
        }
        super.updateTick(world, x, y, z, random);
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }
}