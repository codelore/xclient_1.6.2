package ru.pixelsky.xmod.creative;

import net.minecraft.creativetab.CreativeTabs;
import ru.pixelsky.xmod.core.XMod;

public class XModCreativeTab extends CreativeTabs {

    public XModCreativeTab(String label) {
        super(label);
    }

    @Override
    public int getTabIconItemIndex() {
        return XMod.blockStoolSingle.blockID;
    }

    @Override
    public String getTranslatedTabLabel() {
        return "xMod";
    }
}
