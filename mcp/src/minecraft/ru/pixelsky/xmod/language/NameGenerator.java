/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.language;

import java.io.FileNotFoundException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author Smile
 */
public class NameGenerator
{   
    public static String generateName(String s) throws FileNotFoundException, ScriptException, NoSuchMethodException
    {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        Invocable inv = (Invocable) engine;      
        String message = null;
        
        engine.eval(new java.io.FileReader("/mods/xmod/launguage/dict.js"));
        engine.eval(new java.io.FileReader("/mods/xmod/launguage/main.js"));
        message = inv.invokeFunction("generate", s).toString();
       
        return message;
    }
    
}

