/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.tick;

/**
 *
 * @author Smile
 */
import java.util.ArrayList;
import java.util.List;

public class ScreenMessageHandler 
{
    private static List<ScreenMessageHandler> msgList = new ArrayList<ScreenMessageHandler>();
	
    private String msg;
    private int maxTime;
    private int time;
    private float alpha;
    private int color;
	
    public ScreenMessageHandler(String str, int time)
    {
	this.msg = str;
	this.maxTime = time;
	this.time = time;
	this.alpha = 1.0F;
        this.color = 0xffffff;
    }
    
    public ScreenMessageHandler(String str, int time, int color)
    {
        this.msg = str;
        this.maxTime = time;
        this.time = time;
        this.alpha = 1.0F;
        this.color = color;
    }
	
    public void onUpdate()
    {
	time--;
	while(this.time < maxTime - 100 && this.alpha > 0)
        {
            this.alpha -= 0.1F;
	}
    }
	
    public boolean shouldShow()
    {
	return time > 0;
    }
	
    public String getMessage()
    {
	return msg;
    }
	
    public float getAlpha()
    {
	return alpha;
    }
    
    public int getColor()
    {
        return color;
    }
	
    public static void addLowMessage(String str, int time)
    {
	msgList.add(new ScreenMessageHandler(str, time));
    }
    
    public static void addHightMessage(String str, int time, int color)
    {
        msgList.add(new ScreenMessageHandler(str, time, color));
    }

    public static List<ScreenMessageHandler> getMsgList()
    {
	return msgList;
    }
}