/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.network;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import ru.pixelsky.xmod.core.ClientProxy;
import ru.pixelsky.xmod.core.XMod;


/**
 *
 * @author Smile
 */
public class XModNotifyHandler implements IPacketHandler
{
    private final String NOTIFY_LOW = "XMod|Notify_LOW";
    private final String NOTIFY_HIGHT = "XMod|Notify_HIGH";
    
    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) 
    {
        if(packet.channel.equalsIgnoreCase(NOTIFY_LOW))
        {
            String message = new String(packet.data);
            if(message.isEmpty())
            {
                XMod.proxy.addLowMessageToArray("Ошибка, пожалуйста сообщите администратору.");
                return;
            }
            XMod.proxy.addLowMessageToArray(message);
        }
        else if(packet.channel.equalsIgnoreCase(NOTIFY_HIGHT))
        {
            String message = new String(packet.data);
            if(message.isEmpty())
            {
                XMod.proxy.addLowMessageToArray("Ошибка, пожалуйста сообщите администратору.");
                return;
            }
            XMod.proxy.addHightMessageToArray(message);
        }
    }
    
}
