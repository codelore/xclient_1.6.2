package ru.pixelsky.xmod.network;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class PingPacketHandler implements IPacketHandler {
    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
        if (!packet.channel.equals("psky-ping"))
            return;

        String message = new String(packet.data);
        if (message.equals("ping")) {
            PingUtils.sendPong(player);
        } else if (message.equals("pong")){
            PingUtils.onPongReceived();
        }
    }
}
