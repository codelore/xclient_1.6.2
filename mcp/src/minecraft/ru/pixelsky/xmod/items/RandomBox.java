package ru.pixelsky.xmod.items;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;

import java.util.Random;

public class RandomBox extends Item {
    private static Random rng = new Random();

    private ItemStack[] rarePrizes = new ItemStack[] {
            new ItemStack(Item.legsDiamond),
            new ItemStack(Item.plateDiamond),
            new ItemStack(Item.bootsDiamond),
            new ItemStack(Item.helmetDiamond),
            new ItemStack(Item.swordDiamond),
            new ItemStack(Item.pickaxeDiamond),
            new ItemStack(Item.shovelDiamond),
            new ItemStack(Item.axeDiamond),
            new ItemStack(Item.axeWood),
            new ItemStack(Item.pickaxeGold),
            new ItemStack(Item.plateChain),
    };

    private ItemStack[] prizes = new ItemStack[] {
            new ItemStack(Item.diamond, 1),
            new ItemStack(XMod.blockFlower, 1, 0),
            new ItemStack(XMod.blockFlower, 1, 1),
            new ItemStack(XMod.blockFlower, 1, 2),
            new ItemStack(XMod.blockFlower, 1, 3),

            new ItemStack(XMod.itemLogo, 1),
            new ItemStack(Item.bread, 32),
            new ItemStack(Item.beefCooked, 8),
            new ItemStack(Item.cake, 5),

            new ItemStack(XMod.blockLantern, 2, 0),
            new ItemStack(XMod.blockLantern, 2, 1),
            new ItemStack(XMod.blockLantern, 2, 2),

            new ItemStack(XMod.blockSakuraSapling, 1),
            new ItemStack(Block.blockEmerald, 1)
    };

    public RandomBox(int id) {
        super(id);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        if (world.isRemote)
            return stack;

        stack.stackSize --;

        int num = random(2, 3);
        for (int i = 0; i < num; i++) {
            if (rng.nextInt(100) < 5) {
                ItemStack itemStack = rarePrizes[rng.nextInt(rarePrizes.length)].copy();
                putRandomEnchantments(itemStack);
                player.dropPlayerItem(itemStack);
            } else {
                ItemStack itemStack = prizes[rng.nextInt(prizes.length)].copy();
                player.dropPlayerItem(itemStack);
            }
        }

        return stack;
    }

    private void putRandomEnchantments(ItemStack stack) {
        for (Enchantment enchantment : Enchantment.enchantmentsList) {
            if (enchantment == null)
                continue;

            if(enchantment.canApply(stack) && rng.nextInt(100) < 40) {
                int level = random(1, enchantment.getMaxLevel());
                stack.addEnchantment(enchantment, level);
            }
        }
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.uncommon;
    }

    private int random(int min, int max) {
        return rng.nextInt(max - min + 1) + min;
    }
}
