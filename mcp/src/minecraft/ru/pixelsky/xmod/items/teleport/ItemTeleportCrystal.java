package ru.pixelsky.xmod.items.teleport;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemTeleportCrystal extends ItemLocationed {
    private static final int MAX_DAMAGE = 10;

    public ItemTeleportCrystal(int id) {
        super(id);
        setMaxDamage(MAX_DAMAGE);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
        if (world.isRemote)
            return itemStack;
        if (itemStack == null || itemStack.stackSize == 0)
            return itemStack;

        TeleportPlace p = getTeleportPlace(itemStack);
        if (p != null) {
            if (player.dimension != p.getDimension())
                player.travelToDimension(p.getDimension());
            player.setPositionAndUpdate(p.getX(), p.getY(), p.getZ());
        }

        int dmg = itemStack.getItemDamage() + 1;
        if (dmg < MAX_DAMAGE) {
            itemStack.setItemDamage(dmg);
        } else {
            itemStack.stackSize--;
        }
        return itemStack;
    }
}
