package ru.pixelsky.xmod.items.teleport;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.List;

/**
 * Описывает item, у которого есть место телепортации
 */
public class ItemLocationed extends Item {
    public ItemLocationed(int id) {
        super(id);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean v) {
        TeleportPlace place = getTeleportPlace(stack);
        if (place != null) {
            list.add("X: " + place.getX() + " Y: " + place.getY() + " Z: " + place.getZ());
        }
    }

    public TeleportPlace getTeleportPlace(ItemStack itemStack) {
        NBTTagCompound tagCompound = getLocationCompound(itemStack, false);
        if (tagCompound != null) {
            TeleportPlace place = new TeleportPlace();
            place.readFromNBT(tagCompound);
            return place;
        }
        return null;
    }

    public void setTeleportPlace(ItemStack itemStack, TeleportPlace place) {
        place.writeToNBT(getLocationCompound(itemStack, true));
    }

    public NBTTagCompound getLocationCompound(ItemStack itemStack, boolean create) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if (create && tagCompound == null) {
            tagCompound = new NBTTagCompound();
            itemStack.setTagCompound(tagCompound);
        }
        if (tagCompound != null) {
            if (tagCompound.hasKey("location")) {
                NBTTagCompound locationCompound = tagCompound.getCompoundTag("location");
                return locationCompound;
            } else if (create) {
                NBTTagCompound locationCompound = tagCompound.getCompoundTag("location");
                tagCompound.setCompoundTag("location", locationCompound);
                return locationCompound;
            } else {
                return null;
            }
        }
        return null;
    }
}