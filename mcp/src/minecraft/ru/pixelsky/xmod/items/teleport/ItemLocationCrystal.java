package ru.pixelsky.xmod.items.teleport;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;

public class ItemLocationCrystal extends ItemLocationed {
    private Icon uncodedIcon;
    private Icon codedIcon;

    public ItemLocationCrystal(int id) {
        super(id);
        setMaxStackSize(1);
        setCreativeTab(XMod.xModTab);
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        uncodedIcon = iconRegister.registerIcon("xmod:crystal-uncoded");
        codedIcon = iconRegister.registerIcon("xmod:crystal-coded");
    }

    @Override
    public Icon getIconFromDamage(int damage) {
        switch (damage) {
            case 0:
                return uncodedIcon;
            case 1:
                return codedIcon;
            default:
                return null;
        }
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
        if (world.isRemote)
            return itemStack;

        TeleportPlace p = getTeleportPlace(itemStack);
        if (p == null) {
            ChunkCoordinates loc = player.getPlayerCoordinates();
            TeleportPlace place = new TeleportPlace(player.dimension, loc.posX, loc.posY, loc.posZ);
            setTeleportPlace(itemStack, place);
            itemStack.setItemDamage(1);
        }
        return itemStack;
    }
}
