package ru.pixelsky.xmod.items.teleport;

import net.minecraft.nbt.NBTTagCompound;

public class TeleportPlace {
    private int dimension;
    private int x, y, z;

    public TeleportPlace() {
    }

    public TeleportPlace(int dimension, int x, int y, int z) {
        this.dimension = dimension;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getDimension() {
        return dimension;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public void writeToNBT(NBTTagCompound compound) {
        compound.setInteger("x", x);
        compound.setInteger("y", y);
        compound.setInteger("z", z);
        compound.setInteger("dim", dimension);
    }

    public void readFromNBT(NBTTagCompound compound) {
        x = compound.getInteger("x");
        y = compound.getInteger("y");
        z = compound.getInteger("z");
        dimension = compound.getInteger("dim");
    }

    @Override
    public String toString() {
        return "TeleportPlace{" +
                "dimension=" + dimension +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
