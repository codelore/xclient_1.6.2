package ru.pixelsky.utils;

public class PingManager {
    private static int ping;

    public static int getPing() {
        return ping;
    }

    public static void setPing(int ping) {
        PingManager.ping = ping;
    }
}
