#!/bin/bash

# PART 1.COMPILATION.
./recompile.sh || exit 1

# PART 2.OBFUSCATION.
./reobfuscate.sh || exit 1

# PART 3. PACKING.
# Make empty directory for build
mkdir build
rm -rf build/classes
mkdir build/classes

# Unpack minecraft.jar only with Forge
unzip build/minecraft_clean.zip -d build/classes

# Copy modified files
cp -R reobf/minecraft/* build/classes/ 

# Copy resource files
cp -R res/* build/classes/ 

# Zip to minecraft.jar
cd build/classes
rm ../minecraft.jar
zip -r -9 ../minecraft.jar .

# Delete dir
rm -rf build/classes
